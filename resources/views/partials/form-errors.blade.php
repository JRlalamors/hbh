@if (count($errors) > 0)
    <div class="alert alert-danger alert-close">
    	<a href="#" title="Close" class="glyph-icon alert-close-btn icon-remove"></a>
        <div class="bg-red alert-icon">
            <i class="glyph-icon icon-warning"></i>
        </div>
        <div class="alert-content">
            <h4 class="alert-title">Please check required fields</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif