@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
<style type="text/css">
    .prmenu-wrapper .nav-tabs {
        border: 0;
    }
</style>
@stop
@section('body-class')
    dashboard
@stop
@section('page-title')
    Dashboard
@stop

<!-- Content -->
@section('content')
<section class="main-dashboard">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="prmenu-wrapper">
                    <div class="prmenu-list equal-items nav nav-tabs row">
                        <div class="item active col-xs-12 col-sm-4">
                            <a href="#tab-customers" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url({{asset('images/admin/dashboard/customer-leads.jpg')}})"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url({{asset('images/icon-agentsreview.svg')}})"></div>
                                    <h2 class="title">CUSTOMER LEADS</h2>
                                </div>
                            </a>
                        </div>

                        <div class="item col-xs-12 col-sm-4">                            
                            <a href="#tab-agents" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url({{asset('images/admin/dashboard/agent-leads.jpg')}})"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url({{asset('images/icon-agentleads.svg')}})"></div>
                                    <h2 class="title">AGENT LEADS</h2>
                                </div>
                            </a>
                        </div>

                        <div class="item col-xs-12 col-sm-4">
                            <a href="#tab-deals" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url({{asset('images/admin/dashboard/deals-made.jpg')}})"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url({{asset('images/icon-dealsmade.svg')}})"></div>
                                    <h2 class="title">DEALS MADE</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

               <div class="tab-content">
                   <div id="tab-customers" class="tab-pane active">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Customer ID</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Phone #</th>
                                            <th class="text-center">Buyer?</th>
                                            <th class="text-center">Seller?</th>
                                            <th class="text-center">View Full Profile</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @forelse( $customers as $customer )
                                            <tr>
                                                <td class="text-center">{{ $customer->id }}</td>
                                                <td class="text-center">{{ $customer->asUser->name }}</td>
                                                <td class="text-center">{{ $customer->asUser->email }}</td>
                                                <td class="text-center">{{ $customer->asUser->phone_format }}</td>
                                                <td class="text-center">{{ $customer->buyerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                                <td class="text-center">{{ $customer->sellerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                                <td class="text-center"><a href="{{ route('admin.customers.profile', $customer) }}" class="blue">View Profile</a></td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7">Customers not found..</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>

                   <div id="tab-agents" class="tab-pane fade">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Agency</th>
                                            <th class="text-center">Phone #</th>
                                            <th class="text-center">View Full Profile</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @forelse( $agents as $agent )
                                            <tr>
                                                <td class="text-center">{{ $agent->asUser->name }}</td>
                                                <td class="text-center">{{ $agent->asUser->email }}</td>
                                                <td class="text-center">{{ $agent->zillowApi()['proInfo']['businessName'] }}</td>
                                                <td class="text-center">{{ $agent->asUser->phone_format }}</td>
                                                <td class="text-center"><a href="{{ route('admin.agents.profile', $agent) }}" class="blue">View Profile</a></td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5">Agents not found..</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>

                   <div id="tab-deals" class="tab-pane fade">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Transaction ID</th>
                                            <th class="text-center">Date Scheduled</th>
                                            <th class="text-center">Client's Name</th>
                                            <th class="text-center">Rebate</th>
                                            <th class="text-center">Deal Closed</th>
                                            <th class="text-center">View Details</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @forelse( $deals as $deal )
                                            <tr>
                                                <td class="text-center">{{ $deal->id }}</td>
                                                <td class="text-center">@dateFormat( $deal->until )</td>
                                                <td class="text-center">{{ $deal->customer->asUser->name }}</td>
                                                <td class="text-center">{{ $deal->selectedProposal() ? $deal->selectedProposal()->rebate_format : '
                                                ' }}</td>
                                                <td class="text-center">{{ $deal->closed_deal ? 'Y' : 'N' }}</td>
                                                <td class="text-center"><a href="{{ route('admin.deals.details', $deal)}}" class="blue">View Details</a></td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6">Agents not found..</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>
               </div>

            </div>
        </div>
    </div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop