@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	agents dashboard
@stop

@section('page-title')
	Listing Details 
@stop

<!-- Content -->
@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<label>Customer Name: </label> {{ $list->customer->Asuser->name }} <br>
				<label>Type: </label> {{ $list->type() }} <br>
				<label>Property type: </label> {{ $list->property->human_display }} <br>
				@if( $list->list_type == 'seller' )		
					<label>Address: </label> {{ $list->complete_address }} <br>
				@endif
				<label>ZIP/Postal: </label> {{ $list->zip_postal }} <br>
				<label>SQ. Ft.: </label> {{ $list->square_feet }} <br>
				<label>Bedrooms: </label> {{ $list->bedrooms }} <br>
				<label>Bathrooms: </label> {{ $list->bathrooms }} <br>
				<label>
					@if($list->list_type == 'seller')
						How soon customer looking to sell:
						@else
						How soon customer looking to buy:
					@endif
				</label>
				@if($list->text_until)
					{{ $list->text_until }}
				@else
					@dateFormat( $list->until )
				@endif
			</div>
		</div>


		@if( $list->selectedProposal() )
			<hr>
			<div class="row">
				<div class="col-xs-12">
					<h3>Proposal</h3>
					<label>Agent Name: </label> {{ $list->selectedAgent->asUser->name }} <br>
					<label>Proposal type: </label> 
					@if( $list->list_type == 'seller' )
						@if( $list->selectedProposal()->rebate_type == 'percentage' )
							{{ 'Total Commission Rate' }}
						@else
							{{ 'Flat Fee' }}
						@endif
					@else
						@if( $list->selectedProposal()->rebate_type == 'percentage' )
							{{ 'Commission Rebate' }}
						@else
							{{ 'Cash Rebate' }}
						@endif
					@endif
					<br>
					<label>Rebate: </label> {{ $list->selectedProposal()->rebate_type == 'fixed' ? '$' : '' }} {{ $list->selectedProposal()->rebate }}{{ $list->selectedProposal()->rebate_type == 'percentage' ? '%' : '' }} <br>
					<label>Closed deal?: </label> {{ $list->closed_deal ? 'Yes' : 'No' }}
				</div>
			</div>
		@endif
	</div>
	
@stop


<!-- Added Scripts -->
@section('scripts')

@stop