@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
	<style type="text/css">
		form .form-control {
			height: 48px;
		}
	</style>
@stop

@section('body-class')
	agents agent-profile
@stop
@section('page-controls')
<div class="controls">
	@if( !$agent->approved )
		<a class="btn btn-pink" data-confirmtitle="Approve agent?" data-confirmtext="Are you sure you want to approve this agent?" data-confirmfunc="approveAgent" data-confirmparams="[{{ $agent->id }}]">Approve</a>
	@endif

	@if( !$agent->rejected )
		<a class="btn btn-blue" data-confirmtitle="Reject agent?" data-confirmtext="Are you sure you want to reject this agent?" data-confirmfunc="rejectAgent" data-confirmparams="[{{ $agent->id }}]" >Reject</a>
	@endif
</div>
@stop

@section('page-title')
	Agent Profile
@stop

<!-- Content -->
@section('content')
<section id="agent-profile">

	@include('partials.form-success')

	<div class="profile-details default-section">
		<div class="container">
			<div class="row">
				{!! Form::model($agent, ['route' => ['admin.agents.profile.store', $agent], 'class' => 'default-form']) !!}
					<div class="col-xs-12 col-sm-6">
						<div class="profile-detail">
							<div class="title">Name:</div>
							<div class="content">
								<div class="profile-display">									
									{{ $agent->getOtherData('admin_name') ? $agent->getOtherData('admin_name') : $agent->asUser->name }}
								</div>
								<div class="profile-form hidden">									
									{!! Form::text('admin_name', $agent->getOtherData('admin_name') ? $agent->getOtherData('admin_name') : $agent->asUser->name, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Email:</div>
							<div class="content">
								<div class="profile-display">									
									{{ $agent->getOtherData('admin_email') ? $agent->getOtherData('admin_email') : $agent->asUser->email }}
								</div>
								<div class="profile-form hidden">
									{!! Form::text('admin_email', $agent->getOtherData('admin_email') ? $agent->getOtherData('admin_email') : $agent->asUser->email, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Phone:</div>
							<div class="content">
								<div class="profile-display">									
									<a href="tel:@phoneNum( $agent->asUser->phone_num )">{{ $agent->getOtherData('admin_phone') ? $agent->getOtherData('admin_phone') : $agent->asUser->phone_format }}</a>
								</div>
								<div class="profile-form hidden">
									{!! Form::text('admin_phone', $agent->getOtherData('admin_phone') ? $agent->getOtherData('admin_phone') : $agent->asUser->phone_format, ['class' => 'form-control validate-phone-num']); !!}
								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Agency:</div>
							<div class="content">
								<div class="profile-display">								
									@if( $agent->getOtherData('admin_agency') )
										{{ $agent->getOtherData('admin_agency') }}
									@else
										{{ isset($zillowApi['proInfo']['businessName']) ? $zillowApi['proInfo']['businessName'] : '' }}
									@endif
								</div>
								<div class="profile-form hidden">
									{!! Form::text('admin_agency', $agent->getOtherData('admin_agency') ? $agent->getOtherData('admin_agency') : $zillowApi['proInfo']['businessName'], ['class' => 'form-control'] ) !!}
								</div>
							</div>
						</div>					

						<div class="profile-detail broker-detail">
							<div class="title">Broker:</div>
							<div class="content">
								<div class="profile-display">									
									{{ $agent->getOtherData('admin_broker') && $agent->getOtherData('admin_broker') != null ? $agent->getOtherData('admin_broker') : '' }}
								</div>
								<div class="profile-form hidden">
									{!! Form::text('admin_broker', $agent->getOtherData('admin_broker'), ['class' => 'form-control']) !!}
								</div>
							</div>						
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="profile-detail">
							<div class="title">Agent ID:</div>
							<div class="content">{{ $agent->id }}</div>
						</div>
						<div class="profile-detail">
							<div class="title">Broker:</div>
							@php
								if( $agent->getOtherData('admin_as_broker') ) 
								{
									if( $agent->getOtherData('admin_as_broker') == 'yes' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}
								} else {
									if( $agent->current_role == 'broker' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}		
								}
							@endphp
							<div class="content">
								<div class="content default-form">
									<div class="checkbox-form">
										<label class="checkbox"><input type="radio" name="admin_as_broker" value="yes" {{ $checkedYes }}><span class="chk"></span><span class="text">Yes</span></label>
									</div>
									<div class="checkbox-form">	
										<label class="checkbox"><input type="radio" name="admin_as_broker" value="no" {{ $checkedNo }}><span class="chk"></span><span class="text">No</span></label>									
									</div>
								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Agent:</div>
							@php
								if( $agent->getOtherData('admin_as_agent') ) 
								{
									if( $agent->getOtherData('admin_as_agent') == 'yes' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}
								} else {
									if( $agent->current_role == 'individual-agent' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}		
								}
							@endphp
							<div class="content default-form">
								<div class="checkbox-form">
									<label class="checkbox"><input type="radio" name="admin_as_agent" value="yes" {{ $checkedYes }}><span class="chk"></span><span class="text">Yes</span></label>

								</div>
								<div class="checkbox-form">	
									<label class="checkbox"><input type="radio" name="admin_as_agent" value="no" {{ $checkedNo }}><span class="chk"></span><span class="text">No</span></label>
									
								</div>
							</div>
						</div>						

						<div class="profile-detail">
							<div class="title">License #:</div>
							<div class="content">
								<div class="profile-display">{{ $agent->getOtherData('admin_license') && $agent->getOtherData('admin_license') != null ? $agent->getOtherdata('admin_license') : '' }}</div>

								<div class="profile-form hidden">									
									{!! Form::text('admin_license', $agent->getOtherData('admin_license') && $agent->getOtherData('admin_license') != null ? $agent->getOtherdata('admin_license') : '', ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>
						
						<div class="profile-detail broker-detail">
							@php
								if( $agent->current_role != 'individual-agent' && $agent->current_role != 'broker' )
								{
									$other = $agent->current_role;
								} else {
									$other = '';								
								}
							@endphp
							<div class="title">Other:</div>
							<div class="content">
								<div class="profile-display">									
									{{ $agent->getOtherData('admin_other') && $agent->getOtherData('admin_other') != null ? $agent->getOtherData('admin_other') : $other }}
								</div>
								<div class="profile-form hidden">
									{!! Form::text('admin_other', $agent->getOtherData('admin_other') != null ? $agent->getOtherData('admin_other') : $other, ['class' => 'form-control']) !!}
								</div>
							</div>						
						</div>						

						<div class="text-right profile-form hidden">
							<button class="btn btn-success btn-white">Save</button>
							<a href="javascript:;" class="btn btn-danger  cancel-edit-profile">Cancel</a href="javascript:;">
						</div>	

						<div class="text-right">
							<a href="javascript:;" class="btn btn-blue edit-profile">Edit</a>
						</div>
					</div>					
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="profile-settings default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="tab-controls">
						<ul class="nav nav-tabs nav-justified">
						  <li class="active"><a data-toggle="tab" class="theme-blue" href="#tab-buyers">Buyers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-sellers">Sellers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-pitches">Proposal Requests</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-social">Social</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-qa">Q+A</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-history">History</a></li>
						</ul>
					</div>
					<div class="tab-content">
			  			<div id="tab-buyers" class="tab-pane fade in active">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">How Soon</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Closed Date</th>
			                                <th class="text-center">View Details</th>
			                                <th class="text-center">Transaction ID</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	@forelse( $buyersList as $list )
			                        		<tr>		                        			
				                        		<td class="text-center">
													@if($list->text_until)
														{{ $list->text_until }}
													@else
														@dateFormat( $list->until )
													@endif
												</td>
				                        		<td class="text-center">{{ $list->customer->asUser->name }}</td>
				                        		<td class="text-center">{{ $list->proposal($agent->id)->rebate_format }}</td>
				                        		<td class="text-center"></td>
				                        		<td class="text-center"><a href="{{ route('admin.agents.profle.list-details', [$agent, $list]) }}" class="blue">View Full Details</a></td></td>
				                        		<td class="text-center">{{ $list->proposal($agent->id)->id }}</td>
			                        		</tr>
			                        	@empty
			                        		<tr><td colspan="6">Buyers not found..</td></tr>
			                        	@endforelse
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					  	<div id="tab-sellers" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">How Soon</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Closed Date</th>
			                                <th class="text-center">View Details</th>
			                                <th class="text-center">Transaction ID</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	@forelse( $sellersList as $list )
				                            <tr>
				                                <td class="text-center">
													@if($list->text_until)
														{{ $list->text_until }}
													@else
														@dateFormat( $list->until )
													@endif
												</td>
				                                <td class="text-center">{{ $list->customer->asUser->name }}</td>
				                               	<td class="text-center">{{ $list->proposal($agent->id)->rebate_format }}</td>
				                                <td class="text-center">8/3/17</td>
				                                <td class="text-center"><a href="{{ route('admin.agents.profle.list-details', [$agent, $list]) }}" class="blue">View Full Details</a></td>
				                                <td class="text-center">{{ $list->proposal($agent->id)->id }}</td>
				                            </tr>
			                            @empty
				                            <tr>
				                            	<td colspan="6">Sellers not found..</td>
				                            </tr>
			                            @endforelse
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					  	<div id="tab-pitches" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">Pitch Date</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Was Selected</th>
			                                <th class="text-center">Has Selected an Agent</th>
			                                <th class="text-center">View Full Profile</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            @forelse( $pitches as $pitch )
			                            	<tr>
			                            		<td class="text-center">@dateFormat( $pitch->created_at )</td>
			                            		<td class="text-center">
			                            			@if( $pitch instanceof \App\Model\Proposal )
				                            			{{ $pitch->listing->customer->asUser->name }}
			                            			@else
			                            				{{ $pitch->customer->asUser->name }}
			                            			@endif
			                            		</td>
			                            		<td class="text-center">
			                            			@if( $pitch instanceof App\Model\Proposal )
			                            				{{ $pitch->rebate_format }}
				                            		@else 
				                            			{{ '' }}
			                            			@endif
			                            		</td>
			                            		<td class="text-center">
			                            			@if( $pitch instanceof App\Model\Proposal )
				                            			{{ $pitch->isSelected() ? 'Yes' : 'No' }}
			                            			@else
			                            				{{ '' }}
			                            			@endif
			                            		</td>
			                            		<td class="text-center">
			                            			@if( $pitch instanceof App\Model\Proposal )
				                            			{{ $pitch->listing->hasSelectedAProposal() ? 'Yes' : 'No' }}
			                            			@else
			                            				{{ $pitch->hasSelectedAProposal() ? 'Yes' : 'No' }}
			                            			@endif
			                            		</td>
			                            		<td class="text-center">
			                            			@if( $pitch instanceof App\Model\Proposal )
				                            			<a href="{{ route('admin.customers.profile', $pitch->listing->customer) }}" class="blue">View Profile</a>
			                            			@else
			                            				<a href="{{ route('admin.customers.profile', $pitch->customer) }}">View Profile</a>
			                            			@endif
			                            		</td>
			                            	</tr>
			                            @empty
			                            	<tr><td colspan="6">Requests not found..</td></tr>
			                            @endforelse
			                        </tbody>
			                    </table>
			                </div>
					  	</div>

					  	<div id="tab-social" class="tab-pane fade">
					    	<div class="default-content">
					    		<div class="social-contents">
					    			<div class="row">

					    				<div class="col-xs-12 col-md-6">
					    					{!! Form::model($agent, ['route' => ['admin.agents.profile.storeSocial', $agent], 'class' => 'default-form']) !!}
					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">LinkedIn URL:</div>
					    							<div class="desc social">
					    								<a href="{{ $agent->getOtherdata('admin_linkedin_url') ? $agent->admin_linkedin_username : $agent->linkedin_username }}" target="_blank">{{ $agent->getOtherData('admin_linkedin_url') ? $agent->admin_linkedin_username : $agent->linkedin_username }}</a>
					    							</div>

					    							<div class="form-group form-social hidden">
					    								{!! Form::text('admin_linkedin_url', $agent->getOtherData('admin_linkedin_url') ? $agent->getOtherData('admin_linkedin_url') : $agent->linkedin_url, ['class' => 'form-control']) !!}
					    							</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Website URL:</div>
					    							<div class="desc social">
					    								<a href="{{ $agent->getOtherdata('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url }}" target="_blank">{{ $agent->getOtherData('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url }}</a>
					    							</div>

					    							<div class="form-group form-social hidden">
					    								{!! Form::text('admin_website_url', $agent->getOtherData('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url, ['class' => 'form-control']) !!}
					    							</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper form-social hidden">
					    						<div class="content">
					    							<div class="title">Zillow Username:</div>					    							
						    						<div class="form-group">
						    							{!! Form::text('admin_zillow_url', $agent->getOtherData('admin_zillow_url') ? $agent->getOtherData('admin_zillow_url') : $agent->zillow_url, ['class' => 'form-control']) !!}
						    						</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
						    						<div class="title">Zillow Profile URL:</div>
						    						<div class="desc">
						    							<a href="{{ isset($zillowApi['proInfo']['profileURL']) ? $zillowApi['proInfo']['profileURL'] : '' }}" target="_blank">{{ isset($zillowApi['proInfo']['profileURL']) ? $zillowApi['proInfo']['profileURL'] : '' }}</a>
						    						</div>						    						
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
						    						<div class="title">Zillow Screen Name:</div>
						    						<div class="desc">{{ isset( $zillowApi['screenname'] ) ? $zillowApi['screenname'] : '' }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Zillow Profile Photo:</div>
					    							<div class="desc">
					    								<div class="image">
					    									<img src="{{ isset($zillowApi['proInfo']['photo']) ? $zillowApi['proInfo']['photo'] : asset('images/default-avatar.svg') }}" alt="Home by Home" class="img-responsive">
					    								</div>		    								
					    							</div>
					    						</div>
					    					</div> 			

					    					<div class="content-wrapper">
					    						<div class="content text-right">
					    							<a href="javascript:;" class="btn btn-blue edit-social">Edit</a>

					    							<div class="social-actions hidden">
					    								<button class="btn btn-success">Save</button>
					    								<a href="javascript:;" class="btn btn-red cancel-edit-social">Cancel</a>
					    							</div>
					    						</div>
					    					</div>
					    					{!! Form::close() !!}		
					    				</div>

					    				<div class="col-xs-12 col-md-6">					    					
					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Review Count:</div>
					    							<div class="desc">{{ isset( $zillowApi['reviewCount'] ) ? $zillowApi['reviewCount'] : 0 }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating:</div>
					    							<div class="desc">{{ isset( $zillowApi['proInfo']['avgRating'] ) ? $zillowApi['proInfo']['avgRating'] : 0 }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Local Knowledge:</div>
					    							<div class="desc">{{ isset( $zillowApi['proInfo']['localknowledgeRating'] ) ? $zillowApi['proInfo']['localknowledgeRating'] : 0 }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Process Expertise:</div>
					    							<div class="desc">{{ isset( $zillowApi['proInfo']['processexpertiseRating'] ) ? $zillowApi['proInfo']['processexpertiseRating'] : 0 }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Negotation Skills:</div>
					    							<div class="desc">{{ isset( $zillowApi['proInfo']['negotiationskillsRating'] ) ? $zillowApi['proInfo']['negotiationskillsRating'] : 0 }}</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Responsiveness:</div>
					    							<div class="desc">{{ isset($zillowApi['proInfo']['responsivenessRating']) ? $zillowApi['proInfo']['responsivenessRating'] : 0 }}</div>
					    						</div>
					    					</div>					    					
					    				</div>
					    			</div>
					    		</div>
					    	</div>
					  	</div>
					  	<div id="tab-qa" class="tab-pane fade">
					    	<div class="default-content">
					    		{!! Form::model($agent, ['route' => ['admin.agents.profile.storeQA', $agent]]) !!}
						    		<div class="qa-contents">
						    			<div class="content">
							    			<div class="title">Why should you hire me?</div>
							    			<textarea name="question_1" class="desc" placeholder="Contents from agent here...." disabled>{{ $agent->getOtherData('question_1') }}</textarea> 
						    			</div>
						    			<div class="content">
							    			<div class="title">What do i do better than most agents?</div>
							    			<textarea name="question_2" class="desc" placeholder="Contents from agent here...." disabled>{{ $agent->getOtherData('question_2') }}</textarea> 
						    			</div>
						    			<div class="content">
							    			<div class="title">What do others say about me?</div>
							    			<div class="comments-section">
												<!-- <div class="cs-write">
													<a class="btn btn-pink btn-block hvr-rectangle-out" href="#">WRITE A REVIEW</a>
												</div> -->
												@if( isset($zillowApi['proReviews']['review']) && count($zillowApi['proReviews']['review']) )
													<div class="cs-list">
														<ul>
															@foreach( $zillowApi['proReviews']['review'] as $review )
																<li>
																	<div class="cs-item">
																		<div class="top">
																			<div class="stars">
																				@php
																					$r1 = floatval( isset($review['localknowledgeRating']) ? $review['localknowledgeRating'] : 0 );
																					$r2 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																					$r3 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																					$r4 = floatval( isset($review['negotiationskillsRating']) ? $review['negotiationskillsRating'] : 0 );

																					$raRating = ($r1 + $r2 + $r3 + $r4) / 4;

																					$remaining = 5 - $raRating;

																					for( $i = 1; $i <= $raRating; $i++ ) {
																						echo '<i class="fa fa-star" aria-hidden="true"></i> ';
																					}

																					if( fmod($raRating, 1) !== 0.00 ) {
																						echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
																					}

																					for( $i = 1; $i <= $remaining; $i++ ) 
																					{
																						echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
																					}
																				@endphp
																			</div>
																			<div class="date">{{ isset($review['reviewDate']) ? $review['reviewDate'] : '' }}</div>
																		</div>
																		<div class="cs-name">{{ ucwords(isset($review['reviewer']) ? $review['reviewer'] : '') }}</div>
			
																		<div class="cs-info">{{ isset($review['reviewSummary']) ? $review['reviewSummary'] : '' }}</div>
																		<div class="cs-message">{!! isset($review['description']) ? $review['description'] : '' !!}</div>
																	</div>
																</li>		
															@endforeach						
														</ul>
													</div>
												@endif
												<!-- <div class="cs-more text-center">
													<a class="btn btn-round btn-yellow hvr-rectangle-out" href="#"><strong>Read More Reviews</strong></a>
												</div> -->
											</div>

						    			</div>
						    		</div>
						    		<div class="text-right">
						    			<a href="javascript:;" class="btn btn-blue edit-qa">Edit</a>
						    		</div>
						    		<div class="action-qa hidden text-right">
						    			<button class="btn btn-success">Save</button>
						    			<a href="javascript:;" class="btn btn-danger cancel-edit-qa">Cancel</a>
						    		</div>
					    		{!! Form::close() !!}
					    	</div>
					  	</div>
					  	<div id="tab-history" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th>Action Taken</th>
			                                <th class="text-right">Date and Time</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            @forelse( $activities as $activity )
			                            	<tr>
			                            		<td>{{ $activity->action_taken }} : {{ $activity->ip }}</td>
			                            		<td class="text-right">@dateTimeFormat($activity->date)</td>
			                            	</tr>
			                            @empty
			                            @endforelse
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

<!-- Added Scripts -->
@section('scripts')
	<script type="text/javascript">
		
		// editing profile details
		$('.edit-profile, .cancel-edit-profile').click(function() {
			$('.profile-form, .profile-display, .edit-profile').toggleClass('hidden');
		});

		// editing qa
		$('.edit-qa, .cancel-edit-qa').click(function() {
			$('.action-qa, .edit-qa').toggleClass('hidden');

			$('.desc').each(function(){
				if( $(this).attr('disabled') )
				{
					$(this).removeAttr('disabled');
				} else {
					$(this).attr('disabled', 'disabled');
				}
			});
		});

		$('.edit-social, .cancel-edit-social').click(function(){
			$('.form-social, .desc.social, .social-actions, .edit-social').toggleClass('hidden');
		});

	</script>
@stop