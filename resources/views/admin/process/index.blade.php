@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop
@section('body-class')
	process
@stop
@section('page-title')
	Process
@stop

<!-- Content -->
@section('content')
<section class="process-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="prmenu-wrapper">
					<div class="prmenu-list equal-items">
						<div class="item">
							<a href="{{ route('admin.process.deals') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Curent-deals-out-for-proposal.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-dealsmade.svg')}})"></div>	
									<h2 class="title">CURRENT DEALS OUT FOR PROPOSAL</h2>
								</div>
							</a>
						</div><div class="item">
							
							<a href="{{ route('admin.process.deals', 'dwa=10') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Deals-wtihout-an-agent-over-10-days.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icons-dealswoagent10.svg')}})"></div>
									<h2 class="title">DEALS WITHOUT AN AGENT OVER 10 DAYS</h2>
								</div>
							</a>

						</div><div class="item">
							<a href="{{ route('admin.process.deals', 'dwa=30') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Deals-wtihout-an-agent-over-30-days.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icons-dealswoagent30.svg')}})"></div>
									<h2 class="title">DEALS WITHOUR AN AGENT OVER 30 DAYS</h2>
								</div>
							</a>
						</div><div class="item">
							<a href="{{ route('admin.process.deals', 'dnc=true') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Deals-without-an-agent-that-havent-closed-yet.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icons-dealswagentnotclosed.svg')}})"></div>	
									<h2 class="title">DEALS WITH AN AGENT THAT HAVEN'T CLOSED YET</h2>
								</div>
							</a>
						</div><div class="item">
							
							<a href="{{ route('admin.customers.to-screen') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Customers-to-review.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-customerreview.svg')}})"></div>
									<h2 class="title">CUSTOMERS TO REVIEW</h2>
								</div>
							</a>

						</div><div class="item">
							<a href="{{ route('admin.agents.review') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/process/Agents-to-review.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-agentsreview.svg')}})"></div>
									<h2 class="title">AGENTS TO REVIEW</h2>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>	
@stop


<!-- Added Scripts -->
@section('scripts')

@stop