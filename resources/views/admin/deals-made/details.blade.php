@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop
@section('body-class')
    deals pagetitle-off
@stop

<!-- Content -->
@section('content')
<section id="agent-profile" class="default-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="profile">
                    <div class="item">
                        <div class="photo">
                            <img class="img-responsive" src="{{ asset('images/user-blank.svg') }}">
                        </div>
                    </div><div class="item">
                        <div class="info">
                            <h1 class="name">
                               {{ $deal->customer->asUser->name }}
                            </h1>
                            <div class="company">
                                Customer Info
                            </div>
                            <div class="address">
                                 Customer Info
                            </div>
                            <div class="phone">
                                Customer Info
                            </div>
                            <div class="websites">
                                 Customer Info
                            </div>
                        </div>
                    </div><div class="item">
                        <div class="buttons">
                            <a href="" class="btn btn-pink btn-round hvr-sweep-to-left">Back</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="agent-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
        
                <div class="info-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Request Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Type:</div>
                                        <div class="rate">{{ $deal->property->human_display }}</div>
                                    </div>
                                     @if( $deal->list_type == 'buyer' )
                                    <div class="list">
                                        <div class="head">Budget:</div>
                                        <div class="rate">$ @moneyFormat($deal->budget)</div>
                                    </div>
                                    @endif
                                    <div class="list">
                                        <div class="head">Square Feet:</div>
                                        <div class="rate">{{ $deal->square_feet }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Zip / Postal:</div>
                                        <div class="rate">{{ $deal->zip_postal }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Bedrooms:</div>
                                        <div class="rate">{{ $deal->bedrooms }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Bathrooms:</div>
                                        <div class="rate">{{ $deal->bathrooms }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">{{ $deal->list_type == 'seller' ? 'Selling' : 'Buying' }} until:</div>
                                        <div class="rate">@dateFormat($deal->until)</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Submitted Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Agent Name:</div>
                                        <div class="rate"> {{ $proposal->agent->asUser->name }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Rebate Type:</div>
                                        <div class="rate">{{ ucfirst($proposal->rebate_type) }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Rebate:</div>
                                        <div class="rate">{{ $proposal->rebate_format }}</div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop