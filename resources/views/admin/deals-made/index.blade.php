@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop
@section('body-class')
    dashboard
@stop
@section('page-title')
    Dashboard
@stop

<!-- Content -->
@section('content')
<section class="main-dashboard">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">        
                <div class="table-responsive default-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">State</th>
                                <th class="text-center">Today</th>
                                <th class="text-center">Yesterday</th>
                                <th class="text-center">Day Before</th>
                                <th class="text-center">This Month</th>
                                <th class="text-center">Last Month</th>
                                <th class="text-center">Month Before</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($ctr=1; $ctr<=50; $ctr++){ ?>
                            <tr>
                                <td class="text-center">AK</td>
                                <td class="text-center">12345</td>
                                <td class="text-center">12345</td>
                                <td class="text-center">12345</td>
                                <td class="text-center">12345</td>
                                <td class="text-center">12345</td>
                                <td class="text-center">12345</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop