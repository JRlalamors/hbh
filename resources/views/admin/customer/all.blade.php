@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	customer 
@stop

@section('page-title')
	See All Customers
@stop

<!-- Content -->
@section('content')
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Customer ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                                <th class="text-center">Agent Proposal #1</th>
                                <th class="text-center">Agent Proposal #2</th>
                                <th class="text-center">Agent Proposal #3</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $lists as $list )
                                <tr class="text-center" data-id="{{ $list->id }}">
                                    <td>{{ $list->customer->id }}</td>
                                    <td>{{ $list->customer->asUser->name }}</td>
                                    <td>{{ $list->customer->asUser->email }}</td>
                                    <td><a href="tel:@phoneNum( $list->customer->asUser->phone_num )">{{ $list->customer->asUser->phone_format }}</a></td>
                                    <td>{{ $list->list_type == 'buyer' ? 'Y' : 'N' }}</td>
                                    <td>{{ $list->list_type == 'seller' ? 'Y' : 'N' }}</td>

                                    @if( $list->agents->count() > 0 )
                                        @foreach( $list->agents as $agent )
                                            <td><a href="javascript:;" class="blue assign-agent" data-toggle="modal" data-target="#assignAgent">{{ $agent->asUser->name }}</a></td>
                                        @endforeach
                                    @endif

                                    @for( $i = 1; $i <= 3 - $list->agents->count(); $i++ )
                                        <td><a href="javascript:;" class="blue assign-agent" data-toggle="modal" data-target="#assignAgent">Assign</a></td>
                                    @endfor
 
                                    <td><a href="{{ route('admin.customers.profile', $list->customer) }}" class="blue">View Profile</a></td>
                                </tr>
                            @empty
                                <tr><td colspan="10">Customers not found..</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination">                	
                    @if( $lists instanceof Illuminate\Pagination\LengthAwarePaginator )
                        {{ $lists->links() }}
                    @endif
                </div>

			</div>
		</div>
	</div>
</section>

@stop

@include('admin.customer.agent-form') 

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            var $baseUrl = $('meta[name="base-url"]').attr('content'),
                $listID,
                $theData = {};              

            $('.assign-agent').click(function() {
                $listID = $(this).closest('tr').data('id');
            });

            $('.agents-select').change(function(){  

                var $agentID = $(this).val(),
                    $agentName = $(this).find('option:selected').text();

                // push new values
                $theData.userAgents = {}
                $theData.userAgents[$agentID] = $agentName

                // push new values
                $('.agents-select').not($(this)).each(function($index){
                    $theData.userAgents[$(this).val()] = $(this).find('option:selected:not(:first-child)').text();
                });

                $('.agents-select').not($(this)).each(function(){
                    $(this)
                        .find('option:not(:first-child)')
                            .each(function(){
                                if( $(this).val() in $theData.userAgents ) {
                                    $(this).attr('disabled', 'disabled');
                                }
                                else {
                                    $(this).removeAttr('disabled');
                                }
                            })
                        .end()
                        .find('option:selected:not(:first-child)')
                            .removeAttr('disabled');
                });

            });

            // load all agents in modal with selected per each row
            $('#assignAgent').on('show.bs.modal', function(e) {
                $.ajax({
                    type: 'POST',
                    url: $baseUrl + '/admin/customers/list/' + $listID + '/edit-agents',
                    success: function($data) {

                        $('#assignAgent .set-agent').data("listid", $data.list.id);

                        // change modal header
                        $('#assignAgent .modal-title').text('Assign agents for ' + $data.customer.as_user.first_name + ' ' + $data.customer.as_user.last_name);

                        $('.agents-select').each(function($index){
                            var $this = $(this);
                            $this.html("");
                            $this.append('<option value="" selected>No Agent</option>');

                            // Append Agents
                            Object.keys($data.agentsArr).forEach(function($key){
                                var $selected = '',
                                    $disabled = '';
                                
                                // check if the object key == index and key exists
                                if( Object.keys($data.userAgents)[$index] == $key ) {                                   
                                    $selected = 'selected';
                                }

                                if( $key in $data.userAgents && Object.keys($data.userAgents)[$index] != $key ) {
                                    $disabled = 'disabled';                                 
                                }
                                
                                $this.append('<option value="'+$key+'" '+$selected+' '+$disabled+'>'+$data.agentsArr[$key].name+' ('+$data.agentsArr[$key].email+')</option>');
                            });
                        });

                        $theData = $data;
                    }
                });
            });

            $('.set-agent').click( function() {

                var $listID = $(this).data('listid'),
                $agents = [
                    $(this).closest('.modal').find('[name="agent-1"]').val(),
                    $(this).closest('.modal').find('[name="agent-2"]').val(),
                    $(this).closest('.modal').find('[name="agent-3"]').val()
                ];

                $agents = $agents.filter(Boolean);

                $.ajax({
                    type: 'POST',
                    url: baseUrl + '/admin/customers/list/' + $listID + '/store-list-agents',
                    data: {'agents' : $agents},
                    success: function( $data ) {
                        if( $data.message == "success" ) 
                        {
                            $('#assignAgent').modal('hide');                            
                            window.location = $data.route;
                        }
                    }
                });

            });

        });
    </script>
@stop