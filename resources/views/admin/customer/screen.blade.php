@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	customer 
@stop

@section('page-title')
	Customers to Screen
@stop

<!-- Content -->
@section('content')
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $customers as $customer )
                                <tr class="text-center">
                                    <td>{{ $customer->id }}</td>
                                    <td>{{ $customer->asUser->name }}</td>
                                    <td>{{ $customer->asUser->email }}</td>
                                    <td><a href="@phoneNum( $customer->asUser->phone_num )" class="default">{{ $customer->asUser->phone_format }}</a></td>
                                    <td>{{ $customer->buyerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                    <td>{{ $customer->sellerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                    <td><a href="{{ route('admin.customers.profile', $customer) }}" class="blue">View Profile</a></td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination">                    
                    @if( $customers instanceof Illuminate\Pagination\LengthAwarePaginator )
                        {{ $customers->links() }}
                    @endif
                </div>

			</div>
		</div>
	</div>
</section>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop