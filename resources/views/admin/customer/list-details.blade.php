@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	agents dashboard pagetitle-off
@stop

@section('page-title')
	Listing Details 
@stop

<!-- Content -->
@section('content')
<section id="agent-profile" class="default-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="profile">
                    <div class="item">
                        <div class="photo">
                            <img class="img-responsive" src="{{ asset('images/user-blank.svg') }}">
                        </div>
                    </div><div class="item">
                        <div class="info">
                            <h1 class="name">
                               {{ $list->customer->Asuser->name }} 
                            </h1>
                            <div class="websites">
                                <a href="@phoneNum($list->customer->asUser->phone_num)">{{ $list->customer->asUser->phone_format }}</a>                                
                            </div>
                        </div>
                    </div><div class="item">
                        <div class="buttons">
                            <a href="{{ route('admin.customers.profile', $list->customer) }}" class="btn btn-pink btn-round">Back</a>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="agent-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
        
                <div class="info-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Request Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Type:</div>
                                        <div class="rate">{{ $list->type() }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Property type:</div>
                                        <div class="rate">{{ $list->property->human_display }}</div>
                                    </div>
                                    @if( $list->list_type == 'seller' )	
                                    <div class="list">
                                        <div class="head">Address:</div>
                                        <div class="rate">{{ $list->complete_address }} </div>
                                    </div>
                                    @endif
                                    <div class="list">
                                        <div class="head">ZIP/Postal:</div>
                                        <div class="rate">{{ $list->zip_postal }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">SQ. Ft.:</div>
                                        <div class="rate">{{ $list->square_feet }}</div>
                                    </div>
									<div class="list">
                                        <div class="head">Bedrooms:</div>
                                        <div class="rate">{{ $list->bedrooms }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Bathrooms:</div>
                                        <div class="rate">{{ $list->bathrooms }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">
                                            @if($list->list_type == 'seller')
                                                How soon customer looking to sell:
                                            @else
                                                How soon customer looking to buy:
                                            @endif
                                        </div>
                                        <div class="rate">
                                            @if($list->text_until)
                                                {{ $list->text_until }}
                                            @else
                                                @dateFormat( $list->until )
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if( $list->selectedProposal() )
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Agent Name:</div>
                                        <div class="rate"> {{ $list->selectedAgent->asUser->name }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Proposal Type:</div>
                                        <div class="rate">
                                            @if( $list->list_type == 'seller' )
                                                @if( $list->selectedProposal()->rebate_type == 'percentage' )
                                                    {{ 'Total Commission Rate' }}
                                                @else
                                                    {{ 'Flat Fee' }}
                                                @endif
                                            @else
                                                @if( $list->selectedProposal()->rebate_type == 'percentage' )
                                                    {{ 'Commission Rebate' }}
                                                @else
                                                    {{ 'Cash Rebate' }}
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Rebate:</div>
                                        <div class="rate">{{ $list->selectedProposal()->rebate_format }} </div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Customer Name:</div>
                                        <div class="rate">{{ $list->customer->name }}</div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Closed deal?:</div>
                                        <div class="rate">{{ $list->closed_deal ? 'Yes' : 'No' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif


                   	</div>
                </div>
            </div>
         </div>
    </div>
</section>
	

@stop


<!-- Added Scripts -->
@section('scripts')

@stop