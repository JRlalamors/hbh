@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	customer 
@stop

@section('page-title')
	Customers Without an Agent
@stop

<!-- Content -->
@section('content')
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $customers as $customer )
                                <tr>
                                    <td class="text-center">{{ $customer->id }}</td>
                                    <td class="text-center">{{ $customer->asUser->name }}</td>
                                    <td class="text-center">{{ $customer->asUser->email }}</td>
                                    <td class="text-center"><a href="@phoneNum( $customer->asUser->phone_num )" class="default">{{ $customer->asUser->phone_format }}</a></td>
                                    <td class="text-center">{{ $customer->buyerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                    <td class="text-center">{{ $customer->sellerList()->count() > 0 ? 'Y' : 'N' }}</td>
                                    <td class="text-center"><a href="{{ route('admin.customers.profile', $customer) }}" class="blue">View Profile</a></td>
                                </tr>
                            @empty
                                <tr><td colspane="7">Customers not found..</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination">                    
                    @if( $customers instanceof Illuminate\Pagination\LengthAwarePaginator )
                        {{ $customers->links() }}
                    @endif
                </div>

			</div>
		</div>
	</div>
</section>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop