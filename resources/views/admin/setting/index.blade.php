@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	settings
@stop

@section('page-title')
	Settings
@stop
@section('page-controls')
<div class="controls">
	<a href="{{ route('admin.settings.create') }}" class="btn btn-pink  ">Add a new user</a>
</div>
@stop
<!-- Content -->
@section('content')

<section class="settings-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive default-table">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email Address</th>
								<th>User-Type</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							@forelse( $users as $user )
								<tr>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->role->name  }}</td>
									<td class="text-center">
										<a class="table-ctrls" href="{{ route('admin.settings.edit', $user) }}" title="edit">
											<span class="fa fa-pencil-square-o text-info"></span>
										</a>
										<a class="table-ctrls" href="{{ route('admin.settings.destroy', $user) }}" class="confirm" title="delete">
											<span class="fa fa-trash fa-2 text-danger"></span>
										</a>
									</td>
								</tr>
							@empty
								<tr><td colspan="4">Users not found.</td></tr>
							@endforelse		
						</tbody>
					</table>	
				</div>


			</div>
		</div>
	</div>
</section>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop