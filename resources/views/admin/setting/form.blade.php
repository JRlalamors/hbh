@include('partials.form-errors')

{!! Form::model($user, ['route' => $route, 'class' => 'default-form']) !!}
		
	<div class="row">
		<div class="col-xs-12 col-sm-6">							
			<div class="form-group">
				<label class="control-label" for="first_name">First Name<span class="important">*</span></label>
				{!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name']) !!}
			</div>			
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label class="control-label" for="last_name">Last Name<span class="important">*</span></label>
				{!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name']) !!}		
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label class="control-label" for="email">Email<span class="important">*</span></label>
				{!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
			</div>			
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label class="control-label" for="username">Username</label>
				{!! Form::text('username', null, ['class' => 'form-control', 'id' => 'username']) !!}
			</div>			
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label class="control-label" for="email">Password<span class="important">*</span></label>
				{!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}		
			</div>			
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label class="control-label" for="username">Confirm Password<span class="important">*</span></label>
				{!! Form::password('comfirm_password', ['class' => 'form-control', 'placeholder' => 'Re-type your Password']) !!}
			</div>			
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<label class="control-label" for="phone_num">Phone Number</label>
				{!! Form::text('phone_num', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<label class="control-label" for="role_id">Account Type</label>
				{!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'Please select User Role']) !!}
			</div>
		</div>

	
		<div class="col-xs-12">
			<div class="form-controls">
				<a class="btn btn-blue  " href="{{ route('admin.settings') }}">Cancel</a>
				<button class="btn btn-pink  ">Submit</button>
			</div>
		</div>
	</div>
{!! Form::close() !!}
