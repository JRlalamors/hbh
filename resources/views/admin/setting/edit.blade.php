@extends('layouts.default')


@section('body-class')
	settings dashboard
@stop

@section('page-title')
	Update User
@stop

@section('content')
<section class="settings-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
					
					@include('admin.setting.form', [
						'route' => ['admin.settings.update', $user],
						'user' => $user
					])


			</div>
		</div>
	</div>
</section>
@stop