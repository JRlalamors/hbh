@extends('layouts.default')


@section('body-class')
	settings dashboard
@stop

@section('page-title')
	Create New User
@stop

@section('content')
<section class="settings-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
					
					@include('admin.setting.form', [
						'route' => 'admin.settings.store',
						'user' => new App\User
					])


			</div>
		</div>
	</div>
</section>
@stop