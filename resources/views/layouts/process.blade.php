@include('layouts.partials.header')

	<div id="page-wrapper">
		<div id="page-content">				
			@yield('content')
		</div>			
	</div>

@include('layouts.partials.footer')