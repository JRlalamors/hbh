@include('layouts.partials.header')
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">		
					<h1>@yield('page-title')</h1>
					@yield('page-controls')
				</div>
			</div>
		</div>
	</section>

	<div id="page-wrapper">
		<div id="page-content">				
			@yield('content')
		</div>			
	</div>

@include('layouts.partials.footer')