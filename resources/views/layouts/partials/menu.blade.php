<nav class="page-nav">
    <ul class="list-inline">
        <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li><a href="{{ route('admin.agents') }}">Agents</a></li>
        <li><a href="{{ route('admin.customers') }}">Customers</a></li>
        <li><a href="{{ route('admin.process') }}">Process</a></li>
        <li class="dropdown"><a href="{{ route('admin.settings') }}"><span class="fa fa-cog"></span></a>
    		<ul class="dropdown-menu">
                <li><a href="{{ url('logout') }}">Logout</a></li>
            </ul>
        </li>
    </ul>
</nav>