	
	<footer id="footer">
		@if(Request::path() == '/')
			<div class="contact">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h2><a href="{{ $authUser && $authUser->isCustomer() ? route('customer.list.add') : route('register') }}">Find an agent you can trust.</a></h2>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="bottom">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-2 col-md-push-4 col-lg-2 col-lg-push-5">
						<div class="company-icon">
							<a href="
							@php
								if( $authUser ) {
                                    if( $authUser->isAdmin() ) {
                                        echo route('admin.dashboard');
                                    }
                                    else {
                                        echo route('home');
                                    }
                                }
                                else {
                                    echo route('home');
                                }
							@endphp
							"><img src="{{asset('images/logo-icon.svg')}}" alt="Home by Home"></a>
						</div>
					</div>
					<div class="col-xs-12   col-md-6 col-md-push-4 col-lg-5 col-lg-push-5">
						<div class="social-wrapper">
							<div class="social-list">
								<div class="item">
									<a href="https://twitter.com/homebyhomeCOM" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								</div>
								<div class="item">
									<a href="https://www.facebook.com/myhomebyhome" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								</div>
								<div class="item">
									<a href="https://www.instagram.com/homebyhomes/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								</div>
								<div class="item">
									<a href="https://www.youtube.com/channel/UCrtx-IF_yInmE_mXJ5T7Oag" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
						<div class="menu-wrapper">
							<div class="menu-list">
								<div class="item">
									<a href="{{ route('about') }}">About Us</a>
								</div>
								<div class="item">
									<a href="{{ route('contact') }}">Contact Us</a>
								</div>
								<div class="item">
									<a href="{{ route('terms') }}">Terms & Conditions</a>
								</div>
								<div class="item">
									<a href="{{ route('privacy') }}">Privacy Policy</a>
								</div>
								@if( !$authUser )
									<div class="item">
										<a href="{{ route('agent') }}">Are You An Agent?</a>
									</div>
								@endif
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4 col-md-pull-8 col-lg-5 col-lg-pull-7">
						<div class="copyright">© {{ date('Y') }} HOMEBYHOME.COM</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	@yield('afteElements')

	@yield('beforeScripts')
	
	<script>
	  	var baseUrl = document.querySelectorAll("meta[name=base-url]")[0].getAttribute('content');
	  	var jqueryUrl = baseUrl + '/bower_components/jquery/dist/jquery.min.js';
	  	var script = '<script src="' + jqueryUrl + '"><\/script>';
	  	window.jQuery || document.write(script);
	</script>

	{{--http://cdn.peerjs.com/0.3/peer.min.js--}}
	<script src="{{asset('bower_components/peer/peer.min.js')}}"></script>
	<script type="text/javascript">

		var baseUrl = document.querySelectorAll("meta[name=base-url]")[0].getAttribute('content');
	
		// verify csrf token
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});


		/**
		* PEER TO PEER CHAT
		*/
		var peer = new Peer({key: 'l2q67hfljdm6xbt9', secure : true}),
			$chatID;

		// establish peer chat
		if( peer )
		{
			// user connection
			peer.on('open', function(id) {

				var $chatID = id,
					$userType = '<?php echo $authUser && $authUser->isCustomer() ? 'customer' : 'agent'; ?>';

				// register new id
				$.ajax({
					type: 'POST',
					url: baseUrl + '/' + $userType + '/set-chat-id',
					data: {'chatID' : $chatID},
				});				

				// receive connection
				peer.on('connection', function(conn) {
					console.log(conn);					
				});

				// receiving messages
				peer.on('data', function(data) {
					console.log(data);
				});				

			});


			// connecting to new peer
			$('.chat-agent').click(function(){
				$userID = $(this).data('chatid');

				// get chat id
				$.ajax({
					type: 'POST',
					url: baseUrl + '/customer/get-agent-chatid',
					data: {'userID' : $userID},
					success: function($userID) {

						if( $userID )
						{
							var conn = peer.connect($userID);
						}

					}
				});

			});

		}

	</script>

	@yield('afterJquery')
    	<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    	<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>  
    	<script src="{{asset('bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>
    	<script src="{{asset('bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
		<script src="{{asset('js/jquery.carouFredSel-6.2.1.js')}}"></script>
		<script src="{{asset('js/jquery.matchHeight.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/vendor-scripts.js') }}"></script>
    	<script src="{{asset('js/app.js')}}?v=1312018"></script>
    @yield('scripts')
</body>
</html>