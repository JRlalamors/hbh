@extends('layouts.default')

@section('styles')
	<style type="text/css">
		.panel-group a {
			display: block;			
			padding: 20px 15px;
		}

		.panel-group a:hover, 
		.panel-group a:focus {
			text-decoration: none;
		}

		.panel-group .panel-heading {
			padding: 0;
		}
	</style>
@stop

@section('body-class')
	customer-profile pagetitle-off
@stop

@section('content')
<section id="customer-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="photo"><img class="img-responsive" src="{{ $customer->asUser->profile_photo ? $customer->asUser->profile_photo : asset('images/user-blank.svg') }}"></div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="info">
					<h1 class="name">{{ $customer->asUser->name }}</h1>
					@if( $customer->asUser->city )
						<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $customer->asUser->city . " " . $customer->asUser->state }}</div>
					@endif
					<div class="moreinfo">
						<div class="listitem">
							<div class="title">Email</div><div class="content"><a href="mailto:{{ $customer->asUser->email }}">{{ $customer->asUser->email }}</a></div>
						</div>
						<div class="listitem">
							<div class="title">City</div><div class="content">{{ $customer->asUser->city ?: '--' }}</div>
						</div>
						<div class="listitem">
							<div class="title">State</div><div class="content">{{ $customer->asUser->state ?: '--' }}</div>
						</div>
						<div class="listitem">
							<div class="title">Phone</div><div class="content"><a href="tel:@phoneNum($authUser->phone_num)">{{ $customer->asUser->phone_format }}</a></div>
						</div>
					</div>
					<div class="ctrls">
						<a href="" class="btn btn-pink btn-round hvr-rectangle-out">Message</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="customer-moreinfo" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-push-4">

				@if( $listings->count() > 0 )

					<div class="panel-group" id="accordion">
						@foreach( $listings as $list )
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $list->id }}">{{ $list->property->human_display }}</a>
									</h4>
								</div>
								<div id="collapse{{ $list->id }}" class="panel-collapse collapse {{ $loop->first ? 'in' : '' }}">
									<div class="panel-body profile cp-container">
										<div class="listitem">
											<div class="title">Are you looking to buy a home or sell a home?</div>
											<div class="content">{{ $list->list_type == 'buyer' ? 'Sell Home' : 'Buy Home' }}</div>
										</div>
										<div class="listitem">
											<div class="title">Where are you looking to buy / sell?</div>
											<div class="content">{{ $list->complete_address }}</div>
										</div>
										<div class="listitem">
											<div class="title">Home much are you hoping to spend/sell on a home?</div>
											<div class="content">
												@if( $list->budget )
													{{ $list->budget }}
												@else
													{{ 'N/A' }}
												@endif
											</div>
										</div>
										<div class="listitem">
											<div class="title">What kind of property are you buying/selling?</div>
											<div class="content">{{ $list->property->human_display }}</div>
										</div>
										<div class="listitem">
											<div class="title">How soon are you looking to buy/sell?</div>
											<div class="content">
												@if($list->text_until)
													{{ $list->text_until }}
												@else
													@dateFormat( $list->until )
												@endif
											</div>
										</div>
										<div class="listitem">
											<div class="title">Are you pre-approved by a lender?</div>
											<div class="content">{{ $customer->lender_status }}</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>

				@else

					<div class="profile cp-container">
						Listings not found..
					</div>

				@endif

			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-8">
				<div class="sidebar cp-container">
					<div class="listitem">
						<div class="title">Date Signed Up</div>
						<div class="content">@dateFormat($authUser->created_at)</div>
					</div>
					{{--<div class="listitem">
						<div class="title">Have They Selected An Agent</div>
						<div class="content">Yes</div>
					</div>
					<div class="listitem">
						<div class="title">Proposal Submitted</div>
						<div class="content">September 10, 2017</div>
					</div>--}}
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
@stop