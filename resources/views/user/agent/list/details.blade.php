@extends('layouts.default')

@section('styles')
@stop

@section('body-class')
	agent-profile pagetitle-off
@stop


@section('content')
<section id="agent-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="profile">
					<div class="item">
						<div class="photo">
							<img class="img-responsive" src="{{ isset($resizedPhoto) && $resizedPhoto ? $resizedPhoto : asset('images/user-blank.svg') }}">
						</div>
					</div><div class="item">
						<div class="info">

							<h1 class="name">
								{{ $list->customer->asUser->name }}
							</h1>
							{{--<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> Default Location, Address Here</div>--}}
							<div class="email">
								<label>Email</label>
								<a href="mailto:{{ $list->customer->asUser->email }}">{{ $list->customer->asUser->email }}</a>
							</div>
							<div class="city">
								<label>City</label>
								<span class="">{{ $list->customer->asUser->city ?: '--' }}</span>
							</div>
							<div class="state">
								<label>State</label>
								<span class="">{{ $list->customer->asUser->state ?: '--' }}</span>
							</div>
							<div class="phone">
								<label>Phone</label>
								<a href="tel:@phoneNum($list->customer->asUser->phone_num)">{{ $list->customer->asUser->phone_format }}</a>								
							</div>
						</div>
					</div><div class="item">
						<div class="buttons">
							<a href="{{ route('agent.customer.profile', $list->customer) }}" class="btn btn-pink btn-round hvr-sweep-to-left">View Profile</a>
							{{--@if( !$proposal )
								<a href="{{ route('agent.list.add-proposal', $list) }}" class="btn btn-blue btn-round hvr-sweep-to-left">Sumbit Proposal</a>
							@else
								<a href="{{ route('agent.list.edit-proposal', [$list, $proposal]) }}" class="btn btn-blue btn-round hvr-sweep-to-left">Edit Proposal</a>
							@endif--}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="agent-content">
	<div class="container">
		<div class="row">

			<div class="col-xs-12">
				@include('partials.form-success')
			</div>

			<div class="col-xs-12 col-md-5 col-lg-4">
				<div class="info-wrapper widget-wrapper">
					<div class="widget hidden">
						<h2 class="heading">SOCIAL PROFILES</h2>
						<div class="widget-content">
							<ul class="social-list">
								<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="widget">
						<h2 class="heading">Date Signed Up</h2>
						<div class="widget-content">
							<div class="date">{{ $list->customer->asUser->created_at->format('F d, Y') }}</div>
						</div>
					</div>
					<div class="widget">
						<h2 class="heading">Proposal Submitted</h2>
						<div class="widget-content">
							@php
								$latestProposal = $list->proposals->sortByDesc('created_at')->first();
							@endphp
							@if ($latestProposal)
								<div class="date">{{ $latestProposal->created_at->format('F d, Y') }}</div>
							@else
								--
							@endif
						</div>
					</div>
					<div class="widget">
						<h2 class="heading">Have They Selected An Agent</h2>
						<div class="widget-content">
							<div class="date">{{ $list->selectedAgent ? 'Yes' : 'Not Yet' }}</div>
						</div>
					</div>
					<div class="widget">
						<h2 class="heading">Looking For Or Around</h2>
						<div class="widget-content">
							<div class="date">{{ $list->zip_postal }}</div>
						</div>
					</div>
					<div class="widget">
						<h2 class="heading">Budget</h2>
						<div class="widget-content">
							<div class="date">{{ $list->text_budget }}</div>
						</div>
					</div>
				</div>

			</div>

			<div class="col-xs-12 col-md-7 col-lg-8">
				<div class="info-wrapper proposal-wrapper">
					<h2 class="heading wunderline">Submit A Proposal</h2>
					{!! Form::model($proposal, [
						'method' => $proposal ? 'POST' : 'POST',
						'url' => $proposal ? route('agent.list.update-proposal', [$list, $proposal]) : route('agent.list.send-proposal', $list),
						'class' => 'default-form'
					]) !!}
						<div class="form-group">
							<label>I'm willing to offer a </label>
							@php
								if( $list->list_type == 'seller' )
                                {
                                    $proposalType = [
                                        'percentage' => 'Total Commission Rate',
                                        'fixed' => 'Flat Fee'
                                    ];
                                } else {
                                    $proposalType = [
                                        'percentage' => 'Commission Rebate',
                                        'fixed' => 'Cash Rebate'
                                    ];
                                }
							@endphp
							{!! Form::select('rebate_type', $proposalType, $proposal ? $proposal->rebate_type : null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label>Rebate:</label>
							<div class="input-group">
								<div class="dollar input-group-addon"><i class="fa fa-dollar"></i></div>
								{!! Form::number('rebate', $proposal ? $proposal->rebate : null, ['class' => 'form-control', 'min' => 1, 'max' => 99, 'step' => 'any']) !!}
								<div class="percentage input-group-addon"><i class="fa fa-percent"></i></div>
							</div>
						</div>
						<div class="form-group">
							<label>What do I do better that most agents?</label>
							{!! Form::textarea('propose_1', $proposal && $proposal->getOtherData('propose_1') ? $proposal->getOtherData('propose_1') : $agent->getOtherData('question_1'), ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label>Why should you hire me?</label>
							{!! Form::textarea('propose_2', $proposal && $proposal->getOtherData('propose_2') ? $proposal->getOtherData('propose_2') : $agent->getOtherData('question_2'), ['class' => 'form-control']) !!}
						</div>
						<div class="form-ctrls text-right">
							<button type="submit" class="btn btn-pink btn-round hvr-sweep-to-right">{{ $proposal ? 'UPDATE PROPOSAl' : 'SUBMIT' }}</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			</div>
		</div>
	</div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')
	<script type="text/javascript">
        $(document).ready(function(){

            updateRebateInput($('[name="rebate_type"]').val());
            $('[name="rebate_type"]').change(function(){
                $type = $(this).val();

                updateRebateInput($type);
            });

            function updateRebateInput($type) {
                if ($type == 'fixed') {
                    $('[name="rebate"]').removeAttr('max');
                    $('.dollar').removeClass('hidden');
                    $('.percentage').addClass('hidden');
                } else {
                    $('[name="rebate"]').attr('max', 99);
                    $('.percentage').removeClass('hidden');
                    $('.dollar').addClass('hidden');
                }
            }

        });
	</script>
@stop