@include('partials.form-errors')

{!! Form::model($proposal, ['url' => $route, 'class' => 'default-form']) !!}
	<div class="form-group">
		<label>Proposal Type:</label>
		@php
			if( $list->list_type == 'seller' )
			{
				$proposalType = [
					'percentage' => 'Total Commission Rate',
					'fixed' => 'Flat Fee'
				]; 
			} else {
				$proposalType = [
					'percentage' => 'Commission Rebate',
					'fixed' => 'Cash Rebate'
				];
			}
		@endphp

		{!! Form::select('rebate_type', $proposalType, $proposal->rebate_type, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Rebate:</label>
		<div class="input-group">
			<div class="dollar input-group-addon"><i class="fa fa-dollar"></i></div>
			{!! Form::number('rebate', $proposal->rebate, ['class' => 'form-control', 'min' => 1, 'max' => 99, 'step' => 'any']) !!}
			<div class="percentage input-group-addon"><i class="fa fa-percent"></i></div>
		</div>
		{{--<div class="dollar col-xs-1 text-center hidden">$</div>
		<div class="col-xs-11 rebate-col">
			{!! Form::number('rebate', $proposal->rebate, ['class' => 'form-control', 'min' => 1, 'max' => 6, 'step' => 'any']) !!}
		</div>
		<div class="percentage col-xs-1 text-center">%</div>--}}
	</div>

	<div class="form-group">
		<label>What Do I Do Better Than Most Agents?</label>
		{!! Form::textarea('propose_1', $proposal->getOtherData('propose_1') ? $proposal->getOtherData('propose_1') : $agent->getOtherData('question_1'), ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Why should you hire me?</label>
		{!! Form::textarea('propose_2', $proposal->getOtherData('propose_2') ? $proposal->getOtherData('propose_2') : $agent->getOtherData('question_2'), ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<button class="btn btn-success">{{ $formType == 'add' ? 'Send' : 'Update' }}</button>
		<a href="{{ route('agent.list.details', $list) }}" class="btn btn-danger">Cancel</a>
	</div>
{!! Form::close() !!}

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){

        updateRebateInput($('[name="rebate_type"]').val());
		$('[name="rebate_type"]').change(function(){
			$type = $(this).val();

            updateRebateInput($type);
		});

		function updateRebateInput($type) {
		    if ($type == 'fixed') {
                $('[name="rebate"]').removeAttr('max');
                $('.dollar').removeClass('hidden');
                $('.percentage').addClass('hidden');
			} else {
		        $('[name="rebate"]').attr('max', 99);
                $('.percentage').removeClass('hidden');
                $('.dollar').addClass('hidden');
			}
		}

	});
</script>
@stop