@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
<style type="text/css">
	.dollar, .percentage {
		padding: 15px;
		background: #e7e7e7;
		height: 51px;
	}

	.dollar {
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
		margin-right: -30px;
		z-index: 1;
	}

	.percentage {
		border-top-right-radius: 5px;
		border-bottom-right-radius: 5px;
		margin-left: -30px;
		z-index: 1
	}

	.rebate-col {
		z-index: 0
	}

	.dollar.hidden ~ .rebate-col {
		padding-left: 0;
	}

	.percentage.hidden ~ .rebate-col {
		padding-right: 0;
	}

	[name="rebate"] {
		height: 51px;
	}
</style>
@stop

<!-- Page Title -->
@section('page-title')
	Add Proposal
@stop

<!-- Content -->
@section('content')	

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				@include('user.agent.list.form', [
					'route' => route('agent.list.send-proposal', $list),
					'proposal' => new \App\Model\Proposal,
					'agent' => $agent,
					'formType' => 'add',
					'list' => $list
				])
			</div>
		</div>
	</div>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop