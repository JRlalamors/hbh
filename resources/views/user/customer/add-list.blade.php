@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	agent-page agent-register footer-off
@stop

<!-- Content -->
@section('content')	
<div class="container">

	<div class="row">

		<div class="col-xs-12">
		
			@include('partials.form-step', [
				'form' => 'customer-addlist',
			])

		</div>
		
	</div>

</div> 
	
@stop