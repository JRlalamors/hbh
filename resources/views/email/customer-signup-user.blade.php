@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <h3 class="beginning-text">Hello {{$customer->asUser->name}},</h3>
                    <p>Thank you for registering for Home By Home – a service helping home buyers and sellers save money while finding a qualified, experienced agent. Take some time to explore our site and brush up on your real estate FAQs. Meanwhile, we'll be busy finding you the right agents who will be contacting you shortly with a customized proposal just for you.</p><br><br>
                    <p>In the meantime, if you have any questions, don’t hesitate to email us <a href="mailto:support@homebyhome.com">support@homebyhome.com</a> or call us at <a href="tel:8556008117" style="color: #000;">(855) 600-8117</a>.</p>
                    <br>
                    Thanks again,<br>
                    <img src="https://www.homebyhome.com/images/emails/hbh-signature.png" style="margin-top: 15px">
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent