@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <p><strong>Name: </strong> {{ $firstname . ' ' . $lastname }}</p>
                    <p><p><strong>Email: </strong> {{ $email }}</p></p>
                    <p><strong>Company: </strong> {{ $company }}</p>
                    <p><strong>Phone: </strong> {{ $phone }}</p>
                    <p><strong>Message: </strong>{{ $comment }}</p>
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent