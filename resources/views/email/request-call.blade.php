@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <p><strong>Name: </strong> {{ $name }}</p>
                    <p><strong>Email: </strong> {{ $email }}</p>
                    <p><strong>Phone: </strong> {{ $phone }}</p>
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent