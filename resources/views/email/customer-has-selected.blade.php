@component('mail::message')
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container1"><div class="content2">
					<h3 class="beginning-text">Hi {{ $agent->asUser->name }}</h3>
					<p>This is to inform you that Client Listing "{{ $list->property->human_display }}" response about the agents listed has been submitted.</p>
					<p>Thank you for sending a proposal. Unfotunately another proposal was selected. Looking forward to your future parcitipation on <a href="https://www.homebyhome.com/">HomebyHome.com</a></p>
					<br>
					Thanks again,<br>
			</div></td>
			<td></td>
		</tr>
	</table>
@endcomponent