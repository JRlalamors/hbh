@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <p>{{ $message }}</p>
                    <p><strong>Name: </strong> {{ $customer->name }}</p>
                    <p><strong>Email: </strong> {{ $customer->email }}</p>
                    <p><strong>Phone: </strong> {{ $customer->phone_format }}</p>
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent