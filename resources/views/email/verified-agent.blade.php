@component('mail::message')
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container1"><div class="content2">
				<h3 class="beginning-text">Hi, {{ $agent->asUser->name }}</h3>
				<p>Congratulations! Our team has reviewed your profile and we are pleased to extend our warmest welcome into the Home By Home family. By working together, we can make the real estate process better for all parties. (And who doesn't love parties?)</p>
				<p>You may now access your account at homebyhome.com.</p>
				<p>Psst! Here's your password: {{ $password }}</p>
				<p>In the meantime, if you have any questions, don’t hesitate to email us <a href="mailto:support@homebyhome.com">support@homebyhome.com</a> or call us at <a href="tel:8556008117">(855) 600-8117</a></p>
				<br>
				Thanks again,<br>
				</div></td>
			<td></td>
		</tr>
	</table>
@endcomponent