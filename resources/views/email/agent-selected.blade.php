@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2" bgcolor="#ffffff">
                    <h3>Good News {{ $agent->asUser->name }}!</h3>
                    <p>You've been selected to submit a proposal for {{ $customer->asUser->name }}</p>
                    <p>You can find this information on your dashboard at HomeByHome.com as well as below. {{ $customer->asUser->name }} is looking to {{ $listing->list_type == 'seller' ? 'Sell' : 'Buy' }} a home that is:</p>
                    <p><label>Sq. Ft.: </label> {{ $listing->square_feet }}</p>
                    <p><label>Bedroom: </label> {{ $listing->bedrooms }}</p>
                    <p><label>Bathrooms: </label> {{ $listing->bathrooms }}</p>
                    <p><label>Zip Postal: </label> {{ $listing->zip_postal }}</p>
                    <p><label>Budget: </label>
                        @if($listing->text_budget)
                            {{ $listing->text_budget }}
                        @else
                            {{ $listing->budget }}
                        @endif
                    </p>
                    <p><label>Buying Until: </label>
                        @if($listing->text_until)
                            {{ $listing->text_until }}
                        @else
                            @dateFormat($listing->until)
                        @endif
                    </p>
                    <br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                    Thanks again,<br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent