@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1" style=""><div class="content2">
                    <p><label>Name: </label> {{ $customer->asUser->name }}</p>
                    <p><label>Email: </label> {{ $customer->asUser->email }}</p>
                    <p><label>Phone #: </label> {{ $customer->asUser->phone_num }}
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent