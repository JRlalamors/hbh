@component('mail::message')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <h3 class="beginning-text">New Agent Registration!</h3>
                    <p><label>Name: </label> {{ $agent->asUser->name }}</p>
                    <p><label>Email: </label> {{ $agent->asUser->email }}</p>
                    <p><label>Phone #: </label> {{ $agent->asUser->phone_num }}</p>
                    <br>
                    Thanks again,<br>
                </div></td>
            <td></td>
        </tr>
    </table>
@endcomponent