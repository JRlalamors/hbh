@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
    
@stop

@section('body-class')
    account-login pagetitle-off footer-off
@stop

<!-- Content -->
@section('content') 

<section id="account-login" class="hero-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
               
                <div class="panel panel-default">
                    
                    <div class="panel-body">
                       
                        <div class="panel-right">
                            <div class="panel-logo"><img src="{{asset('images/logo-login.svg')}}" alt="Home by Home"></div>
                            <h2 class="panel-heading">Reset Password</h2>
                            
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                             <form class="default-form" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                   
                                   <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Your Email">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  
                                </div>

                                
                                <div class="form-ctrl">
                                    <button type="submit" class="btn btn-block btn-pink  ">Send Password Reset Link</button>
                               
                                </div>

                            </form>   
                        </div>

                    </div>
                 </div>

    
               
            </div>
        </div>
    </div>
</section>

@stop

<!-- Added Scripts -->
@section('scripts')
<script type="text/javascript">

    $(document).ready(function(){
        windowResize();

        $(window).resize(function(){
            windowResize();
        });
        $(window).load(function(){
            $(window).resize();
        });
    });

    function windowResize(){
        var WindowHeight = $(window).height(),
            HeaderHeight = $('#header').outerHeight(),
            FooterHeight = $('#footer').outerHeight();

        $('#page-wrapper').each(function(){
        

            var ContainerHeight = $(this).outerHeight();
            if( WindowHeight > ( ContainerHeight + HeaderHeight + FooterHeight ) ) {

                TotalPadding = ( WindowHeight - ( ContainerHeight + HeaderHeight + FooterHeight ) ) / 2 ;

                

            }else{

                TotalPadding = 0;
            }

            $(this).css('padding-top', TotalPadding+'px').css('padding-bottom', TotalPadding+'px')
        });
    }
</script>
@stop