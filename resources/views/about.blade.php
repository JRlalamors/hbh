@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
    page-about pagetitle-off footer-off
@stop

<!-- Content -->
@section('content')	
<section id="ab-banner-area" style="background-image:url({{asset('images/bg-about.png')}})">

	<div class="container">
        <div class="row">
            <div class="col-xs-12">
            	<h1 class="title"><span>Make buying</span> <span>or selling</span> <span>a home simple</span></h1>  

            	<div class="caption">
            		<div class="image"><img src="{{asset('images/ab-banner-icon.png')}}" alt="banner-icon" class="img-responsive"></div>
            		<div class="text">Combines technology, innovative software, and expert customer service to make buying or selling a home fast and easy.</div>
            	</div>
             </div>
        </div>
    </div>
</section>
<section id="ab-content-area">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-5 col-lg-4">
				<div class="image"><img src="{{asset('images/ab-content-thumbnail.png')}}" alt="banner-icon" class="img-responsive"></div>
			</div>
			<div class="col-xs-12 col-md-7 col-lg-8">
				<div class="entry-content">
					<p>I bought my first home at age 25, and like many first-time homeowners, I was nervous about the unfamiliar process of buying my first house. Still, I scraped together enough money for a down payment and slowly explored the world of real estate.</p>

					<p>What did I discover? With the incredible resources online, the home-buying process wasn’t as intimidating as I originally thought. I set up showings for homes that interested me, asked pertinent questions, and ultimately purchased the perfect townhouse in Provo, Utah, for me and my soon-to-be wife.</p>

					<p>A couple of years later, with a baby on the way, we put the townhouse on the market, for sale by owner. We created our own fliers and a simple website, staged the house, and scheduled showings, just as an agent would have done in our stead. We found the necessary documents online to facilitate the sale, and three showings later, we found our buyer! To make things even better, our buyers didn’t have an agent either, so we didn’t have to pay any sales commission (a savings of about $12,000).</p>

					<p>Since that time, we have bought two new homes without the help of an agent, which allowed us to negotiate the prices down (by over $20,000). We have since sold both houses without the help of an agent (saving us over $25,000). Throughout the process of buying and selling these homes, there were several moments when we wished we had the help of a licensed real estate agent while still being able to save thousands of dollars.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="ab-hero-area">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>That is why I have created Home By Home, to get licensed real estate agents to compete for your business by offering rebates (at closing), paired with customized proposals.</h2>
				<h2>HomeByHome.com can help you find a great real estate agent and save you thousands of dollars for your next home purchase or sale.</h2>

				<a href="{{ $authUser && $authUser->isCustomer() ? route('customer.list.add') : route('register') }}" class="btn btn-pink btn-round">Why not get started? Just fill out your information today!</a>
			</div>
		</div>
	</div>
</section>
<section id="ab-links-area">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<a href="https://www.homebyhome.com/blog/home-sellers-guide/" class="link-item">
					<div class="image"  style="background-image:url({{asset('images/ab-links-1.jpg')}})"></div>
					<h2 class="link-blue">I’M SELLING MY HOME, DO I NEED A REALTOR?</h2>
				</a>
			</div>
			<div class="col-xs-12 col-md-4">
				<a href="https://www.homebyhome.com/blog/first-time-home-buyers-guide-things-know/" class="link-item">
					<div class="image" style="background-image:url({{asset('images/ab-links-2.jpg')}})"></div>
					<h2 class="link-lblue">BUYING A HOME? DO YOU REALLY NEED A REALTOR?</h2>
				</a>
			</div>
			<div class="col-xs-12 col-md-4">
				<a href="https://www.homebyhome.com/blog/first-time-home-buyers-guide-things-know/" class="link-item">
					<div class="image" style="background-image:url({{asset('images/ab-links-3.jpg')}})"></div>
					<h2 class="link-pink">A FIRST-TIME HOME BUYER’S GUIDE TO BUYING</h2>
				</a>
			</div>	
		</div>
	</div>
</section>
<section id="ab-call-area">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Please email or call us (855-600-8117) with any questions.</h2>
			</div>
		</div>
	</div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop