@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
    page-success pagetitle-off footer-off
@stop


<!-- Content -->
@section('content')	
<section id="success-content" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<div class="content">
					<h1 class="heading">Thank You</h1>
					<h3 class="heading-sub">for Choosing <strong>Home by Home</strong></h3>
					<div class="image"><img class="img-responsive align-center" src="{{asset('images/thumbnail-thankyou.png')}}" alt="Email" class="align-center img-responsive"></div>
					<p>A member of our Home By Home Agent is reviewing your application and will reach out to you within the next 24 hours. For faster review, please call us at <a href="tel:8556008117">(855)600-8117</a>.</p>
				</div>
			</div>
		</div>
	</div>
</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop