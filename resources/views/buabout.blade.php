@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
    page-about pagetitle-off footer-off
@stop

<!-- Content -->
@section('content')	
	<section id="about-banner-area" style="background-image:url({{asset('images/banner-about.jpg')}})">
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 text-center">
	            <p>ABOUT</p>
	            <h1 class="banner-title"><span>Home</span> <span>by</span> <span>Home</span></h1>  
	             </div>
	        </div>
	    </div>
	</section>
	<section id="about-content" class="default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2 class="content-heading">Make buying or selling a home simple</h2>
					<h3>Combines technology, innovative software, and expert customer service to make buying or selling a home fast and easy.</h3>
					<p>I bought my first home at age 25, and like many first-time homeowners, I was nervous about the unfamiliar process of buying my first house. Still, I scraped together enough money for a down payment and slowly explored the world of real estate.</p>
 
					<p>What did I discover? With the incredible resources online, the home-buying process wasn’t as intimidating as I originally thought. I set up showings for homes that interested me, asked pertinent questions, and ultimately purchased the perfect townhouse in Provo, Utah, for me and my soon-to-be wife.</p>
 
					<p>A couple of years later, with a baby on the way, we put the townhouse on the market, for sale by owner. We created our own fliers and a simple website, staged the house, and scheduled showings, just as an agent would have done in our stead. We found the necessary documents online to facilitate the sale, and three showings later, we found our buyer! To make things even better, our buyers didn’t have an agent either, so we didn’t have to pay any sales commission (a savings of about $12,000).</p>
 
					<p>Since that time, we have bought two new homes without the help of an agent, which allowed us to negotiate the prices down (by over $20,000). We have since sold both houses without the help of an agent (saving us over $25,000). Throughout the process of buying and selling these homes, there were several moments when we wished we had the help of a licensed real estate agent while still being able to save thousands of dollars.</p>
 
					<p>That is why I have created Home By Home, to get licensed real estate agents to compete for your business by offering rebates (at closing), paired with customized proposals.</p>
 
					<p>HomeByHome.com can help you find a great real estate agent and save you thousands of dollars for your next home purchase or sale.</p>
 
					<p>Why not get started? Just fill out your information today! </p>

					<p><img src="{{asset('images/thumbnail-about.jpg')}}" alt="About Home by Home" class="align-center img-responsive"></p>

					<a class="btn btn-blue  " href="#">Meet the Team</a>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12"><hr class="global-divider"></div>
		</div>
	</div>

	<section id="about-qlinks" class="default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 text-center">
					<div class="link-item">
						<div class="image"><a href=""><img src="{{asset('images/thumbnail-contact.jpg')}}" alt="Contact Us" class="align-center img-responsive"></a></div>
						<h3><a href="">Contact Home By Home</a></h3>
						<p>We love hearing from you!</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 text-center">
					<div class="link-item">
						<div class="image"><a href=""><img src="{{asset('images/thumbnail-team.jpg')}}" alt="Contact Us" class="align-center img-responsive"></a></div>
						<h3><a href="">Join the team</a></h3>
						<p>Help us redefine the industry</p>
					</div>
				</div>
			</div>
		</div>
	</section>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop