@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

<!-- Content -->
@section('content')	

	<a href="{{ route('register', ['register_as' => 'buyer']) }}" class="btn btn-primary">Find an Agent</a>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop