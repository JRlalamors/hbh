@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop


<!-- Content -->
@section('content') 
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="{{ route('agent.fb-register', $user) }}" class="btn btn-blue">Register as Agent</a> or <a href="{{ route('register.fb-register', $user) }}" class="btn btn-blue">Register as Customer</a>
            </div>
        </div>
    </div>
@stop


<!-- Added Scripts -->
@section('scripts')

@stop