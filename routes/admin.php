<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function() {

	/*
	* Dashboard
	**/
	Route::get('/', 'DashboardController@index')->name('dashboard');

	/*
	* Deals
	**/
	Route::group(['prefix' => 'deals'], function() {
		Route::get('/', 'DealsController@index')->name('deals');
		Route::get('{list}/details', 'DealsController@details')->name('deals.details');
	});

	/*
	* Agents
	**/
	Route::group(['prefix' => 'agents'], function() {
		Route::get('/', 'AgentsController@index')->name('agents');
		Route::get('review', 'AgentsController@reviewAgents')->name('agents.review');
		Route::get('view', 'AgentsController@viewAgents')->name('agents.view');
		Route::get('deals', 'AgentsController@deals')->name('agents.deals');
		Route::post('search-results', 'AgentsController@searchResults')->name('agents.search-results');
		Route::get('{agent}/profile/{status?}', 'AgentsController@profile')->name('agents.profile');
		Route::post('{agent}/profile/store', 'AgentsController@storeProfile')->name('agents.profile.store');
		Route::post('{agent}/profile/store-qa', 'AgentsController@storeQA')->name('agents.profile.storeQA');
		Route::post('{agent}/profile/store-social', 'AgentsController@storeSocial')->name('agents.profile.storeSocial');
		Route::get('{agent}/profile/list/{list}/details', 'AgentsController@listDetails')->name('agents.profle.list-details');
		
		// ajax functions 
		Route::post('{agent}/approve', 'AgentsController@approve');
		Route::post('{agent}/reject', 'AgentsController@reject');
	});

	/*
	* Customer
	**/
	Route::group(['prefix' => 'customers'], function() {
		Route::get('/', 'CustomersController@index')->name('customers');
		Route::get('screen', 'CustomersController@customersToScreen')->name('customers.to-screen');
		Route::get('all', 'CustomersController@allCustomers')->name('customers.all');
		Route::get('without-agent', 'CustomersController@customersWithoutAgent')->name('customers.without-agent');
		Route::post('search-results', 'CustomersController@searchResults')->name('customers.search-results');
		Route::get('{customer}/profile/{status?}', 'CustomersController@profile')->name('customers.profile');
		Route::post('{customer}/profile/store', 'CustomersController@storeProfile')->name('customers.save-profile');
		Route::get('{customer}/profile/list/{list}/details', 'CustomersController@listDetails')->name('customers.profile.list-details');

		// Ajax
        Route::get('list/create-list', 'CustomersController@creatingList');
        Route::post('list/update-list', 'CustomersController@updateList');
        Route::get('list/{list}/edit-list', 'CustomersController@getList');
		Route::post('list/{list}/edit-agents', 'CustomersController@editAgents');
		Route::post('list/{list}/store-agents', 'CustomersController@storeAgents');
		Route::post('list/{list}/store-list-agents', 'CustomersController@storeListAgents');
		Route::post('{customer}/approve', 'CustomersController@approve');
		Route::post('{customer}/reject', 'CustomersController@reject');

	});

	/*
	* Process
	**/
	Route::group(['prefix' => 'process'], function() {
		Route::get('/', 'ProcessController@index')->name('process');
		Route::get('deals', 'ProcessController@deals')->name('process.deals');
	});

	/*
	* Settings
	**/
	Route::group(['prefix' => 'settings'], function(){
		Route::get('/', 'SettingsController@index')->name('settings');
		Route::get('create', 'SettingsController@create')->name('settings.create');
		Route::post('store', 'SettingsController@store')->name('settings.store');
		Route::get('{user}/edit', 'SettingsController@edit')->name('settings.edit');
		Route::post('{user}/update', 'SettingsController@update')->name('settings.update');
		Route::get('{user}/destroy', 'SettingsController@destroy')->name('settings.destroy');
	});

});