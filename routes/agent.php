<?php 

Route::group(['prefix' => 'agent', 'as' => 'agent.'], function() {

	// account
	Route::get('account', 'AgentController@account')->name('account');
	Route::post('account/set-zillow-name', 'AgentController@setZillowName')->name('account.set-zillow-name');
	Route::post('account/list/{list}/details', 'AgentController@accountListDetails');
	Route::post('account/list/{list}/submit-proposal', 'AgentController@accountSubmitProposal');
	Route::post('account/save-settings', 'AgentController@saveSettings')->name('account.settings.update');
	
	// profile
	Route::get('profile', 'AgentController@profile')->name('profile');
	Route::post('profile/update', 'AgentController@updateProfile')->name('profile.update');
	Route::post('profile/update-qa', 'AgentController@updateProfileQA')->name('profile.update-qa');

	// proposals
	Route::get('list/{list}/details', 'AgentController@listDetails')->name('list.details');
    Route::get('list/{list}/details/customer', 'AgentController@accountListCustomerDetails')->name('list.customer-details');
	Route::get('list/{list}/add-proposal', 'AgentController@addProposal')->name('list.add-proposal');
	Route::post('list/{list}/send-proposal', 'AgentController@sendProposal')->name('list.send-proposal');
	Route::get('list/{list}/proposal/{proposal}/edit', 'AgentController@editProposal')->name('list.edit-proposal');
	Route::post('list/{list}/proposal/{proposal}/update', 'AgentController@updateProposal')->name('list.update-proposal');

	// customer
    Route::get('customer/{customer}/profile', 'AgentController@customerProfile')->name('customer.profile');
    
	// chat
	Route::post('set-chat-id', 'AgentController@setChatID');
});