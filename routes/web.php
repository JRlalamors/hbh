<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Model\Agent;

Route::get('testmail', function() {
	$agent = \App\Model\Agent::findOrFail(28);
	Mail::to('jessie.sytian@gmail.com')->send(new \App\Mail\AgentSignupMail($agent));
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/login', 'HomeController@userLogin')->name('login');

Route::get('admin/login', 'HomeController@adminLogin')->name('admin.login');
	
/*
* Facebook Login
**/
Route::group(['prefix' => 'facebook'], function() {
	Route::get('login', 'Auth\LoginController@redirectToProvider')->name('facebook.login');
	Route::get('authenticated', 'Auth\LoginController@fbAuth')->name('facebook.authenticated');
});

/*
*  Public View Routes
**/
Route::get('/', 'HomeController@home')->name('home');

/*
* Buyers
**/
Route::group(['prefix' => 'buyers'], function() {
	Route::get('/', 'BuyersController@index')->name('buyers');
	Route::get('find-an-agent', 'BuyersController@findAgent')->name('buyers.find-agent');
	Route::get('guide', 'BuyersController@guide')->name('buyers.guide');
	Route::get('process', 'BuyersController@process')->name('buyers.process');
});

/*
* Sellers
**/
Route::group(['prefix' => 'sellers'], function() {
	Route::get('/', 'SellersController@index')->name('sellers');
	Route::get('find-an-agent', 'SellersController@findAgent')->name('sellers.find-agent');
	Route::get('guide', 'SellersController@guide')->name('sellers.guide');
	Route::get('process', 'SellersController@process')->name('sellers.process');
});

/*
* Agent
**/
Route::group(['prefix' => 'agent'], function() {
	Route::get('/', 'AgentsController@index')->name('agent');
	Route::get('register', 'AgentsController@register')->name('agent.register');	
	Route::post('validate-form', 'AgentsController@validateForm');
	Route::post('submit-registration', 'AgentsController@submitRegistration')->name('agent.submit-registration');
	Route::get('register-success', 'AgentsController@success');

	// fb registration
	Route::get('{user}/fb-register', 'AgentsController@fbRegister')->name('agent.fb-register');
});

Route::get('about-us', 'HomeController@about')->name('about');
Route::get('terms-and-condition', 'HomeController@terms')->name('terms');
Route::get('privacy-policy', 'HomeController@privacy')->name('privacy');
Route::get('resources', 'HomeController@resources')->name('resources');
Route::get('contact-us', 'HomeController@contact')->name('contact');
Route::get('find-an-agent', 'HomeController@findAgent')->name('find-agent');

// request a call / contact us
Route::post('request-call', 'HomeController@requestCall')->name('request-call');
Route::post('contact-us', 'HomeController@contactUs')->name('request-contact');

/*
* Registration
**/
Route::group(['prefix' => 'register'], function() {
	Route::get('/', 'HomeController@register')->name('register');
    Route::post('/', 'HomeController@getForms')->name('get.forms');
	Route::post('validate-form', 'HomeController@validateForm');
	Route::post('store', 'HomeController@storeRegistration');
	Route::get('success/{user?}', 'HomeController@success')->name('register.success');

	// fb registration
	Route::get('{user}/fb-register', 'HomeController@fbRegister')->name('register.fb-register');
});

Route::group(['middleware' => 'auth'], function() {

	/*
	* Admin Routes
	**/
	require base_path('routes/admin.php');	

	/*
	* Regular User Routes
	**/
	require base_path('routes/user.php');
});