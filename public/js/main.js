$(document).ready(function(){
	resizeWidth();


	$(document).on('click','.mobile-ctrl', function(e){
		e.preventDefault();

		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}

	});

	$(window).resize(function(){
		resizeWidth();

	});
	$(window).load(function(){
		$(window).resize();
	});
});
function resizeWidth(){
	testiCarousel();
}

function testiCarousel(){

	if($('.tst-list').length){

		if(Modernizr.mq('(min-width: 992px)')){
			var itemWidth = 385;
		}else{
			var itemWidth = 295;
		}
		$('.tst-list').carouFredSel({
			responsive: false,
			scroll: 1,
			width: '100%',
			height: 'variable',
			items: {
				height: 'variable',
				width: itemWidth,
				visible: {
					min: 1,
					max: 3
				}
			}
		});

		//$("#testimonials-section .testimonial-list .item-wrapper").matchHeight();
	}
}

