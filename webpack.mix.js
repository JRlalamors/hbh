const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   	.options({
   		processCssUrls: false
   	});

mix.scripts([
	'resources/assets/js/jqueryui.min.js',
	'resources/assets/js/modernizr-2.8.3-respond-1.4.2.min.js',
	'resources/assets/js/jquery.carouFredSel-6.2.1-packed.js',
	'resources/assets/js/jquery.dotdotdot.min.js',
	'resources/assets/js/numeral.js',
], 'public/js/vendor-scripts.js');

mix.styles([
	'resources/assets/css/jqueryui.min.css'
], 'public/css/vendor-styles.css');