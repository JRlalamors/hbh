<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('agent_id');
            $table->integer('listing_id');
            $table->float('rebate', 8, 2);
            $table->enum('rebate_type', ['fixed', 'percentage']);
            $table->text('other_data')->nullable();
            $table->softDeletes();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
