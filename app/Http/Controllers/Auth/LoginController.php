<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Socialite;
use Carbon\Carbon;
use App\Model\OAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, User $user)
    {

        // save last login date time
        $user->last_login = Carbon::now();
        $user->save();

        if( $user->isAdmin() ) {
            return redirect('admin');
        }
        elseif( $user->isCustomer() ) {
            return redirect('/customer/account');
        }
        elseif( $user->isAgent() ) {
            return redirect('/agent/account');
        }
        else {
            return redirect('/');
        }
    }


    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {

        return Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday'
        ])->scopes([
            'user_hometown','user_location','public_profile','user_photos'
        ])->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function fbAuth()
    {
        $fbUser = Socialite::driver('facebook')->stateless()->user();

        $email = $fbUser->getEmail();

        if ($email) {
            $oAuth = OAuth::where('email', $email)->first();
            $authFBuser = User::where('email', $email)->first();

            if( !$oAuth && !$authFBuser )
            {
                $name = explode(" ", $fbUser->getName());

                if (is_array($name)) {
                    $lastName = $name[count($name) - 1];

                    unset($name[count($name) - 1]);
                    $firstname = join(" ", $name);
                } else {
                    $firstname = 'n/a';
                    $lastName = 'n/a';
                }

                // Register new user
                if( !$authFBuser ) {
                    $user = new User;
                    $user->first_name = $firstname;
                    $user->last_name = $lastName;
                    $user->email = $email;
                    $user->password = bcrypt(" ");
                    $user->role_id = 0;
                    $user->save();
                }

                // save auth
                if( !$oAuth ) {
                    $oauth = new OAuth;
                    $oauth->user_id = $user->id;
                    $oauth->email = $email;
                    $oauth->save();
                }

                return view('customer-agent', compact('user'));

            } else {
                Auth::login($authFBuser, true);
            }
        }

        return redirect()->route('login')->withError('It seems your email is not shared in your facebook account, that\'s why we cannot sign you up');
    }
}
