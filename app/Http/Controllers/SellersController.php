<?php

namespace App\Http\Controllers;

use App\Model\Agent;
use Illuminate\Http\Request;

class SellersController extends Controller
{
   	
	public function index() 
	{
		return view('seller.index');
	}

	public function findAgent() 
	{
		$agents = Agent::all();

		return view('seller.find-agent', compact('agents'));
	}

	public function guide() 
	{
		return view('seller.guide');
	}

	public function process() 
	{
		return view('seller.process');
	}

}
