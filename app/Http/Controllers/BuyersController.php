<?php

namespace App\Http\Controllers;

use App\Model\Agent;
use Illuminate\Http\Request;

class BuyersController extends Controller
{
   	
	public function index() 
	{
		return view('buyer.index');
	}

	public function findAgent() 
	{
		$agents = Agent::all();

		return view('buyer.find-agent', compact('agents'));
	}

	public function guide() 
	{
		return view('buyer.guide');
	}

	public function process () 
	{
		return view('buyer.process');
	}

}
