<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SelectedAgentMailCustomer;
use Carbon\Carbon;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Activity;
use App\Model\Customer;
use App\Mail\SelectedAgent;
use Illuminate\Http\Request;
use App\Mail\VerifiedCustomer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CustomersController extends Controller
{
    public function index() 
    {
    	return view('admin.customer.index');
    }

    public function customersToScreen() 
    {

        $with = [
            'asUser',
            'listings',
        ];

        $customers = new Customer;
        
        $customers = $customers->search($with, 10, ['approved' => 0, 'rejected' => 0]);

    	return view('admin.customer.screen', compact('customers'));
    }

    public function allCustomers() 
    {   
        
        $lists = Listing::with(['customer.asUser', 'agents.asUser'])->where('closed_deal', 0)->orderByDesc('id')->paginate(10);

    	return view('admin.customer.all', compact('lists'));
    }

    public function customersWithoutAgent() 
    {
        $with = [
            'asUser'
        ];

        $customers = new Customer;

        $customers = $customers->with(['asUser'])->where(function($customer){
            $customer->whereHas('listings', function($list) {
                $list->whereDoesntHave('agents');
            });
        })->paginate(10);

    	return view('admin.customer.without-agent', compact('customers'));
    }

    public function searchResults(Request $request) 
    {

         $with = [
            'asUser',
            'listings'
        ];

        $customers = new Customer;

        $customers = $customers->search($with, 10, [], $request);

    	return view('admin.customer.search-result', compact('customers'));
    }

    public function profile(Customer $customer, $status = null, Request $request) 
    {
        $customer->load(['asUser', 'listings']);

        $pitches = $customer->listings->each(function($list) {
            $list->load(['agents' => function($agent) {
                $agent->with(['deals', 'asUser']);
            }, 'customer']);
        });

        $buyersList = $customer->buyerList()->sortByDesc('created_at')->values();
        $sellersList = $customer->sellerList()->sortByDesc('created_at')->values();

        // dd( $buyersList, $sellersList );

        $pitches = $pitches->sortByDesc('created_at'); // until

        $request->session()->forget('success');

        $activities = $customer->asUser->activities;
        $activities = $activities->sortByDesc('created_at');
      
        if( $status && $status == 'approved' ) 
        {
            session(['success' => 'This customer has been approved!']);
        } elseif( $status && $status == 'rejected' ) {
            session(['success' => 'This customer has been rejected!']);
        }

        return view('admin.customer.profile', compact('customer', 'proposals', 'pitches', 'activities', 'sellersList', 'buyersList'));
    }

    public function storeProfile(Customer $customer, Request $request) 
    {   

        $customer->setOtherData([
            'admin_name' => array_key_exists('admin_name', $request->all()) || $request->has('admin_name') ? $request->admin_name : $customer->asUser->name,
            'admin_email' => array_key_exists('admin_email', $request->all()) || $request->has('admin_email') ? $request->admin_email : $customer->asUser->email,
            'admin_phone' => array_key_exists('admin_phone', $request->all()) || $request->has('admin_phone') ? preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->admin_phone) : $customer->asUser->phone_num 
        ]);

        return redirect()->back()->withSuccess('Profile successfuly save.');

    }

    public function listDetails(Customer $customer, Listing $list) 
    {

        $list->load(['customer.asUser', 'selectedAgent.asUser']);


        return view('admin.customer.list-details', compact('list', 'customer'));
    }

    public function editAgents(Listing $list) 
    {       
        $userAgents = $list->agents()
                           ->with('asUser')
                           ->get()
                           ->pluck('asUser.name', 'id');

        $agents = Agent::with(['asUser'])->where('approved', 1)->get();
        $agentsArr = [];
        foreach( $agents as $agent )
        {
            $agentsArr[$agent->id] = ['name' => $agent->asUser->name, 'email' => $agent->asUser->email];
        }

        $customer = $list->customer->load('asUser');

        return response()->json(compact('agentsArr', 'userAgents', 'list', 'customer'));
    }

    public function storeAgents(Listing $list, Request $request) {
        $existings = $list->agents->pluck('id')->toArray();
        if( $list->agents()->sync($request->agents) ) {
            $message = 'success';
        }
        else {
            $message = 'error';
        }

        $agents = $list->agents()->with(['asUser'])->get();
        $agentsArr = [];
        foreach( $agents as $agent )
        {
            if ($agent->asUser && !in_array($agent->id, $existings)) {
                Mail::to($agent->asUser)->send(new SelectedAgent($list, $agent));
                Mail::to($agent->asUser)->send(new SelectedAgentMailCustomer($list, $agent));
            }
            $agentsArr[] = ['name' => $agent->asUser->name, 'email' => $agent->asUser->email, 'route' => route('admin.agents.profile', $agent)];
        }

        $pitches = $list->customer->listings->sortByDesc('created_at'); // until

        // update pitches
        if( $pitches->count() > 0 ) 
        {

            $pitchOutput = '';

            foreach( $pitches as $pitch ) 
            {

                if( $pitch->agents->count() > 0 ) 
                {

                    foreach( $pitch->agents as $agent )
                    {
                        $pitchOutput .= '<tr>';
                            $pitchOutput .= '<td class="text-center">' . $pitch->text_until ? $pitch->text_until : with(new Carbon($pitch->until))->format('M d, o') . '</td>';
                            $pitchOutput .= '<td class="text-center">' . $agent->asUser->name . '</td>';
                            $pitchOutput .= '<td class="text-center">' . $pitch->type() . '</td>';

                            if( $pitch->agentHasProposal($agent->id) )
                            {
                                $pitchOutput .= '<td class="text-center">' . $pitch->proposal($agent->id)->rebate_format . '</td>';
                                $pitchOutput .= '<td class="text-center">' . $pitch->proposal($agent->id)->approximate . '</td>';
                            } else {
                                $pitchOutput .= '<td class="text-center">Not yet submitted</td>';
                                $pitchOutput .= '<td class="text-center">Not yet submitted</td>';
                            }

                            $pitchOutput .= '<td class="text-center"><a href="' . route('admin.customers.profile.list-details', [$pitch->customer, $pitch]) . '" class="blue">View Full Details</a></td>';

                            $pitchOutput .= '<td class="text-center">';
                                if( $pitch->hasSelectedAProposal() && $pitch->agent_id == $agent->id ) 
                                {
                                    $pitchOutput .= 'Yes';
                                } else {
                                    $pitchOutput .= 'No';
                                }
                            $pitchOutput .= '</td>';

                            $pitchOutput .= '<td class="text-center">' . $pitch->id . '</td>';

                        $pitchOutput .= '</tr>';
                    }

                }

            }

        } else {
            $pitchOutput = '<td colspan="8">Pitches not found..</td>';
        }

        return response()->json(compact('message', 'agentsArr', 'pitchOutput'));
    }

    public function storeListAgents(Listing $list, Request $request)
    {

        if( $list->agents()->sync($request->agents) ) {
            $message = 'success';
            return response()->json([
                'message' => 'success',
                'route' => route('admin.customers.all')
            ]);
        }
        else {
            return response()->json([
                'message' => 'error'
            ]);
        }

    }

    public function approve(Customer $customer) 
    {
        $customer->approved = 1;
        $customer->rejected = 0;

        if( $customer->save() ) {
            $message = 'success';

			// add activity
			$activity = new Activity;
			$action = 'Profile was approved.';
			$activity->addActivity($customer->asUser->id, $action);
			
			// email to customer	
			Mail::to($customer->asUser->email)->send(new VerifiedCustomer($customer));
        } 
        else {
            $message = 'error';
        }        

        return response()->json(compact('message'));
    }

    public function reject(Customer $customer) 
    {
        $customer->approved = 0;
        $customer->rejected = 1;

        if( $customer->save() ) {
            $message = 'success';

            // add activity
            $activity = new Activity;
            $action = 'Profile was rejected.';
            $activity->addActivity($customer->asUser->id, $action);

        } 
        else {
            $message = 'error';
        }

        return response()->json(compact('message'));

    }

    public function creatingList(Request $request)
    {
        $list = new Listing();
        $type = $request->list_type;

        return view('admin.customer.partials.ajax-customer-list', compact('list','type'));
    }

    public function getList(Listing $list)
    {
        $type = $list->list_type;

        return view('admin.customer.partials.ajax-customer-list', compact('list', 'type'));
    }

    public function updateList(Request $request)
    {
        $list = $request->has('list_id') ? Listing::find($request->list_id) : new Listing();
        
        if (!$list->exists) {
            $new = true;
        }

        $list = $this->createdList($request, $list);
        if (!$list instanceof Listing) {
            return response($list);
        }

        return view('admin.customer.partials.ajax-updated-customer-list', compact('list', 'new'));
    }

    private function createdList(Request $request, Listing $list)
    {
        $errorMessages = [];

        $list->fill($request->all());
        $list->text_until = $request->until;
        $list->budget = (int) preg_replace(array('/\,/', '/\$/'), array(''), $request->numeric_budget);
        $list->text_budget = $request->list_type == 'seller' ? $request->budget : '$' . number_format($request->budget, 0);
        $list->closed_deal = 0;
        $list->customer_id = $request->customer_id;
        $list->city = $request->city;
        $list->state = $request->state;
        $list->sq_feet_min = $request->sq_feet_min;
        $list->sq_feet_max = $request->sq_feet_max;
        $list->bathrooms = $request->bathrooms;
        $list->bedrooms = $request->bedrooms;
        $list->other_data = json_encode(array('street_address' => $request->street_address, 'unit_no' => $request->unit_no));

        // validating rules
        $errorFields = [
            'list_type' => 'required',
            'property_type_id' => 'required',
            'zip_postal' => 'required',
            'budget' => 'required',
        ];

        if ($request->list_type == 'buyer') {
            $errorFields = array_add($errorFields, 'sq_feet_min', 'required_with:sq_feet_max|numeric');
            $errorFields = array_add($errorFields, 'sq_feet_max', 'required_with:sq_feet_min|numeric|greater_than_field:sq_feet_min');
        }

        $validateList = Validator::make($request->all(), $errorFields, $errorMessages);

        $errors = $validateList->errors()->all();
        if( count($errors) > 0 ) {
            return $errors;
        }
        $list->save();

        if ($request->has('status_to_lender')) {
            $customer = $list->customer;
            $customer->status_to_lender = $request->status_to_lender;
            $customer->save();
        }

        return $list;
    }
}
