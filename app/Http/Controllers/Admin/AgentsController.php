<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Activity;
use App\Model\Proposal;
use Illuminate\Http\Request;
use App\Mail\VerifiedAgent;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class AgentsController extends Controller
{
    
	public function index() {
		return view('admin.agent.index');
	}

	public function reviewAgents() 
	{
		$agents = new Agent;

		$agents = $agents->search(['asUser'], 10, ['approved' => 0, 'rejected' => 0]);

		return view('admin.agent.review', compact('agents'));
	}

	public function viewAgents() 
	{

		$agents = new Agent;		

		$agents = $agents->search(['asUser'], 10);

		return view('admin.agent.view', compact('agents'));
	}

	public function deals() 
	{
		return view('admin.agent.deals');
	}

	public function searchResults(Request $request) 
	{

		$with = [
			'asUser'
		];

		$agents = new Agent;

		$agents = $agents->search($with, 10, [], $request);

		return view('admin.agent.search-results', compact('agents'));
	}

	public function profile(Agent $agent, $status = null, Request $request) 
	{

		$lists = Listing::whereHas('proposals', function($proposal) use($agent) {
			$proposal->where('agent_id', $agent->id);
		})->get();

		$zillowApi = $agent->zillowApi();

		$buyersList = $agent->outstandingDeals()
							->with('customer.asUser')
							->where('list_type', 'buyer')
							->get()
							->sortByDesc('until');

		$sellersList = $agent->outstandingDeals()
							 ->with(['customer.asUser'])
							 ->where('list_type', 'seller')
							 ->get()
							 ->sortByDesc('until');

		// activities
		$activities = $agent->asUser->activities;
		$activities = $activities->sortByDesc('created_at');

		// proposals
		$proposals = $agent->proposals->each(function($proposal) {
			$proposal->load('listing.customer.asUser');
		})->sortByDesc('created_at');

		// requests
		$requests = $agent->deals->each(function($deal){
			$deal->load('customer.asUser');
		});

		$pitches = $proposals->merge( $requests )->sortByDesc('created_at');

		if( $status && $status == 'approved' )
		{
			session(['success' => 'This agent has been approved!']);
		} elseif( $status && $status == 'rejected' ) {
			session(['success' => 'This agent has been rejected!']);
		} else {
			// remove all session
			//$request->session()->forget('success');
		}

		return view('admin.agent.profile', compact('agent', 'zillowApi', 'buyersList', 'sellersList', 'activities', 'pitches'));
	}

	public function storeProfile(Agent $agent, Request $request)
	{

		$agent->setOtherData([
			'admin_name' => $request->admin_name,
			'admin_email' => $request->admin_email,
			'admin_phone' => $request->admin_phone,
			'admin_as_broker' => $request->admin_as_broker,
			'admin_as_agent' => $request->admin_as_agent,
			'admin_broker' => $request->admin_broker,
			'admin_agency' => $request->admin_agency,
			'admin_other' => $request->admin_other,
			'admin_license' => $request->admin_license
		]);

		return redirect()->back()->withSuccess('Profile successfuly saved.');

	}

	public function storeQA(Agent $agent, Request $request)
	{

		$agent->setOtherData([
			'question_1' => $request->question_1,
			'question_2' => $request->question_2
		]);

		return redirect()->back()->withSuccess('Q+A successfuly saved.');

	}

	public function storeSocial(Agent $agent, Request $request)
	{

		$agent->setOtherData([
			'admin_linkedin_url' => $request->admin_linkedin_url,
			'admin_website_url' => $request->admin_website_url,
			'admin_zillow_url' => $request->admin_zillow_url
		]);

		return redirect()->back()->withSuccess('Social URLs successfuly saved.');

	}

	public function listDetails(Agent $agent, Listing $list) 
	{

		$list->load('customer.asUser');

		return view('admin.agent.list-details', compact('list'));
	}

	public function approve(Agent $agent) 
	{
		$agent->approved = 1;
		$agent->rejected = 0;

		if( $agent->save() ) {
			$message = 'success';

			// add activities
			$activity = new Activity;
			$action = 'Profile was approved.';
			$activity->addActivity($agent->asUser->id, $action);

			Mail::to($agent->asUser->email)->send(new VerifiedAgent($agent));

		} 
		else {
			$message = 'error';
		}

		return response()->json(compact('message'));

	}

	public function reject(Agent $agent) 
	{
		$agent->approved = 0;
		$agent->rejected = 1;

		if( $agent->save() ) {
			$message = 'success';

			// add activities
			$activity = new Activity;
			$action = 'Profile was rejected.';
			$activity->addActivity($agent->asUser->id, $action);

		} 
		else {
			$message = 'error';
		}

		return response()->json(compact('message'));

	}

}
