<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Model\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    
	public function index() 
	{
		$users = User::where(function($user) {
			$user->whereHas('role');
		})->get();
		return view('admin.setting.index', compact('users'));
	}

	public function create() 
	{
		$roles = Role::pluck('name', 'id')->toArray();

		return  view('admin.setting.create', compact('roles'));
	}

	public function store(Request $request) 
	{	

		try {

			$user = new User;
			$user->fill($request->all());

			$user->password = bcrypt($request->password);
			$user->role_id = $request->role_id;

			$user->saveOrFail();

			return redirect()->route('admin.settings')->withSuccess('User successfuly added.');

		} catch( \Watson\Validating\ValidationException $e ) {
			
			$errors = $e->getErrors();
			return redirect()->back()
							 ->withErrors($errors)
							 ->withInput();
		}				

	}

	public function edit(User $user)
	{
		$roles = Role::get()->pluck('name', 'id');
		return view('admin.setting.edit', compact('roles', 'user'));
	}

	public function update(Request $request, User $user)
	{

		try {

			$user->fill($request->all());
			$user->saveOrFail();

			return redirect()->route('admin.settings')->withSuccess('User successfuly updated.'); 

		} catch( \Watson\Validating\ValidationException $e ) {

			$errors = $e->getErrors();
			return redirect()->back()
							 ->withErrors($errors)
							 ->withInput();

		}

	}

	public function destroy(User $user) {
		$user->delete();
		return redirect()->route('admin.settings')->withSuccess('User successfuly deleted.');
	}

}
