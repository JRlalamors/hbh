<?php

namespace App\Http\Controllers\Admin;

use App\Model\Listing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealsController extends Controller
{
	
	public function index()
	{
		$deals = new Listing;

		$deals = $deals->with(['selectedAgent.asUser', 'customer.asUser'])->whereHas('selectedAgent')->get();

		return view('admin.deals-made.index', compact('deals'));
	}

	public function details(Listing $list) 
	{	

		$deal = $list->load(['customer.AsUser', 'selectedAgent.asUser', 'property']);
		$proposal = $deal->selectedProposal()->load('agent.asUser');

		return view('admin.deals-made.details', compact('deal', 'proposal'));
	}

}
