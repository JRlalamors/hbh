<?php

namespace App\Mail;

use App\Model\Agent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;

class AgentSignupMailUser extends Mailable
{
    use Queueable, SerializesModels;

    public $agent;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Agent $agent, $password = null)
    {
        $this->agent = $agent;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to HomebyHome')
                    ->markdown('email.agent-signup-user')
                    ->with([
                        'agent' => $this->agent,
                    ]);
    }
}
