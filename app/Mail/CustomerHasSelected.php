<?php

namespace App\Mail;

use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerHasSelected extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $agent;
    public $list;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Listing $list, Agent $agent, Customer $customer)
    {
        $this->customer = $customer;
        $this->agent = $agent;
        $this->list = $list;
        $this->subject = $customer->asUser->name . ' already selected an agent.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->markdown('email.customer-has-selected')
                    ->with([
                        'agent' => $this->agent,
                        'customer' => $this->customer,
                        'list' => $this->list
                    ]);
    }
}
