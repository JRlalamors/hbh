<?php

namespace App\Mail;

use App\Model\Agent;
use App\Model\Customer;
use App\Model\Listing;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmittedProposalAgent extends Mailable
{
    use Queueable, SerializesModels;

    public $agent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Listing $list, Agent $agent)
    {
        $this->agent = $agent->load('asUser');
        $this->list = $list;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->subject('Agent Submitted a Proposal')
                    ->markdown('email.agent-submitted-proposal')
                    ->with([
                        'agent' => $this->agent,
                        'list' => $this->list
                    ]);
    }
}
