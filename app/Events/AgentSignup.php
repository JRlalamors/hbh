<?php

namespace App\Events;

use App\Model\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AgentSignup
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $agent;
    public $password;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Agent $agent, $password = null)
    {
        $this->agent = $agent;
        $this->password = $password;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        //
    }
}
