<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use ImageResize;
use App\Model\Role;
use App\Model\Agent;
use App\Model\Customer;
use App\Model\Activity;
use Watson\Validating\ValidatingTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    use ValidatingTrait;

    protected $throwValidationExceptions = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'phone_num'
    ];

    protected $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|unique|email',
        'password' => 'required|min:8',
        //'phone_num' => 'required|numeric|min:11|nullable',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /*
    * Relations
    **/
    public function role() 
    {
        return $this->belongsTo(Role::class);
    }

    public function asCustomer() 
    {
        return $this->hasOne(Customer::class);
    }

    public function asAgent() 
    {
        return $this->hasOne(Agent::class);
    }

    public function activities() 
    {
        return $this->hasMany(Activity::class);
    }

    /*
    *  Helper Methods
    **/
    public function isAdmin() 
    {

        if( $this->role ) 
        {
            if($this->role->slug != 'administrator') 
            {
                return false;
            }
        } else {
            return false;
        }

        return true;

    }

    public function isCustomer() 
    {
        if( !$this->asCustomer ) {
            return false;
        }
        return true;
    }    

    public function isAgent() 
    {   
        if( !$this->asAgent ) {
            return false;
        }
        return true;
    }   

    public function isRegularUser() 
    {

        if( $this->role ) {
            if( $this->role->slug != 'user' )
            {
                return false;
            }
        } else {
            return false;
        }

        return true;

    }

    public function isNewUser()
    {
        return $this->created_at->isSameDay(Carbon::today()) ?: false;
    }

    /*
    * Getter Methods
    **/
    public function getNameAttribute() 
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getphoneFormatAttribute() 
    {

        $phone = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $this->phone_num);
        $phone = str_split($phone, 1);
        $phone = array_chunk($phone, 3);

        $phone[0] = '(' . implode('', $phone[0]) . ') ';

        if( isset( $phone[1] ) ) {
            $phone[1] = implode('', $phone[1]) . (count($phone) > 2 ? '-' : '');
        }

        if( count( $phone ) > 2 ) {
            for( $i = 2; $i < count($phone); $i++ ) {
                $phone[$i] = implode('', $phone[$i]);
            }
        }

        $phone = implode('', $phone);

        return $phone;

    }

    public function getProfilePhotoAttribute()
    {
        $publicPath = '';
        $profilePhoto = asset(Storage::disk('world')->url('profile-photos/' . $this->photo));
        $publicPath = public_path('storage/profile-photos/' . ($this->photo ?: 'false'));

        if( $this->photo && File::exists($publicPath))
        {
            $resizedPhoto = ImageResize::make($profilePhoto)->fit(278, 278)->encode('data-url');
        } else {
            $resizedPhoto = null;
        }

        return File::exists($publicPath) ? $resizedPhoto->encoded : null;
    }

}
