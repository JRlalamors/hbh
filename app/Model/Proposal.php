<?php

namespace App\Model;

use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use Illuminate\Database\Eloquent\Model;

class Proposal extends BaseModel
{
	protected $fillable = [
		'customer_id',
		'agent_id',
		'listing_id',
		'rebate',
		'rebate_type'
	];

	protected $rules = [
		'rebate' => 'required|rebate_value',
		'rebate_type' => 'required',
		'agent_id' => 'required',
		'listing' => 'unique'
	];

	protected $casts = [
        'other_data' => 'array',
    ];

    public function getValidationMessages()
    {
        return [
            'rebate.digits_between' => 'The rebate value must not be greater than 8 digits',
            'rebate.rebate_value' => 'The rebate value must be whole number of decimal'
        ];
    }

	public function agent()
	{
		return $this->belongsTo(Agent::class);
	}

	public function listing()
	{
		return $this->belongsTo(Listing::class);
	}

	/*
	* Helper Method
	**/
	public function isSelected() 
	{	

		if( $this->listing->agent_id == null ) 
		{
			return false;
		}

		return true;
	}

	/*
	* Getter Method
	**/
	public function getRebateTypeNameAttribute()
	{
		return ucfirst($this->rebate_type);
	}

	public function getRebateFormatAttribute()
	{	

		$sign = $this->rebate_type == 'fixed' ? '$ ' : '';
		$per = $this->rebate_type == 'percentage' ? '%' : '';


        $rebate = is_float($this->rebate) ? $this->rebate : number_format($this->rebate, 0);
		//$rebate = $this->rebate_type == 'fixed' ? number_format($this->rebate, 0) : number_format($this->rebate, 0);

		return $sign . $rebate . $per;
	}

	public function getApproximateAttribute() 
	{
		$rebate = $this->rebate;
		$type = $this->rebate_type;
        $budget = $this->listing->budget;

		if( $type == 'percentage' )
		{
			if( is_numeric($budget) && $budget )
			{
                if ($this->listing->list_type == Listing::BUYER) {
                    $calculation = ($this->rebate / 100) * .03 * $budget;
                    $approx = '$ ' . number_format($calculation, 0, '.', ',');
                } else {
                    $calculation = ($budget * 0.06) - ($budget * ($this->rebate / 100));
                    $approx = '$ ' . number_format($calculation, 0, '.', ',');
                }
			} else {
				$approx = $this->rebate_format;
			}

		} else {
            if( is_numeric($budget) && $budget ) {
                if ($this->listing->list_type == Listing::BUYER) {
                    $approx = $this->rebate_format;
                } else {
                    $calculation = ($budget * 0.06) - $this->rebate;
                    $approx = '$ ' . number_format($calculation, 0, '.', ',');
                }
            } else {
                $approx = $this->rebate_format;
            }
		}

		return $approx;

	}

}
