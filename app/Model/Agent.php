<?php

namespace App\Model;

use GuzzleHttp\Client;
use Parser;
use App\User;
use DateTime;
use Carbon\Carbon;
use App\Model\Listing;
use App\Model\Customer;
use App\Model\Proposal;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Agent extends BaseModel
{	

	protected $table = 'agents';
    
    protected $fillable = [
    	'current_role',
    	'work_with',
    	'zillow_url',
    	'linkedin_url',
    	'website_url'
    ];

    protected $rules = [
    	'current_role' => 'required',
    	'work_with' => 'required',        
        'website_url' => 'nullable'
    ];

    protected $casts = [
        'other_data' => 'array',
    ];

    public function asUser() 
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function deals()
    {
        return $this->belongsToMany(Listing::class)->withTimeStamps();
    }

    public function outstandingDeals()
    {
        return $this->hasMany(Listing::class, 'agent_id');
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    /*
    *	Helper Methods
    **/   
    public function customers() 
    {

        $customers = Customer::whereHas('listings', function($listing) {
            $listing->whereHas('agents', function($agent) {
                $agent->where('id', $this->id);
            });
        });

        return $customers;

    }

    public function listHasProposal($listID)
    {
        $proposal = Proposal::where('agent_id', $this->id)
                              ->where('listing_id', $listID)
                              ->first();

        if( $proposal ) 
        {
            return true;
        }

        return false;
    }

    public function outstandingDealsWithTimestamps()
    {
        return $this->outstandingDeals()->with(['customer.asUser', 'agents' => function($agent) {
            $agent->where('id', $this->id);
        }])->get();
    }

    public function hasZillow()
    {   
        if( empty($this->zillow_url) ) {
            return false;
        }

        return true;
    }

    public function zillowApi() {
        if (env('APP_ENV') != 'local') {
            if ( $this->hasZillow() ) {

                // check last fetch if within the last hour use existing else fetch again
                $lastFetch = $this->getOtherData('zillow_last_fetch');

                $today = with(new Carbon())->getTimestamp();

                $zillowResults = $this->getOtherData('zillow_results');
                if ($lastFetch && $zillowResults) {
                    $dateTime = with(new Carbon)->timestamp($lastFetch);
                    $diffInMinutes = with(new Carbon)->diffInMinutes($dateTime);
                    $zillowResults['reviewCount'] = isset($zillowResults['proInfo']) ? $zillowResults['proInfo']['reviewCount'] : 0;
                    return $zillowResults;
                }

                $zillowUsername = basename($this->getOtherData('admin_zillow_url') ? $this->getOtherData('admin_zillow_url') : $this->zillow_url);

                $zillowURL = 'http://www.zillow.com/webservice/ProReviews.htm?zws-id=X1-ZWz193u04g198r_2599m&count=10&screenname=' . $zillowUsername;
                //$curl = Curl::to($zillowURL)->allowRedirect(true)->get();

                $client = new Client();
                $curl = $client->request('GET', $zillowURL, [
                    'headers' => ['Accept' => 'application/xml'],
                    'timeout' => 120
                ])->getBody()->getContents();

                $zillowApi = Parser::xml($curl);

                if( !isset($zillowApi['response']) )
                {
                    return $zillowResults;
                } else {
                    $zillowApi = $zillowApi['response']['result'];
                    $zillowApi['reviewCount'] = isset($zillowResults['proInfo']) ? $zillowResults['proInfo']['reviewCount'] : 0;
                    $this->setOtherData([
                        'zillow_results' => $zillowApi,
                        'zillow_last_fetch' => with(new DateTime)->getTimestamp(),
                    ]);
                }

            }
        }

        return null;
    }

    public function ratingPercentage($rating) 
    {
        $zillowApi = $this->zillowApi();

        $rating = isset($zillowApi['proInfo'][$rating]) ? floatval($zillowApi['proInfo'][$rating]) : 0;

        $rating = ($rating / 5) * 100;

        return $rating . '%';
    }

    /*
    * Getter Methods
    **/
    public function getzillowUsernameAttribute()
    {
        return 'https://www.zillow.com/profile/' . basename( $this->zillow_url );
    }

    public function getLinkedinUsernameAttribute()
    {
        return 'https://www.linkedin.com/in/' . basename( $this->linkedin_url );
    }

    public function getAdminLinkedinUsernameAttribute()
    {
        return 'https://www.linkedin.com/in/' . $this->getOtherData('admin_linkedin_url');
    }

}
