<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    public function getHumanDisplayAttribute()
    {
        return str_replace('-', ' ', $this->display_name);
    }
}
