<?php

namespace App\Model;

use App\User;

use Illuminate\Database\Eloquent\Model;

class OAuth extends Model
{
    
	protected $table = 'oauth';


	/*
	* Relationships
	**/
	public function user() 
	{
		return $this->belongsTo(User::class);
	}

}
