<?php

namespace App\Model;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
	use ValidatingTrait;

	protected $table = 'roles';

	protected $fillable = [
		'name'
	];

}
