<?php

namespace App\Model;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{	

	use ValidatingTrait;
	protected $injectUniqueIdentifier = true;
    
	public function add(Request $request) 
	{

		$this->fill($request->all());

		return $this->saveOrFail();

	}

	public function search($with = [], $paginate = null, $where = [], Request $request = null) 
	{	

		$query = $this;

		if( count( $with ) != 0 ) 
		{
			$query = $query->with($with);
		}

		/* if request has parameters
		*/
		if( $request != null ) {
			// if request has name
			if( $request->has('name') ) {

				// check model instance
				if( $this instanceof \App\Model\Agent || $this instanceof \App\Model\Customer ) 
				{
					$query = $query->whereHas('asUser', function($user) use($request) {
						$user->where('first_name', 'like', "%$request->name%")
							 ->orWhere('last_name', 'like', "%$request->name%");
					});
				} else {
					$query->where('first_name', 'like', "%$request->name%")
						  ->orWhere('last_name', 'like', "%$request->name%");
				}
			}

			// if request has email
			if( $request->has('email') ) {

				if( $this instanceof \App\Model\Agent || $this instanceof \App\Model\Customer ) 
				{
					$query = $query->whereHas('asUser', function($user) use($request) {
						$user->where('email', $request->email);
					});
				} else {
					$query = $query->where('email', $request->email);
				}	

			}

			// if request has phone
			if( $request->has('phone') ) {

				if( $this instanceof \App\Model\Agent || $this instanceof \App\Model\Customer )
				{
					$query = $query->whereHas('asUser', function($user) use($request) {
						$user->where('phone_num', 'like', "%$request->phone%");
					});
				} else {
					$query = $query->where('phone_num', "%$request->phone%");
				}

			}
		} 
		
		$query = $query->orderByDesc('id');

		/**
		* If request has where parameters
		*/
		if( $where != null ) {

			foreach( $where as $key => $val ) {
				$query = $query->where($key, $val);
			}

		}


		// if request has paginate
		if( $paginate ) 
		{
			return $query->paginate($paginate);
		} else {
			return $query->get();
		}


	}

	public function getOtherData($name, $default = null) 
    {

    	$value = Arr::get($this->other_data, $name);

    	if (is_array($value)) return $value;

        $value = trim($value);
    	return $value ? $value : $default;
    }

    public function setOtherData($data = [])
    {

    	$otherData = collect($this->other_data);

    	$toAddData = $otherData->merge( collect($data) );

    	$this->other_data = $toAddData;

    	return $this->save();

    }

    public function otherDataPhoneFormat() 
    {

    	if( !$this->getOtherData('admin_phone') )
    	{
    		return null;
    	}

    	$phone = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $this->getOtherData('admin_phone'));
        $phone = str_split($phone, 1);
        $phone = array_chunk($phone, 3);

        $phone[0] = '(' . implode('', $phone[0]) . ') ';

        if( isset( $phone[1] ) ) {
            $phone[1] = implode('', $phone[1]) . (count($phone) > 2 ? '-' : '');
        }

        if( count( $phone ) > 2 ) {
            for( $i = 2; $i < count($phone); $i++ ) {
                $phone[$i] = implode('', $phone[$i]);
            }
        }

        $phone = implode('', $phone);

        return $phone;

    }
}
