<?php

namespace App\Providers;

use Socialite;
use App\Model\Agent;
use App\Model\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        * Auth User
        **/
        view()->composer('*', function($view) {
            $view->with('authUser', Auth::user());
        });

        /*
        * Custom Validations
        **/ 
        Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
          $min_field = $parameters[0];
          $data = $validator->getData();
          $min_value = $data[$min_field];
          return $value > $min_value;
        });   

        Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
          return str_replace(':field', $parameters[0], $message);
        });

        Validator::extend('rebate_value', function($attribute, $value, $parameters, $validator) {
            return is_numeric($value) || is_float($value) ? true : false;
        });

        /*
        * Directives
        **/
        Blade::directive('phoneNum', function ($phoneNum) {
          return "<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $phoneNum) ?>";
        });

        Blade::directive('phoneFormat', function ($phoneNum) {
          return "<?php print_r explode(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $phoneNum) ?>";
        });        

        Blade::directive('dateFormat', function($date) {
          return "<?php echo date('M d, o', strtotime($date)); ?>";
        });

        Blade::directive('dateTimeFormat', function($date) {
          return "<?php echo date('M d, o g:i:s', strtotime($date)); ?>";
        });

        Blade::directive('moneyFormat', function($money) {
          return "<?php echo number_format($money, 2, '.', ','); ?>";
        });

        Blade::directive('numberFormat', function($number) {
          return "<?php echo floor($number); ?>";
        });

        /*
        * Events Listeners
        **/

        // customer
        Customer::created(function($customer){
          event(new \App\Events\CustomerSignup($customer));
        });

        // agent
        Agent::created(function($agent) {
          event (new \App\Events\AgentSignup($agent));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
