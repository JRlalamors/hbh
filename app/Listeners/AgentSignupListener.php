<?php

namespace App\Listeners;

use App\Events\AgentSignup;
use App\Mail\AgentSignupMail;
use App\Mail\AgentSignupMailUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgentSignupListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgentSignup  $event
     * @return void
     */
    public function handle(AgentSignup $event)
    {   
        // send mail to admin
        $emails = [config('homebyhome.contact_email'), config('homebyhome.contact_email_2')];
		$sytianEmail = config('homebyhome.contact_sytian');

        Mail::to($emails)->cc($sytianEmail)->send(new AgentSignupMail($event->agent));

        $agent = $event->agent;
        if ($agent->asUser) {
            Mail::to($agent->asUser->email)->send(new AgentSignupMailUser($event->agent));
        }
    }
}
