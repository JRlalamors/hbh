<?php

namespace App\Listeners;

use App\Model\Customer;
use App\Mail\CustomerSignup as CustomerSignupMail;
use App\Mail\CustomerSignupMailUser as CustomerSignupMailUser;
use App\Events\CustomerSignup;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerSignupListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerSignup  $event
     * @return void
     */
    public function handle(CustomerSignup $event)
    {
        $emails = [config('homebyhome.contact_email'), config('homebyhome.contact_email_2')];
		$sytianEmail = config('homebyhome.contact_sytian');

        Mail::to($emails)->cc($sytianEmail)->send(new CustomerSignupMail($event->customer));
		
		// send email to customer
		Mail::to($event->customer->asUser->email)->send(new CustomerSignupMailUser($event->customer));

    }
}
