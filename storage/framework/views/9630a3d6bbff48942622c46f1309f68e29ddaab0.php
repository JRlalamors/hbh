

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		form .form-control {
			height: 48px;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents agent-profile
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-controls'); ?>
<div class="controls">
	<?php if( !$agent->approved ): ?>
		<a class="btn btn-pink" data-confirmtitle="Approve agent?" data-confirmtext="Are you sure you want to approve this agent?" data-confirmfunc="approveAgent" data-confirmparams="[<?php echo e($agent->id); ?>]">Approve</a>
	<?php endif; ?>

	<?php if( !$agent->rejected ): ?>
		<a class="btn btn-blue" data-confirmtitle="Reject agent?" data-confirmtext="Are you sure you want to reject this agent?" data-confirmfunc="rejectAgent" data-confirmparams="[<?php echo e($agent->id); ?>]" >Reject</a>
	<?php endif; ?>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Agent Profile
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="agent-profile">

	<?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="profile-details default-section">
		<div class="container">
			<div class="row">
				<?php echo Form::model($agent, ['route' => ['admin.agents.profile.store', $agent], 'class' => 'default-form']); ?>

					<div class="col-xs-12 col-sm-6">
						<div class="profile-detail">
							<div class="title">Name:</div>
							<div class="content">
								<div class="profile-display">									
									<?php echo e($agent->getOtherData('admin_name') ? $agent->getOtherData('admin_name') : $agent->asUser->name); ?>

								</div>
								<div class="profile-form hidden">									
									<?php echo Form::text('admin_name', $agent->getOtherData('admin_name') ? $agent->getOtherData('admin_name') : $agent->asUser->name, ['class' => 'form-control']); ?>

								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Email:</div>
							<div class="content">
								<div class="profile-display">									
									<?php echo e($agent->getOtherData('admin_email') ? $agent->getOtherData('admin_email') : $agent->asUser->email); ?>

								</div>
								<div class="profile-form hidden">
									<?php echo Form::text('admin_email', $agent->getOtherData('admin_email') ? $agent->getOtherData('admin_email') : $agent->asUser->email, ['class' => 'form-control']); ?>

								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Phone:</div>
							<div class="content">
								<div class="profile-display">									
									<a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $agent->asUser->phone_num) ?>"><?php echo e($agent->getOtherData('admin_phone') ? $agent->getOtherData('admin_phone') : $agent->asUser->phone_format); ?></a>
								</div>
								<div class="profile-form hidden">
									<?php echo Form::text('admin_phone', $agent->getOtherData('admin_phone') ? $agent->getOtherData('admin_phone') : $agent->asUser->phone_format, ['class' => 'form-control validate-phone-num']);; ?>

								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Agency:</div>
							<div class="content">
								<div class="profile-display">								
									<?php if( $agent->getOtherData('admin_agency') ): ?>
										<?php echo e($agent->getOtherData('admin_agency')); ?>

									<?php else: ?>
										<?php echo e(isset($zillowApi['proInfo']['businessName']) ? $zillowApi['proInfo']['businessName'] : ''); ?>

									<?php endif; ?>
								</div>
								<div class="profile-form hidden">
									<?php echo Form::text('admin_agency', $agent->getOtherData('admin_agency') ? $agent->getOtherData('admin_agency') : $zillowApi['proInfo']['businessName'], ['class' => 'form-control'] ); ?>

								</div>
							</div>
						</div>					

						<div class="profile-detail broker-detail">
							<div class="title">Broker:</div>
							<div class="content">
								<div class="profile-display">									
									<?php echo e($agent->getOtherData('admin_broker') && $agent->getOtherData('admin_broker') != null ? $agent->getOtherData('admin_broker') : ''); ?>

								</div>
								<div class="profile-form hidden">
									<?php echo Form::text('admin_broker', $agent->getOtherData('admin_broker'), ['class' => 'form-control']); ?>

								</div>
							</div>						
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="profile-detail">
							<div class="title">Agent ID:</div>
							<div class="content"><?php echo e($agent->id); ?></div>
						</div>
						<div class="profile-detail">
							<div class="title">Broker:</div>
							<?php 
								if( $agent->getOtherData('admin_as_broker') ) 
								{
									if( $agent->getOtherData('admin_as_broker') == 'yes' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}
								} else {
									if( $agent->current_role == 'broker' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}		
								}
							 ?>
							<div class="content">
								<div class="content default-form">
									<div class="checkbox-form">
										<label class="checkbox"><input type="radio" name="admin_as_broker" value="yes" <?php echo e($checkedYes); ?>><span class="chk"></span><span class="text">Yes</span></label>
									</div>
									<div class="checkbox-form">	
										<label class="checkbox"><input type="radio" name="admin_as_broker" value="no" <?php echo e($checkedNo); ?>><span class="chk"></span><span class="text">No</span></label>									
									</div>
								</div>
							</div>
						</div>
						<div class="profile-detail">
							<div class="title">Agent:</div>
							<?php 
								if( $agent->getOtherData('admin_as_agent') ) 
								{
									if( $agent->getOtherData('admin_as_agent') == 'yes' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}
								} else {
									if( $agent->current_role == 'individual-agent' )
									{
										$checkedYes = 'checked';
										$checkedNo = '';
									} else {
										$checkedYes = '';
										$checkedNo = 'checked';
									}		
								}
							 ?>
							<div class="content default-form">
								<div class="checkbox-form">
									<label class="checkbox"><input type="radio" name="admin_as_agent" value="yes" <?php echo e($checkedYes); ?>><span class="chk"></span><span class="text">Yes</span></label>

								</div>
								<div class="checkbox-form">	
									<label class="checkbox"><input type="radio" name="admin_as_agent" value="no" <?php echo e($checkedNo); ?>><span class="chk"></span><span class="text">No</span></label>
									
								</div>
							</div>
						</div>						

						<div class="profile-detail">
							<div class="title">License #:</div>
							<div class="content">
								<div class="profile-display"><?php echo e($agent->getOtherData('admin_license') && $agent->getOtherData('admin_license') != null ? $agent->getOtherdata('admin_license') : ''); ?></div>

								<div class="profile-form hidden">									
									<?php echo Form::text('admin_license', $agent->getOtherData('admin_license') && $agent->getOtherData('admin_license') != null ? $agent->getOtherdata('admin_license') : '', ['class' => 'form-control']); ?>

								</div>
							</div>
						</div>
						
						<div class="profile-detail broker-detail">
							<?php 
								if( $agent->current_role != 'individual-agent' && $agent->current_role != 'broker' )
								{
									$other = $agent->current_role;
								} else {
									$other = '';								
								}
							 ?>
							<div class="title">Other:</div>
							<div class="content">
								<div class="profile-display">									
									<?php echo e($agent->getOtherData('admin_other') && $agent->getOtherData('admin_other') != null ? $agent->getOtherData('admin_other') : $other); ?>

								</div>
								<div class="profile-form hidden">
									<?php echo Form::text('admin_other', $agent->getOtherData('admin_other') != null ? $agent->getOtherData('admin_other') : $other, ['class' => 'form-control']); ?>

								</div>
							</div>						
						</div>						

						<div class="text-right profile-form hidden">
							<button class="btn btn-success btn-white">Save</button>
							<a href="javascript:;" class="btn btn-danger  cancel-edit-profile">Cancel</a href="javascript:;">
						</div>	

						<div class="text-right">
							<a href="javascript:;" class="btn btn-blue edit-profile">Edit</a>
						</div>
					</div>					
				<?php echo Form::close(); ?>

			</div>
		</div>
	</div>

	<div class="profile-settings default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="tab-controls">
						<ul class="nav nav-tabs nav-justified">
						  <li class="active"><a data-toggle="tab" class="theme-blue" href="#tab-buyers">Buyers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-sellers">Sellers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-pitches">Proposal Requests</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-social">Social</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-qa">Q+A</a></li>
						  <li><a class="theme-pink" data-toggle="tab" href="#tab-history">History</a></li>
						</ul>
					</div>
					<div class="tab-content">
			  			<div id="tab-buyers" class="tab-pane fade in active">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">How Soon</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Closed Date</th>
			                                <th class="text-center">View Details</th>
			                                <th class="text-center">Transaction ID</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	<?php $__empty_1 = true; $__currentLoopData = $buyersList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			                        		<tr>		                        			
				                        		<td class="text-center">
													<?php if($list->text_until): ?>
														<?php echo e($list->text_until); ?>

													<?php else: ?>
														<?php echo date('M d, o', strtotime($list->until)); ?>
													<?php endif; ?>
												</td>
				                        		<td class="text-center"><?php echo e($list->customer->asUser->name); ?></td>
				                        		<td class="text-center"><?php echo e($list->proposal($agent->id)->rebate_format); ?></td>
				                        		<td class="text-center"></td>
				                        		<td class="text-center"><a href="<?php echo e(route('admin.agents.profle.list-details', [$agent, $list])); ?>" class="blue">View Full Details</a></td></td>
				                        		<td class="text-center"><?php echo e($list->proposal($agent->id)->id); ?></td>
			                        		</tr>
			                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			                        		<tr><td colspan="6">Buyers not found..</td></tr>
			                        	<?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					  	<div id="tab-sellers" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">How Soon</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Closed Date</th>
			                                <th class="text-center">View Details</th>
			                                <th class="text-center">Transaction ID</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	<?php $__empty_1 = true; $__currentLoopData = $sellersList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
				                            <tr>
				                                <td class="text-center">
													<?php if($list->text_until): ?>
														<?php echo e($list->text_until); ?>

													<?php else: ?>
														<?php echo date('M d, o', strtotime($list->until)); ?>
													<?php endif; ?>
												</td>
				                                <td class="text-center"><?php echo e($list->customer->asUser->name); ?></td>
				                               	<td class="text-center"><?php echo e($list->proposal($agent->id)->rebate_format); ?></td>
				                                <td class="text-center">8/3/17</td>
				                                <td class="text-center"><a href="<?php echo e(route('admin.agents.profle.list-details', [$agent, $list])); ?>" class="blue">View Full Details</a></td>
				                                <td class="text-center"><?php echo e($list->proposal($agent->id)->id); ?></td>
				                            </tr>
			                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
				                            <tr>
				                            	<td colspan="6">Sellers not found..</td>
				                            </tr>
			                            <?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					  	<div id="tab-pitches" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th class="text-center">Pitch Date</th>
			                                <th class="text-center">Clients Name</th>
			                                <th class="text-center">Proposal</th>
			                                <th class="text-center">Was Selected</th>
			                                <th class="text-center">Has Selected an Agent</th>
			                                <th class="text-center">View Full Profile</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            <?php $__empty_1 = true; $__currentLoopData = $pitches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pitch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			                            	<tr>
			                            		<td class="text-center"><?php echo date('M d, o', strtotime($pitch->created_at)); ?></td>
			                            		<td class="text-center">
			                            			<?php if( $pitch instanceof \App\Model\Proposal ): ?>
				                            			<?php echo e($pitch->listing->customer->asUser->name); ?>

			                            			<?php else: ?>
			                            				<?php echo e($pitch->customer->asUser->name); ?>

			                            			<?php endif; ?>
			                            		</td>
			                            		<td class="text-center">
			                            			<?php if( $pitch instanceof App\Model\Proposal ): ?>
			                            				<?php echo e($pitch->rebate_format); ?>

				                            		<?php else: ?> 
				                            			<?php echo e(''); ?>

			                            			<?php endif; ?>
			                            		</td>
			                            		<td class="text-center">
			                            			<?php if( $pitch instanceof App\Model\Proposal ): ?>
				                            			<?php echo e($pitch->isSelected() ? 'Yes' : 'No'); ?>

			                            			<?php else: ?>
			                            				<?php echo e(''); ?>

			                            			<?php endif; ?>
			                            		</td>
			                            		<td class="text-center">
			                            			<?php if( $pitch instanceof App\Model\Proposal ): ?>
				                            			<?php echo e($pitch->listing->hasSelectedAProposal() ? 'Yes' : 'No'); ?>

			                            			<?php else: ?>
			                            				<?php echo e($pitch->hasSelectedAProposal() ? 'Yes' : 'No'); ?>

			                            			<?php endif; ?>
			                            		</td>
			                            		<td class="text-center">
			                            			<?php if( $pitch instanceof App\Model\Proposal ): ?>
				                            			<a href="<?php echo e(route('admin.customers.profile', $pitch->listing->customer)); ?>" class="blue">View Profile</a>
			                            			<?php else: ?>
			                            				<a href="<?php echo e(route('admin.customers.profile', $pitch->customer)); ?>">View Profile</a>
			                            			<?php endif; ?>
			                            		</td>
			                            	</tr>
			                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			                            	<tr><td colspan="6">Requests not found..</td></tr>
			                            <?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>

					  	<div id="tab-social" class="tab-pane fade">
					    	<div class="default-content">
					    		<div class="social-contents">
					    			<div class="row">

					    				<div class="col-xs-12 col-md-6">
					    					<?php echo Form::model($agent, ['route' => ['admin.agents.profile.storeSocial', $agent], 'class' => 'default-form']); ?>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">LinkedIn URL:</div>
					    							<div class="desc social">
					    								<a href="<?php echo e($agent->getOtherdata('admin_linkedin_url') ? $agent->admin_linkedin_username : $agent->linkedin_username); ?>" target="_blank"><?php echo e($agent->getOtherData('admin_linkedin_url') ? $agent->admin_linkedin_username : $agent->linkedin_username); ?></a>
					    							</div>

					    							<div class="form-group form-social hidden">
					    								<?php echo Form::text('admin_linkedin_url', $agent->getOtherData('admin_linkedin_url') ? $agent->getOtherData('admin_linkedin_url') : $agent->linkedin_url, ['class' => 'form-control']); ?>

					    							</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Website URL:</div>
					    							<div class="desc social">
					    								<a href="<?php echo e($agent->getOtherdata('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url); ?>" target="_blank"><?php echo e($agent->getOtherData('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url); ?></a>
					    							</div>

					    							<div class="form-group form-social hidden">
					    								<?php echo Form::text('admin_website_url', $agent->getOtherData('admin_website_url') ? $agent->getOtherData('admin_website_url') : $agent->website_url, ['class' => 'form-control']); ?>

					    							</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper form-social hidden">
					    						<div class="content">
					    							<div class="title">Zillow Username:</div>					    							
						    						<div class="form-group">
						    							<?php echo Form::text('admin_zillow_url', $agent->getOtherData('admin_zillow_url') ? $agent->getOtherData('admin_zillow_url') : $agent->zillow_url, ['class' => 'form-control']); ?>

						    						</div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
						    						<div class="title">Zillow Profile URL:</div>
						    						<div class="desc">
						    							<a href="<?php echo e(isset($zillowApi['proInfo']['profileURL']) ? $zillowApi['proInfo']['profileURL'] : ''); ?>" target="_blank"><?php echo e(isset($zillowApi['proInfo']['profileURL']) ? $zillowApi['proInfo']['profileURL'] : ''); ?></a>
						    						</div>						    						
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
						    						<div class="title">Zillow Screen Name:</div>
						    						<div class="desc"><?php echo e(isset( $zillowApi['screenname'] ) ? $zillowApi['screenname'] : ''); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Zillow Profile Photo:</div>
					    							<div class="desc">
					    								<div class="image">
					    									<img src="<?php echo e(isset($zillowApi['proInfo']['photo']) ? $zillowApi['proInfo']['photo'] : asset('images/default-avatar.svg')); ?>" alt="Home by Home" class="img-responsive">
					    								</div>		    								
					    							</div>
					    						</div>
					    					</div> 			

					    					<div class="content-wrapper">
					    						<div class="content text-right">
					    							<a href="javascript:;" class="btn btn-blue edit-social">Edit</a>

					    							<div class="social-actions hidden">
					    								<button class="btn btn-success">Save</button>
					    								<a href="javascript:;" class="btn btn-red cancel-edit-social">Cancel</a>
					    							</div>
					    						</div>
					    					</div>
					    					<?php echo Form::close(); ?>		
					    				</div>

					    				<div class="col-xs-12 col-md-6">					    					
					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Review Count:</div>
					    							<div class="desc"><?php echo e(isset( $zillowApi['reviewCount'] ) ? $zillowApi['reviewCount'] : 0); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating:</div>
					    							<div class="desc"><?php echo e(isset( $zillowApi['proInfo']['avgRating'] ) ? $zillowApi['proInfo']['avgRating'] : 0); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Local Knowledge:</div>
					    							<div class="desc"><?php echo e(isset( $zillowApi['proInfo']['localknowledgeRating'] ) ? $zillowApi['proInfo']['localknowledgeRating'] : 0); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Process Expertise:</div>
					    							<div class="desc"><?php echo e(isset( $zillowApi['proInfo']['processexpertiseRating'] ) ? $zillowApi['proInfo']['processexpertiseRating'] : 0); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Negotation Skills:</div>
					    							<div class="desc"><?php echo e(isset( $zillowApi['proInfo']['negotiationskillsRating'] ) ? $zillowApi['proInfo']['negotiationskillsRating'] : 0); ?></div>
					    						</div>
					    					</div>

					    					<div class="content-wrapper">
					    						<div class="content">
					    							<div class="title">Average Rating of Responsiveness:</div>
					    							<div class="desc"><?php echo e(isset($zillowApi['proInfo']['responsivenessRating']) ? $zillowApi['proInfo']['responsivenessRating'] : 0); ?></div>
					    						</div>
					    					</div>					    					
					    				</div>
					    			</div>
					    		</div>
					    	</div>
					  	</div>
					  	<div id="tab-qa" class="tab-pane fade">
					    	<div class="default-content">
					    		<?php echo Form::model($agent, ['route' => ['admin.agents.profile.storeQA', $agent]]); ?>

						    		<div class="qa-contents">
						    			<div class="content">
							    			<div class="title">Why should you hire me?</div>
							    			<textarea name="question_1" class="desc" placeholder="Contents from agent here...." disabled><?php echo e($agent->getOtherData('question_1')); ?></textarea> 
						    			</div>
						    			<div class="content">
							    			<div class="title">What do i do better than most agents?</div>
							    			<textarea name="question_2" class="desc" placeholder="Contents from agent here...." disabled><?php echo e($agent->getOtherData('question_2')); ?></textarea> 
						    			</div>
						    			<div class="content">
							    			<div class="title">What do others say about me?</div>
							    			<div class="comments-section">
												<!-- <div class="cs-write">
													<a class="btn btn-pink btn-block hvr-rectangle-out" href="#">WRITE A REVIEW</a>
												</div> -->
												<?php if( isset($zillowApi['proReviews']['review']) && count($zillowApi['proReviews']['review']) ): ?>
													<div class="cs-list">
														<ul>
															<?php $__currentLoopData = $zillowApi['proReviews']['review']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<li>
																	<div class="cs-item">
																		<div class="top">
																			<div class="stars">
																				<?php 
																					$r1 = floatval( isset($review['localknowledgeRating']) ? $review['localknowledgeRating'] : 0 );
																					$r2 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																					$r3 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																					$r4 = floatval( isset($review['negotiationskillsRating']) ? $review['negotiationskillsRating'] : 0 );

																					$raRating = ($r1 + $r2 + $r3 + $r4) / 4;

																					$remaining = 5 - $raRating;

																					for( $i = 1; $i <= $raRating; $i++ ) {
																						echo '<i class="fa fa-star" aria-hidden="true"></i> ';
																					}

																					if( fmod($raRating, 1) !== 0.00 ) {
																						echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
																					}

																					for( $i = 1; $i <= $remaining; $i++ ) 
																					{
																						echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
																					}
																				 ?>
																			</div>
																			<div class="date"><?php echo e(isset($review['reviewDate']) ? $review['reviewDate'] : ''); ?></div>
																		</div>
																		<div class="cs-name"><?php echo e(ucwords(isset($review['reviewer']) ? $review['reviewer'] : '')); ?></div>
			
																		<div class="cs-info"><?php echo e(isset($review['reviewSummary']) ? $review['reviewSummary'] : ''); ?></div>
																		<div class="cs-message"><?php echo isset($review['description']) ? $review['description'] : ''; ?></div>
																	</div>
																</li>		
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
														</ul>
													</div>
												<?php endif; ?>
												<!-- <div class="cs-more text-center">
													<a class="btn btn-round btn-yellow hvr-rectangle-out" href="#"><strong>Read More Reviews</strong></a>
												</div> -->
											</div>

						    			</div>
						    		</div>
						    		<div class="text-right">
						    			<a href="javascript:;" class="btn btn-blue edit-qa">Edit</a>
						    		</div>
						    		<div class="action-qa hidden text-right">
						    			<button class="btn btn-success">Save</button>
						    			<a href="javascript:;" class="btn btn-danger cancel-edit-qa">Cancel</a>
						    		</div>
					    		<?php echo Form::close(); ?>

					    	</div>
					  	</div>
					  	<div id="tab-history" class="tab-pane fade">
					    	<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th>Action Taken</th>
			                                <th class="text-right">Date and Time</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            <?php $__empty_1 = true; $__currentLoopData = $activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			                            	<tr>
			                            		<td><?php echo e($activity->action_taken); ?> : <?php echo e($activity->ip); ?></td>
			                            		<td class="text-right"><?php echo date('M d, o g:i:s', strtotime($activity->date)); ?></td>
			                            	</tr>
			                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			                            <?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		
		// editing profile details
		$('.edit-profile, .cancel-edit-profile').click(function() {
			$('.profile-form, .profile-display, .edit-profile').toggleClass('hidden');
		});

		// editing qa
		$('.edit-qa, .cancel-edit-qa').click(function() {
			$('.action-qa, .edit-qa').toggleClass('hidden');

			$('.desc').each(function(){
				if( $(this).attr('disabled') )
				{
					$(this).removeAttr('disabled');
				} else {
					$(this).attr('disabled', 'disabled');
				}
			});
		});

		$('.edit-social, .cancel-edit-social').click(function(){
			$('.form-social, .desc.social, .social-actions, .edit-social').toggleClass('hidden');
		});

	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>