<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1" style=""><div class="content2">
                    <p><label>Name: </label> <?php echo e($customer->asUser->name); ?></p>
                    <p><label>Email: </label> <?php echo e($customer->asUser->email); ?></p>
                    <p><label>Phone #: </label> <?php echo e($customer->asUser->phone_num); ?>

                </div></td>
            <td></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>