

<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agent-profile pagetitle-off
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<section id="agent-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="profile">
					<?php echo Form::model($agent, ['route' => ['agent.profile.update'], 'class' => 'default-form row', 'files' => true]); ?>

					<div class="item col-xs-12 col-sm-4">
						<div class="photo text-center">
							<?php if( $agent->profile_photo ): ?>
								<img class="img-responsive" src="<?php echo e($agent->profile_photo); ?>">
							<?php else: ?>
								<img class="img-responsive" src="<?php echo e(isset( $zillowApi['proInfo']['photo'] ) ? $zillowApi['proInfo']['photo'] : asset('images/user-blank.svg')); ?>">
							<?php endif; ?>
						</div>						
						<br>
						<div class="form-info form-group <?php echo e(count($errors) == 0 ? 'hidden' : ''); ?>">
							<?php echo Form::file('photo', ['class' => 'form-control']); ?>

						</div>
					</div>

					<div class="item col-xs-12 col-sm-8">
						<div class="info <?php echo e(count($errors) > 0 ? 'hidden' : ''); ?>">
							<h1 class="name"><?php echo e($zillowApi != null ? $zillowApi['proInfo']['name'] : $agent->name); ?></h1>
							<div class="company">
								<?php echo e($zillowApi != null ? $zillowApi['proInfo']['businessName'] : ''); ?>

							</div>
							<div class="address">
								<?php echo e($zillowApi != null ? $zillowApi['proInfo']['address'] : ''); ?>

							</div>
							<div class="phone">
								<a href="tel:
								<?php if( $zillowApi != null ): ?>
										<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $zillowApi['proInfo']['phone']) ?>
									<?php else: ?>
										<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $authUser->phone_num) ?>
									<?php endif; ?>">
									<?php if( $zillowApi != null ): ?>
										<?php echo e($zillowApi['proInfo']['phone']); ?>

									<?php else: ?>
										<?php echo e($authUser->phone_format); ?>

									<?php endif; ?>
								</a>
							</div>
							<div class="websites">
								<ul class="list-inline">
									<?php if($agent->asAgent->zillow_url): ?>
									<li>
										<a href="<?php echo e($agent->asAgent->zillow_username); ?>" target="_blank">
											<img src="<?php echo e(asset('images/zillow-icon.png')); ?>">
										</a>
									</li>
									<?php endif; ?>
									<?php if($agent->asAgent->linkedin_url): ?>
									<li>
										<a href="<?php echo e($agent->asAgent->linkedin_username); ?>" target="_blank">
											<img src="<?php echo e(asset('images/linkedin-icon.png')); ?>">
										</a>
									</li>
									<?php endif; ?>
									<?php if($agent->asAgent->website_url): ?>
									<li>
										<a href="<?php echo e($agent->asAgent->website_url); ?>" target="_blank">
											<img src="<?php echo e(asset('images/earth-icon.png')); ?>">
										</a>
									</li>
									<?php endif; ?>
									<?php if($agent->asAgent->getOtherData('facebook_url')): ?>
										<li>
											<a href="<?php echo e($agent->asAgent->getOtherData('facebook_url')); ?>" target="_blank">
												<img src="<?php echo e(asset('images/facebook-icon.png')); ?>">
											</a>
										</li>
									<?php endif; ?>
									<?php if($agent->asAgent->getOtherData('yelp_url')): ?>
										<li>
											<a href="<?php echo e($agent->asAgent->getOtherData('yelp_url')); ?>" target="_blank">
												<img src="<?php echo e(asset('images/yelp-icon.png')); ?>">
											</a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						</div>						
						<div class="form-info <?php echo e(count($errors) == 0 ? 'hidden' : ''); ?>">							
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<label>First Name</label>
										<?php echo Form::text('first_name', null, ['class' => 'form-control']); ?>

									</div>
									<div class="col-xs-12 col-sm-6">
										<label>Last Name</label>
										<?php echo Form::text('last_name', null, ['class' => 'form-control']); ?>					
									</div>
								</div>									
							</div>

							<div class="form-group">
								<label>Phone #</label>
								<?php echo Form::text('phone_num', $agent->phone_format, ['class' => 'form-control validate-phone-num']); ?>

							</div>

							<div class="form-group text-right">
								<button class="btn btn-success hvr-sweep-to-left">Save</button>
								<a href="javascript:;" class="btn btn-danger hvr-sweep-to-left cancel-edit-profile">Cancel</a>
							</div>
						</div>

						<div class="text-right">
							<a href="javascript:;" class="btn btn-blue hvr-sweep-to-left edit-profile <?php echo e(count($errors) > 0 ? 'hidden' : ''); ?>">Edit</a>
						</div>
					</div>
					<?php echo Form::close(); ?>


					<div class="item">
						<div class="buttons">
							<?php if( $authUser->isCustomer() ): ?>
								<a href="javascript:;" class="btn btn-pink btn-round hvr-sweep-to-left">Hire Me</a>
								<a href="javascript:;" class="btn btn-blue btn-round hvr-sweep-to-left">Contact Me</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<br>
				<?php echo $__env->make('partials.form-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
		</div>
	</div>
</section>
<section id="agent-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
		
				<div class="info-wrapper">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<h2 class="heading">Experience Levels</h2>
							<div class="content">
								<div class="rating-list">
									<div class="list">
										<div class="head">Local Knowledge</div>
										<div class="rate"><?php echo e(isset($zillowApi['proInfo']['localknowledgeRating']) ? $zillowApi['proInfo']['localknowledgeRating'] : 0); ?></div>
									</div>
									<div class="list">
										<div class="head">Process Expertise</div>
										<div class="rate"><?php echo e(isset($zillowApi['proInfo']['processexpertiseRating']) ? $zillowApi['proInfo']['processexpertiseRating'] : 0); ?></div>
									</div>
									<div class="list">
										<div class="head">Negotiation Skills:</div>
										<div class="rate"><?php echo e(isset($zillowApi['proInfo']['negotiationskillsRating']) ? $zillowApi['proInfo']['negotiationskillsRating'] : 0); ?></div>
									</div>
									<div class="list">
										<div class="head">Responsive:</div>
										<div class="rate"><?php echo e(isset($zillowApi['proInfo']['responsivenessRating']) ? $zillowApi['proInfo']['responsivenessRating'] : 0); ?></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<h2 class="heading">Areas Serviced</h2>
							<div class="content">
								<div class="area-list">
									<?php if( isset($zillowApi['proInfo']['serviceAreas']['area']) ): ?>
										<?php $__currentLoopData = $zillowApi['proInfo']['serviceAreas']['area']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $area): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="list"><?php echo e($area); ?></div>								
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<hr/>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<?php echo Form::model($agent, ['route' => 'agent.profile.update-qa', 'class' => 'default-form']); ?>

							<div class="content-info">
								<h2 class="heading">Why Should You Hire Me?</h2>
								<div class="content qa-info"><?php echo e($agent->asAgent->getOtherData('question_1')); ?></div>

								<div class="form-group qa-field hidden">
									<?php echo Form::textarea('question_1', $agent->asAgent->getOtherData('question_1'), ['class' => 'form-control', 'rows' => 7]); ?>

								</div>
							</div>
							<div class="content-info">
								<h2 class="heading">What Do I Better Than Most Agents?</h2>
								<div class="content qa-info"><?php echo e($agent->asAgent->getOtherData('question_2')); ?></div>

								<div class="form-group qa-field hidden">
									<?php echo Form::textarea('question_2', $agent->asAgent->getOtherData('question_2'), ['class' => 'form-control', 'rows' => 7]); ?>

								</div>

								<div class="form-group qa-field hidden text-right">
									<button class="btn btn-success hvr-sweep-to-left">Save</button>
									<a href="javascript:;" class="btn btn-danger hvr-sweep-to-left cancel-edit-qa">Cancel</a>
								</div>
								<div class="form-group text-right">
									<a class="btn btn btn-blue hvr-sweep-to-left edit-qa">Edit</a>
								</div>
							</div>
							<?php echo Form::close(); ?>

							<div class="content-info">
								<h2 class="heading">What Do Others Say About Me?</h2>
								<div class="content">
									<div class="comments-section">
										<!-- <div class="cs-write">
											<a class="btn btn-pink btn-block hvr-rectangle-out" href="#">WRITE A REVIEW</a>
										</div> -->
										<?php if( isset($zillowApi['proReviews']['review']) && count($zillowApi['proReviews']['review']) ): ?>
											<div class="cs-list">
												<ul>
													<?php $__currentLoopData = $zillowApi['proReviews']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<li>
															<div class="cs-item">
																<div class="top">
																	<div class="stars">
																		<?php 
																			$r1 = floatval( isset($review['localknowledgeRating']) ? $review['localknowledgeRating'] : 0 );
																			$r2 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																			$r3 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
																			$r4 = floatval( isset($review['negotiationskillsRating']) ? $review['negotiationskillsRating'] : 0 );

																			$raRating = ($r1 + $r2 + $r3 + $r4) / 4;

																			$remaining = 5 - $raRating;

																			for( $i = 1; $i <= $raRating; $i++ ) {
																				echo '<i class="fa fa-star" aria-hidden="true"></i> ';
																			}

																			if( fmod($raRating, 1) !== 0.00 ) {
																				echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
																			}

																			for( $i = 1; $i <= $remaining; $i++ ) 
																			{
																				echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
																			}
																		 ?>
																	</div>
																	<div class="date"><?php echo e(isset($review['reviewDate']) ? $review['reviewDate'] : ''); ?></div>
																</div>
																<div class="cs-name"><?php echo e(ucwords(isset($review['reviewer']) ? $review['reviewer'] : '')); ?></div>
	
																<div class="cs-info"><?php echo e(isset($review['reviewSummary']) ? $review['reviewSummary'] : ''); ?></div>
																<div class="cs-message"><?php echo isset($review['description']) ? $review['description'] : ''; ?></div>
															</div>
														</li>		
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
												</ul>
											</div>

											<div class="zillow-url">
												<a href="#">See all Reviews on Zillow</a>
												<div class="powered">
													<span>Powered by</span>
													<img src="<?php echo e(asset('images/logo-zillow.png')); ?>" class="img-responsive">
												</div>
											</div>
										<?php endif; ?>
										<!-- <div class="cs-more text-center">
											<a class="btn btn-round btn-yellow hvr-rectangle-out" href="#"><strong>Read More Reviews</strong></a>
										</div> -->
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		
		$('.edit-profile, .cancel-edit-profile').click(function(){
			$('.info, .form-info, .edit-profile').toggleClass('hidden');
		});

		$('.edit-qa, .cancel-edit-qa').click(function(){
			$('.qa-info, .qa-field, .edit-qa').toggleClass('hidden');
		});

	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>