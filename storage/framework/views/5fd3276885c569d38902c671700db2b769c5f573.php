<?php echo $__env->make('layouts.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div id="page-wrapper">
		<div id="page-content">				
			<?php echo $__env->yieldContent('content'); ?>
		</div>			
	</div>

<?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>