<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2" bgcolor="#ffffff">
                    <h3>Good News <?php echo e($agent->asUser->name); ?>!</h3>
                    <p>You've been selected to submit a proposal for <?php echo e($customer->asUser->name); ?></p>
                    <p>You can find this information on your dashboard at HomeByHome.com as well as below. <?php echo e($customer->asUser->name); ?> is looking to <?php echo e($listing->list_type == 'seller' ? 'Sell' : 'Buy'); ?> a home that is:</p>
                    <p><label>Sq. Ft.: </label> <?php echo e($listing->square_feet); ?></p>
                    <p><label>Bedroom: </label> <?php echo e($listing->bedrooms); ?></p>
                    <p><label>Bathrooms: </label> <?php echo e($listing->bathrooms); ?></p>
                    <p><label>Zip Postal: </label> <?php echo e($listing->zip_postal); ?></p>
                    <p><label>Budget: </label>
                        <?php if($listing->text_budget): ?>
                            <?php echo e($listing->text_budget); ?>

                        <?php else: ?>
                            <?php echo e($listing->budget); ?>

                        <?php endif; ?>
                    </p>
                    <p><label>Buying Until: </label>
                        <?php if($listing->text_until): ?>
                            <?php echo e($listing->text_until); ?>

                        <?php else: ?>
                            <?php echo date('M d, o', strtotime($listing->until)); ?>
                        <?php endif; ?>
                    </p>
                    <br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                    Thanks again,<br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                </div></td>
            <td></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>