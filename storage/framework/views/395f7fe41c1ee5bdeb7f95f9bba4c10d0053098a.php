<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
    account-login pagetitle-off footer-off
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?> 
<section id="account-login" class="hero-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
               
                <div class="panel panel-default">
                    
                    <div class="panel-body">
                       
                        <div class="panel-right">
                            <div class="panel-logo"><img src="<?php echo e(asset('images/logo-login.svg')); ?>" alt="Home by Home"></div>
                            <h2 class="panel-heading">Welcome to Home by Home</h2>
                            
                            <form class="default-form" method="POST" action="<?php echo e(url('login')); ?>">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                 
                                        <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autofocus>

                                        <?php if($errors->has('email')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                  
                                </div>

                                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                    
                                      <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>

                                        <?php if($errors->has('password')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                         <div class="text-right">
                                            <a class="forgot" href="<?php echo e(route('password.request')); ?>">Forgot Your Password?</a>
                                        </div>
                                </div>
                               

                                <div class="form-group hidden">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Remember Me
                                                <a href="<?php echo e(url('register')); ?>">Register</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-ctrl">
                                    <button type="submit" class="btn btn-block btn-pink  ">Continue</button>
                                    <hr/>
                                    <a  href="<?php echo e(route('facebook.login')); ?>" type="submit" class="btn btn-block btn-blue   fb-log"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;&nbsp; Login with Facebook</a>
                                </div>
                            </form>   
                        </div>

                    </div>
                 </div>

    
               
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        windowResize();

        $(window).resize(function(){
            windowResize();
        });
        $(window).load(function(){
            $(window).resize();
        });
    });

    function windowResize(){
        var WindowHeight = $(window).height(),
            HeaderHeight = $('#header').outerHeight(),
            FooterHeight = $('#footer').outerHeight();

        $('#page-wrapper').each(function(){
        

            var ContainerHeight = $(this).outerHeight();
            if( WindowHeight > ( ContainerHeight + HeaderHeight + FooterHeight ) ) {

                TotalPadding = ( WindowHeight - ( ContainerHeight + HeaderHeight + FooterHeight ) ) / 2 ;

                

            }else{

                TotalPadding = 0;
            }

            $(this).css('padding-top', TotalPadding+'px').css('padding-bottom', TotalPadding+'px')
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>