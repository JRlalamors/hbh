

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Agents
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="agents-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Agency</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__empty_1 = true; $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td class="text-center"><?php echo e($agent->asUser->name); ?></td>
                                    <td class="text-center"><?php echo e($agent->asUser->email); ?></td>
                                    <td class="text-center"><?php echo e($agent->getOtherData('admin_agency') ? $agent->getOtherData('admin_agency') : $agent->getOtherData('zillow_results')['proInfo']['businessName']); ?></td>
                                    <td class="text-center"><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $agent->asUser->phone_num) ?>" class="default"><?php echo e($agent->asUser->phone_format); ?></a></td>
                                    <td class="text-center"><a href="<?php echo e(route('admin.agents.profile', $agent)); ?>" class="blue">View Profile</a></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr><td colspan="5">Agents not found..</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>                   
                </div>

                <div class="pagination">
                    <?php if( $agents instanceof Illuminate\Pagination\LengthAwarePaginator ): ?>
                        <?php echo e($agents->links()); ?>

                    <?php endif; ?>
                </div>

			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>