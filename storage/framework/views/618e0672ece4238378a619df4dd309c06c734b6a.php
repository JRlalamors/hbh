

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer customer-deals
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
    <?php echo e($pageTitle); ?>

<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                            </tr>
                        </thead>
                        <tbody>                            
                            <?php $__empty_1 = true; $__currentLoopData = $listings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <?php if (!$listing->customer || !$listing->customer->asUser) continue; ?>
                                <tr>
                                    <td class="text-center"><?php echo e($listing->id); ?></td>
                                    <td class="text-center"><?php echo e($listing->customer->asUser->name); ?></td>
                                    <td class="text-center"><?php echo e($listing->customer->asUser->email); ?></td>
                                    <td class="text-center"><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $listing->customer->asUser->phone_num) ?>" class="default"><?php echo e($listing->customer->asUser->phone_format); ?></a></td>
                                    <td class="text-center"><?php echo e($listing->list_type == 'buyer' ? 'Y' : 'N'); ?></td>
                                    <td class="text-center"><?php echo e($listing->list_type == 'seller' ? 'Y' : 'N'); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr><td colspan="10">Listings not found..</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                	<?php if( $listings instanceof Illuminate\Pagination\LengthAwarePaginator ): ?>
                        <?php echo e($listings->links()); ?>

                    <?php endif; ?>
                </div>

			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>