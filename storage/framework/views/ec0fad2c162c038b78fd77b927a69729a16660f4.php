<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo e(isset($description) ? $description : null); ?>">
    <meta name="author" content="">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="base-url" content="<?php echo e(url('/')); ?>">

    <title><?php echo e(isset($title) ?  $title : 'Home by Home'); ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('favicons/apple-touch-icon.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('favicons/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="256x256"  href="<?php echo e(asset('favicons/android-icon-256x256.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('favicons/favicon.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('favicons/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('favicons/favicon-16x16.png')); ?>">

    <link href="<?php echo e(asset('bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bower_components/metisMenu/dist/metisMenu.min.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/vendor-styles.css')); ?>">
    <link href="<?php echo e(asset('css/worksans.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('bower_components/sweetalert/dist/sweetalert.css')); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo e(asset('js/modernizr-2.8.3-respond-1.4.2.min.js')); ?>"></script>
    <link href="<?php echo e(asset('css/app.css')); ?>?v=1312018" rel="stylesheet">

    <?php echo $__env->yieldContent('styles'); ?>

    <?php echo $__env->yieldPushContent('stack-css'); ?>

    <?php echo $__env->yieldContent('head-scripts'); ?>



    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

    })(window,document,'script','dataLayer','GTM-W7DNMG6');</script>

    <!-- End Google Tag Manager -->

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        function recaptchaSuccess() {   
            $('.rc-button').removeAttr('disabled');
        }
    </script>
</head>

<body class="<?php echo $__env->yieldContent('body-class'); ?>">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7DNMG6"

    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->
<?php echo $__env->make('layouts.partials.loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="mobile-menu">
    <div class="wrapper">
        <div class="content">
        <h2 class="heading">Main Menu</h2> 
        <a href="javascript:;" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> 
        <?php if( $authUser ): ?>
            <?php if( $authUser->isAdmin() ): ?>
                <?php echo $__env->make('layouts.partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>
                <?php echo $__env->make('layouts.partials.user-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        <?php else: ?>                        
            <?php echo $__env->make('layouts.partials.user-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
        </div>  
    </div>
</div>
<header id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
            
                <div class="wrapper-table">
                    <div class="inner">
                        <div class="item">
                            <a href="
                                <?php 
                                    if( $authUser ) {
                                        if( $authUser->isAdmin() ) {
                                            echo route('admin.dashboard');
                                        }
                                        else {
                                            echo route('home');
                                        }
                                    }
                                    else {
                                        echo route('home');
                                    }
                                 ?>
                            " class="logo"><img src="<?php echo e(asset('images/logo-homebyhome.svg')); ?>" alt="Home by Home"></a>
                        </div>
                        <div class="item">
                            <a class="mobile-ctrl">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                            <?php if( $authUser ): ?>
                                <?php if( $authUser->isAdmin() ): ?>
                                    <?php echo $__env->make('layouts.partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <?php else: ?>
                                    <?php echo $__env->make('layouts.partials.user-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <?php endif; ?>
                            <?php else: ?>                        
                                <?php echo $__env->make('layouts.partials.user-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>  
    </div>    
</header>
<?php echo $__env->make('layouts.partials.notice', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>