

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents dashboard pagetitle-off
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Listing Details 
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="agent-profile" class="default-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="profile">
                    <div class="item">
                        <div class="photo">
                            <img class="img-responsive" src="<?php echo e(asset('images/user-blank.svg')); ?>">
                        </div>
                    </div><div class="item">
                        <div class="info">
                            <h1 class="name">
                               <?php echo e($list->customer->Asuser->name); ?> 
                            </h1>
                            <div class="websites">
                                <a href="<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $list->customer->asUser->phone_num) ?>"><?php echo e($list->customer->asUser->phone_format); ?></a>                                
                            </div>
                        </div>
                    </div><div class="item">
                        <div class="buttons">
                            <a href="<?php echo e(route('admin.customers.profile', $list->customer)); ?>" class="btn btn-pink btn-round">Back</a>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="agent-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
        
                <div class="info-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Request Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Type:</div>
                                        <div class="rate"><?php echo e($list->type()); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Property type:</div>
                                        <div class="rate"><?php echo e($list->property->human_display); ?></div>
                                    </div>
                                    <?php if( $list->list_type == 'seller' ): ?>	
                                    <div class="list">
                                        <div class="head">Address:</div>
                                        <div class="rate"><?php echo e($list->complete_address); ?> </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="list">
                                        <div class="head">ZIP/Postal:</div>
                                        <div class="rate"><?php echo e($list->zip_postal); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">SQ. Ft.:</div>
                                        <div class="rate"><?php echo e($list->square_feet); ?></div>
                                    </div>
									<div class="list">
                                        <div class="head">Bedrooms:</div>
                                        <div class="rate"><?php echo e($list->bedrooms); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Bathrooms:</div>
                                        <div class="rate"><?php echo e($list->bathrooms); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">
                                            <?php if($list->list_type == 'seller'): ?>
                                                How soon customer looking to sell:
                                            <?php else: ?>
                                                How soon customer looking to buy:
                                            <?php endif; ?>
                                        </div>
                                        <div class="rate">
                                            <?php if($list->text_until): ?>
                                                <?php echo e($list->text_until); ?>

                                            <?php else: ?>
                                                <?php echo date('M d, o', strtotime($list->until)); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if( $list->selectedProposal() ): ?>
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="heading">Proposal</h2>
                            <div class="content">
                                <div class="data-list">
                                    <div class="list">
                                        <div class="head">Agent Name:</div>
                                        <div class="rate"> <?php echo e($list->selectedAgent->asUser->name); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Proposal Type:</div>
                                        <div class="rate">
                                            <?php if( $list->list_type == 'seller' ): ?>
                                                <?php if( $list->selectedProposal()->rebate_type == 'percentage' ): ?>
                                                    <?php echo e('Total Commission Rate'); ?>

                                                <?php else: ?>
                                                    <?php echo e('Flat Fee'); ?>

                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if( $list->selectedProposal()->rebate_type == 'percentage' ): ?>
                                                    <?php echo e('Commission Rebate'); ?>

                                                <?php else: ?>
                                                    <?php echo e('Cash Rebate'); ?>

                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Rebate:</div>
                                        <div class="rate"><?php echo e($list->selectedProposal()->rebate_format); ?> </div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Customer Name:</div>
                                        <div class="rate"><?php echo e($list->customer->name); ?></div>
                                    </div>
                                    <div class="list">
                                        <div class="head">Closed deal?:</div>
                                        <div class="rate"><?php echo e($list->closed_deal ? 'Yes' : 'No'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>


                   	</div>
                </div>
            </div>
         </div>
    </div>
</section>
	

<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>