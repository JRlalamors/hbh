<nav class="page-nav">
    <ul class="list-inline">
        <li><a href="<?php echo e(route('admin.dashboard')); ?>">Dashboard</a></li>
        <li><a href="<?php echo e(route('admin.agents')); ?>">Agents</a></li>
        <li><a href="<?php echo e(route('admin.customers')); ?>">Customers</a></li>
        <li><a href="<?php echo e(route('admin.process')); ?>">Process</a></li>
        <li class="dropdown"><a href="<?php echo e(route('admin.settings')); ?>"><span class="fa fa-cog"></span></a>
    		<ul class="dropdown-menu">
                <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
            </ul>
        </li>
    </ul>
</nav>