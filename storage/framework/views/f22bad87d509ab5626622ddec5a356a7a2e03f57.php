<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <h3 class="beginning-text">Hello <?php echo e($agent->asUser->name); ?></h3>
                    <p>Welcome to HomebyHome, thank you for registering to our website. Before you can use your account, this is subject to approval by the website administrator, keep your lines open for validation.</p>
                    Thanks again,<br>
                </div></td>
            <td></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>