

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	See All Customers
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Customer ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                                <th class="text-center">Agent Proposal #1</th>
                                <th class="text-center">Agent Proposal #2</th>
                                <th class="text-center">Agent Proposal #3</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__empty_1 = true; $__currentLoopData = $lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr class="text-center" data-id="<?php echo e($list->id); ?>">
                                    <td><?php echo e($list->customer->id); ?></td>
                                    <td><?php echo e($list->customer->asUser->name); ?></td>
                                    <td><?php echo e($list->customer->asUser->email); ?></td>
                                    <td><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $list->customer->asUser->phone_num) ?>"><?php echo e($list->customer->asUser->phone_format); ?></a></td>
                                    <td><?php echo e($list->list_type == 'buyer' ? 'Y' : 'N'); ?></td>
                                    <td><?php echo e($list->list_type == 'seller' ? 'Y' : 'N'); ?></td>

                                    <?php if( $list->agents->count() > 0 ): ?>
                                        <?php $__currentLoopData = $list->agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <td><a href="javascript:;" class="blue assign-agent" data-toggle="modal" data-target="#assignAgent"><?php echo e($agent->asUser->name); ?></a></td>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php for( $i = 1; $i <= 3 - $list->agents->count(); $i++ ): ?>
                                        <td><a href="javascript:;" class="blue assign-agent" data-toggle="modal" data-target="#assignAgent">Assign</a></td>
                                    <?php endfor; ?>
 
                                    <td><a href="<?php echo e(route('admin.customers.profile', $list->customer)); ?>" class="blue">View Profile</a></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr><td colspan="10">Customers not found..</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">                	
                    <?php if( $lists instanceof Illuminate\Pagination\LengthAwarePaginator ): ?>
                        <?php echo e($lists->links()); ?>

                    <?php endif; ?>
                </div>

			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.customer.agent-form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function(){

            var $baseUrl = $('meta[name="base-url"]').attr('content'),
                $listID,
                $theData = {};              

            $('.assign-agent').click(function() {
                $listID = $(this).closest('tr').data('id');
            });

            $('.agents-select').change(function(){  

                var $agentID = $(this).val(),
                    $agentName = $(this).find('option:selected').text();

                // push new values
                $theData.userAgents = {}
                $theData.userAgents[$agentID] = $agentName

                // push new values
                $('.agents-select').not($(this)).each(function($index){
                    $theData.userAgents[$(this).val()] = $(this).find('option:selected:not(:first-child)').text();
                });

                $('.agents-select').not($(this)).each(function(){
                    $(this)
                        .find('option:not(:first-child)')
                            .each(function(){
                                if( $(this).val() in $theData.userAgents ) {
                                    $(this).attr('disabled', 'disabled');
                                }
                                else {
                                    $(this).removeAttr('disabled');
                                }
                            })
                        .end()
                        .find('option:selected:not(:first-child)')
                            .removeAttr('disabled');
                });

            });

            // load all agents in modal with selected per each row
            $('#assignAgent').on('show.bs.modal', function(e) {
                $.ajax({
                    type: 'POST',
                    url: $baseUrl + '/admin/customers/list/' + $listID + '/edit-agents',
                    success: function($data) {

                        $('#assignAgent .set-agent').data("listid", $data.list.id);

                        // change modal header
                        $('#assignAgent .modal-title').text('Assign agents for ' + $data.customer.as_user.first_name + ' ' + $data.customer.as_user.last_name);

                        $('.agents-select').each(function($index){
                            var $this = $(this);
                            $this.html("");
                            $this.append('<option value="" selected>No Agent</option>');

                            // Append Agents
                            Object.keys($data.agentsArr).forEach(function($key){
                                var $selected = '',
                                    $disabled = '';
                                
                                // check if the object key == index and key exists
                                if( Object.keys($data.userAgents)[$index] == $key ) {                                   
                                    $selected = 'selected';
                                }

                                if( $key in $data.userAgents && Object.keys($data.userAgents)[$index] != $key ) {
                                    $disabled = 'disabled';                                 
                                }
                                
                                $this.append('<option value="'+$key+'" '+$selected+' '+$disabled+'>'+$data.agentsArr[$key].name+' ('+$data.agentsArr[$key].email+')</option>');
                            });
                        });

                        $theData = $data;
                    }
                });
            });

            $('.set-agent').click( function() {

                var $listID = $(this).data('listid'),
                $agents = [
                    $(this).closest('.modal').find('[name="agent-1"]').val(),
                    $(this).closest('.modal').find('[name="agent-2"]').val(),
                    $(this).closest('.modal').find('[name="agent-3"]').val()
                ];

                $agents = $agents.filter(Boolean);

                $.ajax({
                    type: 'POST',
                    url: baseUrl + '/admin/customers/list/' + $listID + '/store-list-agents',
                    data: {'agents' : $agents},
                    success: function( $data ) {
                        if( $data.message == "success" ) 
                        {
                            $('#assignAgent').modal('hide');                            
                            window.location = $data.route;
                        }
                    }
                });

            });

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>