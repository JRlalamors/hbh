<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container"><!-- content -->
            <table class="footer-upper">
                <tr>
                    <td colspan="3"><p>Please email or call us (855-600-8117) with any questions.</p></td>
                </tr>
            </table>
            <table class="footer-lower">
                <tr>
                    <td><img class="footer-logo" src="https://www.homebyhome.com/images/emails/logo-homebyhome.svg"></td>
                    <td><p><a href="#"><img src="https://www.homebyhome.com/images/emails/twittericon.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/facebook.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/insta.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/youtube.png"></a></p></td>
                    <td style="text-align:right;"><p> © 2017 HOMEBYHOME.COM <br>All right reserved</p></td>
                </tr>
            </table>
            <!-- /content --></td>
        <td></td>
    </tr>
</table>