

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Search Agents
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>

<section class="agents-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<?php echo Form::open(['route' => 'admin.agents.search-results', 'class' => 'default-form']); ?>

				<!-- <form class="default-form" > -->
						
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">							
							<div class="form-group">
								<label class="control-label" for="name">Name</label>
								<div class="control-form"><?php echo Form::text('name', null, ['class' => 'form-control', 'id' => 'name']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="license">License #</label>
								<div class="control-form"><?php echo Form::text('license', null, ['class' => 'form-control', 'id' => 'license
								']); ?></div>		
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="broker">Broker Name</label>
								<div class="control-form"><?php echo Form::text('broker', null, ['class' => 'form-control', 'id' => 'broker']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="city">City</label>
								<div class="control-form"><?php echo Form::text('city', null, ['class' => 'form-control', 'id' => 'city']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="state">State</label>
								<div class="control-form"><?php echo Form::text('state', null, ['class' => 'form-control', 'id' => 'state']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="zip">Zip</label>
								<div class="control-form"><?php echo Form::text('zip', null, ['class' => 'form-control', 'id' => 'zip']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="email">Email</label>
								<div class="control-form"><?php echo Form::email('email', null, ['class' => 'form-control', 'id' => 'email']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="phone">Phone</label>
								<div class="control-form"><?php echo Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<div class="form-controls">
								<button class="btn btn-pink">Search</button>
							</div>	
						</div>
					
						
					</div>
				<!-- </form> -->
				<?php echo Form::close(); ?>




			</div>
		</div>
	</div>
</section>
<section class="agents-menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="agmenu-wrapper">
					<div class="agmenu-list equal-items">
						<div class="item">
							<a href="<?php echo e(route('admin.agents.review')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/agents/Agents-to-review.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-customerleads.svg')); ?>)"></div>
									<h2 class="title">AGENTS TO REVIEW</h2>
								</div>
							</a>
						</div><div class="item">
							
							<a href="<?php echo e(route('admin.agents.view')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/agents/See-All-Agents.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-agentleads.svg')); ?>)"></div>
									<h2 class="title">SEE ALL AGENTS</h2>
								</div>
							</a>

						</div><div class="item">
							<a href="<?php echo e(route('admin.agents.deals')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/agents/Curent-deals-out-for-proposal.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-dealsmade.svg')); ?>)"></div>
									<h2 class="title">CURRENT DEALS OUT FOR PROPOSAL</h2>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>	
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>