

<!-- Added Styles -->

<?php $__env->startSection('body-class'); ?>
pagetitle-off
<?php $__env->stopSection(); ?>


<!-- Content -->
<?php $__env->startSection('content'); ?> 
  <section id="contact-content" class="default-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <div class="heading">
          
            <h1 class="content-heading">Contact Us</h1>
            <h3>Contact one of our Home By Home specialists to see how we can help you save thousands of dollars on your next home purchase or sale.</h3>

          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-xs-12 col-sm-6">

          <?php echo $__env->make('partials.form-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <?php echo Form::open(['route' => 'request-contact', 'class' => 'default-form recaptcha-form']); ?>

            <div class="form-group">
              <?php echo Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'FIRST NAME']); ?>

            </div>
            <div class="form-group">
              <?php echo Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'LAST NAME']); ?>

            </div>
            <div class="form-group">
              <?php echo Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'EMAIL']); ?>

            </div>
            <div class="form-group">
              <?php echo Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'COMPANY']); ?>

            </div>
            <div class="form-group">
              <?php echo Form::text('phone', null, ['class' => 'form-control validate-phone-num', 'placeholder' => 'PHONE']); ?>

            </div>
            <div class="form-group">
              <?php echo Form::textarea('question_comment', null, ['class' => 'form-control', 'placeholder' => 'QUESTION OR COMMENT']); ?>

            </div>
            <div class="form-group">                         
                <div class="g-recaptcha" data-sitekey="6LdAITYUAAAAAL7nw9TfeaGfZJRRStTOmY5vNjuC" data-callback="recaptchaSuccess"></div>
            </div>
             <div class="form-ctrl">
              <button disabled class="btn btn-block btn-pink btn-round  rc-button">CONTACT US</button>
             </div>
           <?php echo Form::close(); ?>

        </div>
        <div class="col-xs-12 col-sm-6">
          <iframe class="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3302.0769212356554!2d-118.71508948439735!3d34.144374120316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80e820d0a6a953a7%3A0x15bbef2acfbd0217!2s5222+Cangas+Dr%2C+Agoura+Hills%2C+CA+91301%2C+USA!5e0!3m2!1sen!2sph!4v1501494104672" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>


          <div class="row">

            <div class="col-xs-12 col-sm-6">

              <p><strong>Our Address</strong><br>
              5222 Cangas Dr.<br>
              Calabasas, CA 91301
              </p>

            </div>

            <div class="col-xs-12 col-sm-6">
            <p><strong>Call Us Today</strong><br>
              855 600 8117
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>