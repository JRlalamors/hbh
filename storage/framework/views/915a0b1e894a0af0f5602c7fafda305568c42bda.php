

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	home
<?php $__env->stopSection(); ?>
<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="home-banner-area" style="background-image:url(<?php echo e(asset('images/banner-landing.jpg')); ?>)">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <h1 class="banner-title">Find the Best Real Estate Agents Near You</h1>
            <?php 
                if( $authUser )
                {
                    $route = 'customer.list.add';
                } else {    
                    $route = 'register';
                }

             ?>
            <?php echo Form::open(['route' => $authUser ? 'customer.list.add' : 'register', 'class' => 'form autosearchome', 'method' => 'get']); ?>

                <div class="form-group">                    
                    <div class="autocomplete">                              
                        <div class="pac-card" id="pac-card">
                            <div id="pac-container">
                                <input id="pac-input" type="text" placeholder="Enter your Zip code to begin" class="form-control text-center">
                            </div>
                        </div>
                        <div id="map" class="hidden"></div>                    
                    </div>
                    <input type="hidden" name="street_number" id="street_number">
                    <input type="hidden" name="route" id="route">
                    <input type="hidden" name="administrative_area_level_1" id="administrative_area_level_1">
                    <input type="hidden" name="locality" id="locality">
                    <input type="hidden" name="postal_code" id="postal_code">
                    <button class="btn btn-pink">Find Agent</button>
                </div>
            <?php echo Form::close(); ?>


            <h4 class="banner-sub">Compare up to 3 custom agent proposals to <br/>
            save thousands in commissions or rebates</h4>
            </div>
        </div>
    </div>
</section>
<section id="home-about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="global-heading">
                    <h2 class="title">AS SEEN ON</h2>
                </div>
                <div class="logos-wrapper">
                    <div class="logos">
                        <div class="item">
                            <div class="item-wrapper">
                                <img src="<?php echo e(asset('images/tv-5.jpg')); ?>" alt="ABC" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrapper">
                                <img src="<?php echo e(asset('images/tv-4.jpg')); ?>" alt="CBS" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrapper">
                                <img src="<?php echo e(asset('images/tv-3.jpg')); ?>" alt="FOX" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrapper">
                                <img src="<?php echo e(asset('images/tv-2.jpg')); ?>" alt="NBC" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrapper">
                                <img src="<?php echo e(asset('images/tv-1.jpg')); ?>" alt="Realty Times" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="global-heading">
                    <div class="icon hidden"><img src="<?php echo e(asset('images/icon-heading.svg')); ?>" alt="Home by Home"></div>
                    <h2 class="title">ITS SIMPLE AND FREE</h2>
                    <div class="desc hidden">Using our service is as easy as 1 - 2 - 3</div>
                </div>
                <div class="hiw-wrapper">
                    <div class="hiw-list">
                        <div class="item">
                            <div class="item-wrapper">
                                <div class="icon"><img src="<?php echo e(asset('images/service-1.png')); ?>" alt="Information"></div>
                                <div class="title-wrapper">
                                    <div class="title">REGISTER</div>
                                    <div class="desc">Tell us a little about you</div>
                                </div>
                            </div>
                        </div><div class="item divider"></div><div class="item">
                            <div class="item-wrapper">
                                <div class="icon"><img src="<?php echo e(asset('images/service-2.png')); ?>" alt="Proposals"></div>
                                <div class="title-wrapper">
                                    <div class="title">REVIEW</div>
                                    <div class="desc">Review agent proposals</div>
                                </div>
                            </div>
                        </div><div class="item divider"></div><div class="item">
                            <div class="item-wrapper">
                                <div class="icon"><img src="<?php echo e(asset('images/service-3.png')); ?>" alt="Buy and Save"></div>
                                <div class="title-wrapper">
                                    <div class="title">SAVE</div>
                                    <div class="desc">Save thousands of dollars</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <hr class="global-divider hidden"/>
            </div>
        </div>
        
    </div>
</section>
<section id="home-howitworks">
    <div class="container">
        <div class="col-xs-12">
            
           
            <div class="global-heading">
                <div class="icon"><img src="<?php echo e(asset('images/icon-heading.svg')); ?>" alt="Home by Home"></div>
                <h2 class="title">TESTIMONIALS</h2>
            </div>
            <div class="tst-wrapper">
                <div class="tst-list">
                    <div class="item">
                        <div class="item-wrapper">
                            <div class="image"><img src="<?php echo e(asset('images/sam-n.jpg')); ?>" alt="Home by Home"></div>
                            <div class="content">
                                <p>I wish I knew about Home by Home when I was buying a house! We really needed a service like this to help us navigate the process of finding an agent as a first time home buyer. I'm telling all my friends.</p>
                            </div>
                            <div class="author"><span class="name">Sam N.</span><span class="location">Northridge, CA</span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-wrapper">
                            <div class="image"><img src="<?php echo e(asset('images/jordan-g.jpg')); ?>" alt="Home by Home"></div>
                            <div class="content">
                                <p>Home By Home is changing the way to find a real estate agent. It’s amazing that they can help someone like me save thousands of dollars when buying a home for the very first time. I highly recommend HomeByHome.com</p>
                            </div>
                            <div class="author"><span class="name">Jordan G.</span><span class="location">Calabasas, CA</span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-wrapper">
                            <div class="image"><img src="<?php echo e(asset('images/evan-g.jpg')); ?>" alt="Home by Home"></div>
                            <div class="content">
                                <p>I was sold as soon as I heard about HomeByHome.com. With their focus on quality and savings I don’t know why anyone would look to find a real estate agent any other way</p>
                            </div>
                            <div class="author"><span class="name">Evan G.</span><span class="location">Los Angeles, CA</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tst-controls hidden">
                <a class="btn btn-blue invert  ">See More Testimonials</a>
            </div>
        </div>
    </div>
</section>

<section id="home-about-again">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 col-lg-8">
                <h3 class="section-heading hidden">All about us</h3>
                <h2 class="section-title">OUR STORY</h2>
                <div class="inner-content">
                    <p>Nearly two years ago, my wife and I decided to sell our home. Surprisingly, one of the biggest challenges we had was finding an agent who had the skills and knowledge we were looking for. There were plenty who made promises of a fast and simple sales process, but it was tough to know which agents had the ability to walk the talk and which of them were just making empty promises in hopes of winning our business. We were fortunate enough to end up with a great agent who helped us navigate snags within the home sale process.</p>

                    <p>Home By Home was started to make the process of finding qualified, experienced agents simpler and the process of buying and selling a home more affordable. We work hard not only to vet the agents who join our network, but also to screen and vet the home buyers and sellers who join Home By Home. We do all of this to ensure that your agent can focus on what they do best – helping you navigate a complex home sales process.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-5 col-lg-4">

                <div class="widget">                    
                    <?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('partials.form-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <h2 class="widget-title">REQUEST A CALL</h2>

                    <?php echo Form::open(['url' => 'request-call', 'class' => 'form recaptcha-form']); ?>

                        <div class="form-group">    
                            <?php echo Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); ?>

                        </div>
                        <div class="form-group">    
                            <?php echo Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email*']); ?>

                        </div>
                        <div class="form-group">
                            <?php echo Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone Number*']); ?>

                        </div>
                        <div class="form-group">                         
                            <div class="g-recaptcha" data-sitekey="6LdAITYUAAAAAL7nw9TfeaGfZJRRStTOmY5vNjuC" data-callback="recaptchaSuccess"></div>
                        </div>
                        <div class="form-ctrls">
                            <button disabled type="submit" class="btn btn-pink btn-round rc-button">Request A Call</button>
                        </div>
                    <?php echo Form::close(); ?>

                </div>

            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>
    <script>
        function initMap() {
            // return;
            var map = new google.maps.Map(document.getElementById('map'));
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');

            var options = {
              types: ['(regions)'],
              componentRestrictions: {country: 'usa'},        
              strictBounds: true
            };

            var autocomplete = new google.maps.places.Autocomplete(input, options);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');

            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                postal_code: 'short_name'
            };

            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              infowindow.close();
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                //window.alert("Address not found: '" + place.name + "'");
                return;
              }

              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }

                fillInAddress();
            });

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();


                // check if the place has postal
                var havePostal = false;
                for( i = 0; i < place.address_components.length; i++ )
                {
                    if( place.address_components[i].types[0] == 'postal_code' ) {
                        havePostal = true;
                    }
                }


                // clear all address inputs
                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }

                if( !havePostal ) {

                    var geo = place.geometry.location,
                        location = new google.maps.LatLng(geo.lat(), geo.lng()),
                        map = new google.maps.Map(document.getElementById('map'), {
                          center: location,
                          zoom: 15
                        }),
                        request = {
                            location: location,
                            radius: '500',
                            type: ['address']
                        };

                    var service = new google.maps.places.PlacesService(map);
                    service.nearbySearch(request, firstZipCode);

                } else {                   

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                      var addressType = place.address_components[i].types[0];
                      if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                      }
                    }
                }
               
            }

            function firstZipCode(results, status) {

                var postalCode;

                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        var place = results[i].geometry.location;

                        var latlng = new google.maps.LatLng(place.lat(), place.lng()); 

                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'latLng': latlng}, function(results, status) {

                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {


                                    // check if the loop has postal
                                    var components = results[0].address_components,
                                        hasPostal;
                                    for( c = 0; c < components.length; c++ )
                                    {
                                        if( results[0].address_components[c].types.includes('postal_code') )
                                        {
                                            hasPostal = true;
                                        }                                       
                                    }

                                    for (j = 0; j < results[0].address_components.length; j++) {
                                        if (hasPostal)
                                        {

                                            // fill in the addresses inputs
                                            if ( results[0].address_components[j].types.length > 1 ) {
                                                
                                                for(var t = 0;  t < results[0].address_components[j].types.length; t++)
                                                {
                                                    var addressType = results[0].address_components[j].types[t],
                                                        val = results[0].address_components[j][componentForm[addressType]];
                                                        if( document.getElementById(addressType) )
                                                        {
                                                            document.getElementById(addressType).value = val;
                                                        }
                                                }

                                            } else {
                                                if( componentForm[ results[0].address_components[j].types ] )
                                                {   
                                                    var addressType = results[0].address_components[j].types;
                                                    var val = results[0].address_components[j][componentForm[addressType]];

                                                    document.getElementById(addressType).value = val;
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                        });

                        break;
                    }
                }

            }

        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUaOvb7g_KLbkUEX-GvbFBqw6Jpgi6Cuw&libraries=places&callback=initMap"
            async defer></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#pac-input').keydown(function(e){

                var key = e.charCode || e.keyCode || 0;

                if( key == 13 )
                {   
                    e.preventDefault();
                }

            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>