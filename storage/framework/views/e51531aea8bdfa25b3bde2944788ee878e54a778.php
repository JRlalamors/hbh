

<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer-profile pagetitle-off
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section id="customer-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="photo"><img class="img-responsive" src="<?php echo e($customer->asUser->profile_photo ? $customer->asUser->profile_photo : asset('images/user-blank.svg')); ?>"></div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="info">
					<h1 class="name"><?php echo e($customer->asUser->name); ?></h1>
					
					<?php if( $customer->asUser->city ): ?>
						<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($customer->asUser->city . " " . $customer->asUser->state); ?></div>
					<?php endif; ?>
					<div class="moreinfo">
						<div class="listitem">
							<div class="title">Email</div><div class="content"><a href="mailto:<?php echo e($customer->asUser->email); ?>"><?php echo e($customer->asUser->email); ?></a></div>
						</div>
						<div class="listitem">
							<div class="title">City</div><div class="content"><?php echo e($customer->asUser->city ?: '--'); ?></div>
						</div>
						<div class="listitem">
							<div class="title">State</div><div class="content"><?php echo e($customer->asUser->state ?: '--'); ?></div>
						</div>
						<div class="listitem">
							<div class="title">Phone</div><div class="content"><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $authUser->phone_num) ?>"><?php echo e($customer->asUser->phone_format); ?></a></div>
						</div>
					</div>
					<div class="ctrls">
						<?php  $deal = $list;  ?>
						<a href="<?php echo e(route('agent.list.details', $deal)); ?>" class="btn btn-pink btn-round hvr-rectangle-out">Edit Proposal</a>
						<a href="<?php echo e(route('agent.customer.profile', $customer)); ?>" class="btn btn-blue btn-round hvr-rectangle-out">See Customer's View</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="customer-moreinfo" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-push-4">

				<div class="cp-container">
					<h2 class="heading">About My Home Needs</h2>
					<div class="content">
						<?php if($list->list_type == 'buyer'): ?>
							<p>Hi, my name is <?php echo e($customer->asUser->name); ?>.  I am looking to buy a <?php echo e($list->property->human_display); ?> in the <?php echo e($list->complete_address); ?> area <?php echo e($list->text_until ? $list->text_until : $list->until); ?>.</p>
							<p>My budget is approximately <?php echo e($list->text_budget ? $list->text_budget : $list->budget); ?>. and <?php echo e($customer->lender_status); ?> for a mortgage.
								<?php if($list->bedrooms && $list->bathrooms): ?><br><br>I am looking for a home that is <?php echo e($list->bedrooms); ?> bedrooms and <?php echo e($list->bathrooms); ?> bathrooms.<?php endif; ?>
							</p>
						<?php else: ?>
							<p>Hi, my name is <?php echo e($customer->asUser->name); ?>. I am looking to sell my <?php echo e(number_format($list->sq_feet_max, 0)); ?> square foot <?php echo e($list->property->human_display); ?> that is <?php echo e($list->bedrooms); ?> bedrooms and <?php echo e($list->bathrooms); ?> in the <?php echo e($list->complete_address); ?> area. <?php echo e($list->text_until ? $list->text_until : $list->until); ?>.
								<br><br>I believe my home is worth between <?php echo e($list->text_budget ? $list->text_budget : $list->budget); ?>.
								<br><br>I look forward to hearing from you.
							</p>
						<?php endif; ?>
						
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-8">
				<div class="sidebar cp-container">
					<div class="listitem">
						<div class="title">Date Signed Up</div>
						<div class="content"><?php echo date('M d, o', strtotime($list->created_at)); ?></div>
					</div>
					<div class="listitem">
						<div class="title">Proposal Submitted</div>
						<?php 
							$latestProposal = $list->proposals->sortByDesc('created_at')->first();
						 ?>
						<?php if($latestProposal): ?>
							<div class="content"><?php echo e($latestProposal->created_at->format('F d, Y')); ?></div>
						<?php endif; ?>
					</div>
					<div class="listitem">
						<div class="title">Have They Selected An Agent</div>
						<div class="content"><?php echo e($list->selectedAgent ? 'Yes' : 'Not Yet'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>