

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agent-page agent-home
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>	
	
	<section id="ahome-banner-area" style="background-image:url(<?php echo e(asset('images/banner-agent-home.jpg')); ?>)">
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 text-center">
	            <h1 class="banner-title">Win More Business, Sell More Homes</h1>
	            <p>Spark your business with highly qualified leads from Home By Home</p>
	           	<a href="<?php echo e(route('agent.register')); ?>" class="btn btn-pink  ">Join Free Today</a>
	            </div>
	        </div>
	    </div>
	</section>
	<?php 
	 function youtube_id_from_url($url) {
	 	$pattern = 
                '%^# Match any youtube URL
                (?:https?://)?  # Optional scheme. Either http or https
                (?:www\.)?      # Optional www subdomain
                (?:             # Group host alternatives
                  youtu\.be/    # Either youtu.be,
                | youtube\.com  # or youtube.com
                  (?:           # Group path alternatives
                    /embed/     # Either /embed/
                  | /v/         # or /v/
                  | /watch\?v=  # or /watch\?v=
                  )             # End path alternatives.
                )               # End host alternatives.
                ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
                $%x'
                ;
        $result = preg_match($pattern, $url, $matches);
        if ($result) {
            return $matches[1];
        }
        return false;
    }
	?>
	<section id="ahome-videos" class="default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"  src="https://player.vimeo.com/video/225771774" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"  src="https://player.vimeo.com/video/235654123" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				</div>
				<!-- <div class="col-xs-12 text-center">
					<a style="margin-top:30px;" href="<?php echo e(route('agent.register')); ?>" class="btn btn-blue  ">View more Testimonials</a>
				</div> -->
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12"><hr class="global-divider"/></div>
		</div>
	</div>
	<section id="ahome-howitworks" class="default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="hiw-heading"><strong>HOW IT WORKS</strong></h2>
					<div class="hiw-wrapper">
						<div class="hiw-list">
							<div class="hiw-item">
								<div class="item-wrapper">
									<div class="image"><img src="<?php echo e(asset('images/agent-how-1.svg')); ?>" alt="HIW - 1"></div>
									<div class="desc">Sign up and get approved to become a certified Home By Home agent</div>
								</div>
							</div><div class="hiw-item">
								<div class="item-wrapper">
									<div class="image"><img src="<?php echo e(asset('images/agent-how-2.svg')); ?>" alt="HIW - 2"></div>
									<div class="desc">Start receiving qualified leads from buyers and sellers</div>
								</div>
							</div><div class="hiw-item">
								<div class="item-wrapper">
									<div class="image"><img src="<?php echo e(asset('images/agent-how-3.svg')); ?>" alt="HIW - 3"></div>
									<div class="desc">Submit proposals centered on exclusive HBH pricing and qualifications</div>
								</div>
							</div><div class="hiw-item">
								<div class="item-wrapper">
									<div class="image"><img src="<?php echo e(asset('images/agent-how-4.svg')); ?>" alt="HIW - 4"></div>
									<div class="desc">Win More Business, Sell More Homes</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>