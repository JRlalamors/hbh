

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<style type="text/css">
	select option.selected-agent:after {
		content: "(selected)";
	}

	.agents-list {
		margin: 20px 0 0;
	}

	.agents-list ul li {
		border-bottom: 1px solid #cecece;
	}

	.agents-list ul li:last-child {
		border: 0;
	}

	.agents-list ul li a {
		padding: 10px 0;
		display: block;
	}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents customer-profile
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-controls'); ?>
<div class="controls">	
	<?php if( !$customer->approved ): ?>
		<a class="btn btn-pink   confirm" data-confirmtitle="Approve customer?" data-confirmtext="Are you sure you want to approve this customer?" data-confirmfunc="approveCustomer" data-confirmparams="[<?php echo e($customer->id); ?>]">Approve</a>
	<?php endif; ?>

	<?php if( !$customer->rejected ): ?>
		<a class="btn btn-blue   confirm" data-confirmtitle="Reject customer?" data-confirmtext="Are you sure you want to reject this customer?" data-confirmfunc="rejectCustomer" data-confirmparams="[<?php echo e($customer->id); ?>]">Reject</a>
	<?php endif; ?>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Customer Profile
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="customer-profile">

	<?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="profile-details default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="display-profile">						
						<div class="profile-detail">
							<div class="title">Name:</div>
							<div class="content"><?php echo e($customer->getOtherData('admin_name') ? $customer->getOtherData('admin_name') : $customer->asUser->name); ?></div>
						</div>
						<div class="profile-detail">
							<div class="title">Email:</div>
							<div class="content"><?php echo e($customer->getOtherData('admin_email') ? $customer->getOtherData('admin_email') : $customer->asUser->email); ?></div>
						</div>
						<div class="profile-detail">
							<div class="title">Phone:</div>
							<div class="content"><?php echo e($customer->getOtherData('admin_phone') ? $customer->otherDataPhoneFormat() : $customer->asUser->phone_format); ?></div>
						</div>	

						<button class="btn btn-primary pull-right edit-profile">Edit</button>
					</div>

					<div class="edit-profile-form hidden">						
						<?php echo Form::model($customer, ['route' => ['admin.customers.save-profile', $customer], 'class' => 'default-form']); ?>

							<div class="profile-detail">
								<div class="title">Name:</div>
								<div class="content">								
									<?php echo Form::text('admin_name', $customer->getOtherData('admin_name') ? $customer->getOtherData('admin_name') : $customer->asUser->name, ['class' => 'form-control']); ?>

								</div>
							</div>

							<div class="profile-detail">
								<div class="title">Email:</div>
								<div class="content">								
									<?php echo Form::text('admin_email', $customer->getOtherData('admin_email') ? $customer->getOtherData('admin_email') : $customer->asUser->email, ['class' => 'form-control']); ?>

								</div>
							</div>

							<div class="profile-detail">
								<div class="title">Phone:</div>
								<div class="content">								
									<?php echo Form::text('admin_phone', $customer->getOtherData('admin_phone') ? $customer->otherDataPhoneFormat() : $customer->asUser->phone_format, ['class' => 'form-control validate-phone-num']); ?>

								</div>
							</div>

							<div class="text-right">
								<button class="btn btn-success">Save</button>
								<a href="javascript:;" class="btn btn-danger cancel">Cancel</a>
							</div>
						<?php echo Form::close(); ?>

					</div>

				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="profile-detail">
						<div class="title">ID:</div>
						<div class="content"><?php echo e($customer->id); ?></div>
					</div>
					<div class="profile-detail">
						<div class="title">Buyer:</div>
						<div class="content">
							<div class="content default-form">
								<div class="checkbox-form">
									<label class="checkbox"><input type="radio" name="buyer" <?php echo e($customer->buyerList()->count() > 0 ? 'checked' : ''); ?>><span class="chk pink"></span><span class="text">Yes</span></label>
								</div>
								<div class="checkbox-form">	
									<label class="checkbox"><input type="radio" name="buyer" <?php echo e($customer->buyerList()->count() == 0 ? 'checked' : ''); ?>><span class="chk pink"></span><span class="text">No</span></label>									
								</div>
							</div>
						</div>
					</div>
					<div class="profile-detail">
						<div class="title">Seller:</div>
						<div class="content default-form">
							<div class="checkbox-form">
								<label class="checkbox"><input type="radio" name="seller" <?php echo e($customer->sellerList()->count() > 0 ? 'checked' : ''); ?>><span class="chk pink"></span><span class="text">Yes</span></label>
							</div>
							<div class="checkbox-form">	
								<label class="checkbox"><input type="radio" name="seller" <?php echo e($customer->sellerList()->count() == 0 ? 'checked' : ''); ?>><span class="chk pink"></span><span class="text">No</span></label>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>

	<div class="profile-settings default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="tab-controls">
						<ul class="nav nav-tabs nav-justified">
						  <li class="active"><a data-toggle="tab" class="theme-blue" href="#tab-buyers">Buyers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-sellers">Sellers</a></li>
						  <li><a class="theme-blue" data-toggle="tab" href="#tab-pitches">Pitches</a></li>
						  <li><a href="#tab-history" class="theme-blue" data-toggle="tab">History</a></li>
						</ul>
					</div>					
					<div class="tab-content">
			  			<div id="tab-buyers" class="tab-pane fade in active">
							<a href="javascript:;" data-type="buyer" data-toggle="modal" data-target="#customerList" class="create-list btn btn-info">Create Buyer List</a>
					    	<div class="details-wrapper">						    	
						    	<?php $__empty_1 = true; $__currentLoopData = $buyersList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
						    		<?php echo $key != 0 ? '<hr>' : ''; ?>

						    		<div id="list-id-<?php echo e($list->id); ?>" class="details-list">
										<?php echo $__env->make('admin.customer.partials.column-buyer', ['list' => $list], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						    		</div>
						    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
						    		<div class="details-list">
						    			<div class="detail-col">Listing not found..</div>
						    		</div>
						    	<?php endif; ?>

					    	</div>
					  	</div>
					  	<div id="tab-sellers" class="tab-pane fade">
							<a href="javascript:;" data-type="seller" data-toggle="modal" data-target="#customerList" class="create-list btn btn-info">Create Seller List</a>
								<div class="details-wrapper">
								<?php $__empty_1 = true; $__currentLoopData = $sellersList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
						    		<?php echo $key != 0 ? '<hr>' : ''; ?>

						    		<div id="list-id-<?php echo e($list->id); ?>" class="details-list">
										<?php echo $__env->make('admin.customer.partials.column-seller', ['list' => $list], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						    		</div>
						    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
						    		<div class="details-list">
						    			<div class="detail-col">Listing not found..</div>
						    		</div>
						    	<?php endif; ?>
						    	
					    	</div>
					  	</div>
					  	<div id="tab-pitches" class="tab-pane fade">
					    	<div class="table-responsive default-table">			                 
			                    <table class="table table-pitches">
			                        <thead>
			                            <tr>
			                                <th class="text-center">Date Scheduled</th>
			                                <th class="text-center">Agent</th>
			                                <th class="text-center">Buyer/Seller</th>
			                                <th class="text-center">Rebate</th>
			                                <th class="text-center">Approx $</th>
			                                <th class="text-center">View Details</th>
			                                <th class="text-center">Was Selected</th>
			                                <th class="text-center">Transaction ID</th>
			                            </tr>
			                        </thead>
			                        <tbody>			                        	
			                        	<?php $__empty_1 = true; $__currentLoopData = $pitches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pitch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			                        		<?php if( $pitch->agents->count() > 0 ): ?>

			                        			<?php $__currentLoopData = $pitch->agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                        				<tr>			                        					
				                        				<td class="text-center">
															<?php if($list->text_until): ?>
																<?php echo e($list->text_until); ?>

															<?php else: ?>
																<?php echo date('M d, o', strtotime($list->until)); ?>
															<?php endif; ?>
														</td>
				                        				<td class="text-center"><?php echo e($agent->asUser->name); ?></td>
				                        				<td class="text-center"><?php echo e($pitch->type()); ?></td>

			                        					<?php if( $pitch->agentHasProposal($agent->id) ): ?>
					                        				<td class="text-center">
				                        						<?php echo e($pitch->proposal($agent->id)->rebate_format); ?>

					                        				</td>
					                        				<td class="text-center"><?php echo e($pitch->proposal($agent->id)->approximate); ?></td>
			                        					<?php else: ?>
			                        						<td class="text-center">Not yet submitted</td>
			                        						<td class="text-center">Not yet submitted</td>
			                        					<?php endif; ?>

			                        					<td class="text-center"><a href="<?php echo e(route('admin.customers.profile.list-details', [$pitch->customer, $pitch])); ?>" class="blue">View Full Details</a></td>

			                        					<td class="text-center">
			                        						<?php if( $pitch->hasSelectedAProposal() && $pitch->agent_id == $agent->id ): ?>
			                        							<?php echo e('Yes'); ?>

			                        						<?php else: ?>
			                        							<?php echo e('No'); ?>

			                        						<?php endif; ?>
			                        					</td>

			                        					<td class="text-center"><?php echo e($pitch->id); ?></td>
			                        				</tr>
			                        			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			                        		<?php endif; ?>
			                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			                        		<tr><td colspan="8">Pitches not found..</td></tr>
			                        	<?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>
					  	
					  	<div id="tab-history" class="tab-pane fade">	
					  		<div class="table-responsive default-table">
			                    <table class="table">
			                        <thead>
			                            <tr>
			                                <th>Action Taken</th>
			                                <th class="text-right">Date and Time</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            <?php $__empty_1 = true; $__currentLoopData = $activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			                            	<tr>
			                            		<td><?php echo e($activity->action_taken); ?> ( IP : <?php echo e($activity->ip); ?> )</td>
			                            		<td class="text-right"><?php echo date('M d, o g:i:s', strtotime($activity->date)); ?></td>
			                            	</tr>
			                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			                            <?php endif; ?>
			                        </tbody>
			                    </table>
			                </div>
					  	</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.customer.agent-form', ['customer' => $customer], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		$(document).ready(function(){

            function IsJsonString(str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            }

			var $baseUrl = $('meta[name="base-url"]').attr('content'),
				$listID,
				$listType,
				$theData = {};				

			$('body').on('click', '.assign-agent', function() {
				$listID = $(this).data('list');
			});

			$('body').on('click', '.create-list', function () {
                $listID = null;
				$listType = $(this).data('type');
            });

            $('body').on('click', '.edit-list', function() {
                $listID = $(this).data('list');
            });

			$('.agents-select').change(function(){	

				var $agentID = $(this).val(),
					$agentName = $(this).find('option:selected').text();

				// push new values
				$theData.userAgents = {}
				$theData.userAgents[$agentID] = $agentName

				// push new values
				$('.agents-select').not($(this)).each(function($index){
					$theData.userAgents[$(this).val()] = $(this).find('option:selected:not(:first-child)').text();
				});

				$('.agents-select').not($(this)).each(function(){
					$(this)
						.find('option:not(:first-child)')
							.each(function(){
								if( $(this).val() in $theData.userAgents ) {
									$(this).attr('disabled', 'disabled');
								}
								else {
									$(this).removeAttr('disabled');
								}
							})
						.end()
						.find('option:selected:not(:first-child)')
							.removeAttr('disabled');
				});

			});

            $('#customerList').on('show.bs.modal', function(e) {
                if ($listID != null) {
                    var actUrl = $baseUrl + '/admin/customers/list/' + $listID + '/edit-list';
				} else {
                    var actUrl = $baseUrl + '/admin/customers/list/create-list';
				}

                $.ajax({
					dataType: 'html',
                    type: 'GET',
                    url: actUrl,
					data: {
					    'list_type' : $listType
					},
                    success: function($data) {
                        $('#customerList').find('#ajax-result').html($data);
                        setTimeout(function () {
                            initMap();
                        }, 200);
                    }
                });
            });

            $('body').on('submit', '.listing-form', function (e) {
                e.preventDefault();

                $('.alert-danger').fadeOut();

                var $data = {
                    'customer_id' : '<?php echo e($customer->id); ?>',
                    'list_type' : $('input[name="list_type"]').val(),
                    'property_type_id' : $('input[name="property_type_id"]:checked').val(),
                    'zip_postal' : $('input[name="zip_postal"]').val(),
                    'budget' : $('input[name="list_type"]').val() == 'buyer' ? $('input[name="budget"]').val() : $('select[name="budget"]').val(),
                    'numeric_budget' : $('input[name="list_type"]').val() == 'buyer' ? $('[name="budget"]').val() : $('[name="budget"]').find('option:selected').data('numeric'),
                    'city' : $('input[name="city"]').val(),
                    'state' : $('input[name="state"]').val(),
                    'street_address' : $('input[name="street_address"]').val(),
                    'unit_no' : $('input[name="unit_no"]').val(),
                    'until' : $('select[name="until"]').val(),
                    'list_id' : $('input[name="list_id"]').length > 0 ? $('input[name="list_id"]').val() : null,
                    'status_to_lender' : $('input[name="status_to_lender"]:checked').val(),
                    'sq_feet_min' : $('input[name="sq_feet_min"]').val(),
                    'sq_feet_max' : $('input[name="sq_feet_max"]').val(),
                    'bedrooms' : $('select[name="bedrooms"]').val(),
                    'bathrooms' : $('select[name="bathrooms"]').val(),
                };

                $.ajax({
                    dataType: 'html',
                    type: 'POST',
					data : $data,
                    url: $baseUrl + '/admin/customers/list/update-list',
                    success: function($data) {
                        if (IsJsonString($data)) {
                            var $errors = JSON.parse($data);
							$('.alert-danger').fadeIn();
							$('.alert-danger').find('.alert-content').html(function () {
                                var html = '<ul>';
                                $errors.forEach(function(item, index) {
                                    html += '<li>' + item + '</li>';
                                });
                                html += '</ul>';
                                return html;
                            })
						} else {
                            if ($listID != null) {
                                $('#list-id-' + $listID).html($data);
                            } else {
								$('.tab-pane.active').find('.details-wrapper').prepend($data);
                            }
                            $('#customerList').modal('hide');
                            $listID = null;
                        }
                    }
                });
            });

			// load all agents in modal with selected per each row
			$('#assignAgent').on('show.bs.modal', function(e) {
				$.ajax({
					type: 'POST',
					url: $baseUrl + '/admin/customers/list/' + $listID + '/edit-agents',
					success: function($data) {

						$('#assignAgent .set-agent').data("listid", $data.list.id);

						$('.agents-select').each(function($index){
							var $this = $(this);
							$this.html("");
							$this.append('<option value="" selected>No Agent</option>');

							// Append Agents
							Object.keys($data.agentsArr).forEach(function($key){
								var $selected = '',
									$disabled = '';
								
								// check if the object key == index and key exists
								if( Object.keys($data.userAgents)[$index] == $key ) {
									$selected = 'selected';
								}

								if( $key in $data.userAgents && Object.keys($data.userAgents)[$index] != $key ) {
									$disabled = 'disabled';									
								}
								
								$this.append('<option value="'+$key+'" '+$selected+' '+$disabled+'>'+$data.agentsArr[$key].name+' ('+$data.agentsArr[$key].email+')</option>');
							});
						});

						$theData = $data;
					}
				});
			});

			// Setting agents
			$('.set-agent').click( function() {

				var $listID = $(this).data('listid'),
				$agents = [
					$(this).closest('.modal').find('[name="agent-1"]').val(),
					$(this).closest('.modal').find('[name="agent-2"]').val(),
					$(this).closest('.modal').find('[name="agent-3"]').val()
				];

				$agents = $agents.filter(Boolean);

				$.ajax({
					type: 'POST',
					url: baseUrl + '/admin/customers/list/' + $listID + '/store-agents',
					data: {'agents' : $agents},
					success: function( $data ) {
						if( $data.message == "success" ) 
						{
							$('#assignAgent').modal('hide');
							$('.agents-list[data-agentlistid="'+$listID+'"]').html("");

							Object.keys($data.agentsArr).forEach(function($key){
								$('.agents-list[data-agentlistid="'+$listID+'"]')
									.append('<div><a href="'+$data.agentsArr[$key].route+'">'+$data.agentsArr[$key].name+' <br> ('+$data.agentsArr[$key].email+')</a></div>');
							});

							$('.table-pitches')
								.find('tbody')
									.html($data.pitchOutput);
						} else {

						}
					}
				});

			});

			$('.edit-profile').click(function() {
				$('.edit-profile-form').removeClass('hidden');
				$('.display-profile').addClass('hidden');
			});

			$('.cancel').click(function() {
				$('.edit-profile-form').addClass('hidden');
				$('.display-profile').removeClass('hidden');
			});

		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUaOvb7g_KLbkUEX-GvbFBqw6Jpgi6Cuw&libraries=places&callback=initMap" async defer></script>
	<?php echo $__env->make('admin.customer.partials.geocode-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>