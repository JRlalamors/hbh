

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		
		.chat-wrapper {
			position: fixed;
			bottom: 10px;			
			border: 1px solid #0C4672;
			background: #fff;
			padding: 10px;
			right: 10px;
			z-index: 10;
			display: none;
		}

		.chat-header {
			margin-top: -10px;
			margin-left: -10px;
			margin-right: -10px;
			padding: 10px;
			background: #0C4672;
		}

		.chat-name {
			margin: 0;
			float: left;
			color: #fff;
		}

		.chat-body {
			height: 240px;
			border-top: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
			margin-left: -10px;
			margin-right: -10px;
			margin: 5px 0;
		}

		.chat-close {
			float: right;
			cursor: pointer;
			color: #fff;
			font-size: 16px;
		}

		.chat-close:hover {
			text-decoration: none;
			color: #fff;
		}

		.chat-wrapper .btn {
			padding: 10px;
			font-size: 14px;
			font-size: 16px;
		}

	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer-dashboard pagetitle-off site-notice
<?php $__env->stopSection(); ?>
<!-- Content -->
<?php $__env->startSection('content'); ?>
<section id="cs-listings">

	<?php if( $newUser ): ?>
		<div class="bg-success text-success text-center">Thank You For Signing Up For Home By Home</div>
	<?php endif; ?>

	<?php if( $buyingList->count() > 0 ): ?>
	<div class="cs-items">
		<div class="cs-heading">
			<div class="container">
				<div class="row">

					<div class="col-xs-12">
						
						<div class="cs-heading-wrapper">
							<div class="item title"><h2>My Proposals For Buying a Home</h2></div>
							<div class="item ctrl">
								<?php if(!$authUser->isNewUser()): ?>
								<a href="<?php echo e(route('customer.list.add', ['list_type' => 'buyer'])); ?>" class="btn btn-pink  ">Request more Proposals</a> <a href="javascript:;" class="hidden btn  btn-lblue  ">Close</a>
								<?php endif; ?>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
		
		<div class="cs-contents">
			<div class="container">
				<?php $__currentLoopData = $buyingList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php echo $__env->make('user.customer.partials.proposal-row', [
						'list' => $list
					], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php if( !$loop->last ): ?>
						<hr>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if( $sellingList->count() > 0 ): ?>
	<div class="cs-items">
		<div class="cs-heading">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">						
						<div class="cs-heading-wrapper">
							<div class="item title"><h2>My Proposals For Selling a Home</h2></div>
							<div class="item ctrl">
								<?php if(!$authUser->isNewUser()): ?>
									<a href="<?php echo e(route('customer.list.add', ['list_type' => 'seller'])); ?>" class="btn btn-pink  ">Request more Proposals</a> <a href="javascript:;" class="btn hidden  btn-lblue  ">Close</a>
								<?php endif; ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		
		<div class="cs-contents">
			<div class="container">
				<?php $__currentLoopData = $sellingList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php echo $__env->make('user.customer.partials.proposal-row', [
						'list' => $list
					], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php if( !$loop->last ): ?>
						<hr>	
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

</section>

<section id="post-default" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="post-heading"><span class="text">ARTICLES AND GUIDES</span></h2>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="post-highlights">
							<div class="bg" style="background-image:url(<?php echo e(asset('images/post-1.jpg')); ?>)"></div>
							<div class="heading">
								<h2 class="title">WHY DO I NEED A REAL ESTATE AGENT?</h2>
								<a href="https://www.homebyhome.com/blog/need-real-estate-agent/" class="btn btn-pink hvr-shutter-out-horizontal">Learn More</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="post-highlights">
							<div class="bg" style="background-image:url(<?php echo e(asset('images/post-2.jpg')); ?>)"></div>
							<div class="heading">
								<h2 class="title">HOW DOES HOMEBYHOME WORK FOR ME?</h2>
								<a href="<?php echo e(route('sellers.process')); ?>" class="btn btn-pink hvr-shutter-out-horizontal">Learn More</a>
							</div>
						</div>
					</div>
				</div>
				

				<h2 class="post-heading"><span class="text">TESTIMONIALS</span></h2>

			 	<div class="tst-wrapper">
					<div class="tst-list">
						<div class="item">
							<div class="item-wrapper">
								<div class="image"><img src="<?php echo e(asset('images/sam-n.jpg')); ?>" alt="Home by Home"></div>
								<div class="content">
									<p>I wish I knew about Home by Home when I was buying a house! We really needed a service like this to help us navigate the process of finding an agent as a first time home buyer. I'm telling all my friends.</p>
								</div>
								<div class="author"><span class="name">Sam N.</span><span class="location">Northridge, CA</span></div>
							</div>
						</div>
						<div class="item">
							<div class="item-wrapper">
								<div class="image"><img src="<?php echo e(asset('images/jordan-g.jpg')); ?>" alt="Home by Home"></div>
								<div class="content">
									<p>Home By Home is changing the way to find a real estate agent. It’s amazing that they can help someone like me save thousands of dollars when buying a home for the very first time. I highly recommend HomeByHome.com</p>
								</div>
								<div class="author"><span class="name">Jordan G.</span><span class="location">Calabasas, CA</span></div>
							</div>
						</div>
						<div class="item">
							<div class="item-wrapper">
								<div class="image"><img src="<?php echo e(asset('images/evan-g.jpg')); ?>" alt="Home by Home"></div>
								<div class="content">
									<p>I was sold as soon as I heard about HomeByHome.com. With their focus on quality and savings I don’t know why anyone would look to find a real estate agent any other way</p>
								</div>
								<div class="author"><span class="name">Evan G.</span><span class="location">Los Angeles, CA</span></div>
							</div>
						</div>
					</div>
				</div>
	            <div class="tst-controls hidden">
	                <a class="btn btn-blue invert  ">See More Testimonials</a>
	            </div>

			</div>
		</div>
	</div>
</section>

<div class="chat-wrapper">
	<div class="chat-header">
		<p class="chat-name">Agent Name</p>
		<a class="chat-close">&times;</a>
		<div class="clearfix"></div>
	</div>
	<div class="chat-body">
		
	</div>
	<div class="chat-actions">
		<input type="text" name="chat-messsage" class="chat-message" placeholder="Type something..">
		<button class="btn btn-xs btn-blue  ">Send</button>
	</div>
</div>

<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>
<script>
$('document').ready(function(){

	$('.proposal-box').on('click','.pb-menu-ctrl',function(){		
		$wrapper = $(this).closest('.proposal-box');

		if($wrapper.hasClass('active')){
			$wrapper.removeClass('active');
		} else{
			$wrapper.addClass('active');
		}
	});

	/*$('.list-details').on('click','.list-heading',function(){
		$wrapper = $(this).closest('.list-details');

		if($wrapper.hasClass('active')){
			$(this).next('.list-info').slideUp();
			$wrapper.removeClass('active');
		} else{
			$(this).next('.list-info').slideDown();
			$wrapper.addClass('active');
		}
	});	*/

	$('body').on('click', '.start-editing', function () {
		var $this = $(this),
			form = $this.closest('.listing-form');

		form.find('.preview').hide();
		form.find('.editing').show();
    })
	
	$('body').on('click', '.cancel-editing', function () {
        var $this = $(this),
            form = $this.closest('.listing-form');

        form.find('.preview').show();
        form.find('.editing').hide();
    })
	
	$('body').on('click', '.update-editing', function (e) {
		e.preventDefault();
		var $this = $(this),
			id = $this.data('id'),
			form = $($this.data('form'));

		console.log(form.attr('action'));
		$.ajax({
			type : 'POST',
			url : form.attr('action'),
			data : form.serialize(),
			success : function(response) {
			    $('#basic-info-' + id).html(response);
			}
		})
    })

});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>