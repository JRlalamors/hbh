<?php echo $__env->make('layouts.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">		
					<h1><?php echo $__env->yieldContent('page-title'); ?></h1>
					<?php echo $__env->yieldContent('page-controls'); ?>
				</div>
			</div>
		</div>
	</section>

	<div id="page-wrapper">
		<div id="page-content">				
			<?php echo $__env->yieldContent('content'); ?>
		</div>			
	</div>

<?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>