

<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		.panel-group a {
			display: block;			
			padding: 20px 15px;
		}

		.panel-group a:hover, 
		.panel-group a:focus {
			text-decoration: none;
		}

		.panel-group .panel-heading {
			padding: 0;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer-profile pagetitle-off
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section id="customer-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="photo"><img class="img-responsive" src="<?php echo e($customer->asUser->profile_photo ? $customer->asUser->profile_photo : asset('images/user-blank.svg')); ?>"></div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="info">
					<h1 class="name"><?php echo e($customer->asUser->name); ?></h1>
					<?php if( $customer->asUser->city ): ?>
						<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($customer->asUser->city . " " . $customer->asUser->state); ?></div>
					<?php endif; ?>
					<div class="moreinfo">
						<div class="listitem">
							<div class="title">Email</div><div class="content"><a href="mailto:<?php echo e($customer->asUser->email); ?>"><?php echo e($customer->asUser->email); ?></a></div>
						</div>
						<div class="listitem">
							<div class="title">City</div><div class="content"><?php echo e($customer->asUser->city ?: '--'); ?></div>
						</div>
						<div class="listitem">
							<div class="title">State</div><div class="content"><?php echo e($customer->asUser->state ?: '--'); ?></div>
						</div>
						<div class="listitem">
							<div class="title">Phone</div><div class="content"><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $authUser->phone_num) ?>"><?php echo e($customer->asUser->phone_format); ?></a></div>
						</div>
					</div>
					<div class="ctrls">
						<a href="" class="btn btn-pink btn-round hvr-rectangle-out">Message</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="customer-moreinfo" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-push-4">

				<?php if( $listings->count() > 0 ): ?>

					<div class="panel-group" id="accordion">
						<?php $__currentLoopData = $listings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo e($list->id); ?>"><?php echo e($list->property->human_display); ?></a>
									</h4>
								</div>
								<div id="collapse<?php echo e($list->id); ?>" class="panel-collapse collapse <?php echo e($loop->first ? 'in' : ''); ?>">
									<div class="panel-body profile cp-container">
										<div class="listitem">
											<div class="title">Are you looking to buy a home or sell a home?</div>
											<div class="content"><?php echo e($list->list_type == 'buyer' ? 'Sell Home' : 'Buy Home'); ?></div>
										</div>
										<div class="listitem">
											<div class="title">Where are you looking to buy / sell?</div>
											<div class="content"><?php echo e($list->complete_address); ?></div>
										</div>
										<div class="listitem">
											<div class="title">Home much are you hoping to spend/sell on a home?</div>
											<div class="content">
												<?php if( $list->budget ): ?>
													<?php echo e($list->budget); ?>

												<?php else: ?>
													<?php echo e('N/A'); ?>

												<?php endif; ?>
											</div>
										</div>
										<div class="listitem">
											<div class="title">What kind of property are you buying/selling?</div>
											<div class="content"><?php echo e($list->property->human_display); ?></div>
										</div>
										<div class="listitem">
											<div class="title">How soon are you looking to buy/sell?</div>
											<div class="content">
												<?php if($list->text_until): ?>
													<?php echo e($list->text_until); ?>

												<?php else: ?>
													<?php echo date('M d, o', strtotime($list->until)); ?>
												<?php endif; ?>
											</div>
										</div>
										<div class="listitem">
											<div class="title">Are you pre-approved by a lender?</div>
											<div class="content"><?php echo e($customer->lender_status); ?></div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>

				<?php else: ?>

					<div class="profile cp-container">
						Listings not found..
					</div>

				<?php endif; ?>

			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-8">
				<div class="sidebar cp-container">
					<div class="listitem">
						<div class="title">Date Signed Up</div>
						<div class="content"><?php echo date('M d, o', strtotime($authUser->created_at)); ?></div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>