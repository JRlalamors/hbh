<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <p><?php echo e($message); ?></p>
                    <p><strong>Name: </strong> <?php echo e($customer->name); ?></p>
                    <p><strong>Email: </strong> <?php echo e($customer->email); ?></p>
                    <p><strong>Phone: </strong> <?php echo e($customer->phone_format); ?></p>
                </div></td>
            <td></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>