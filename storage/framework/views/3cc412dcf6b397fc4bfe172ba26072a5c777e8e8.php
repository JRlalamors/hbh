

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agent-page agent-register footer-off
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>	
<div class="container">

	<div class="row">

		<div class="col-xs-12">
		
			<?php echo $__env->make('partials.form-step', [
					'form' => 'agent-registration'
				], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		</div>

	</div>

</div> 
	

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>